-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 15, 2019 at 10:50 AM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `brainpower`
--

-- --------------------------------------------------------

--
-- Table structure for table `aboutpage`
--

DROP TABLE IF EXISTS `aboutpage`;
CREATE TABLE IF NOT EXISTS `aboutpage` (
  `aboutpage_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `desig` varchar(255) NOT NULL,
  `about_me` text NOT NULL,
  `certified_in` varchar(255) NOT NULL,
  `personal_stat` text NOT NULL,
  `other_stat` text NOT NULL,
  `vission` text NOT NULL,
  `mission` text NOT NULL,
  PRIMARY KEY (`aboutpage_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aboutpage`
--

INSERT INTO `aboutpage` (`aboutpage_id`, `name`, `desig`, `about_me`, `certified_in`, `personal_stat`, `other_stat`, `vission`, `mission`) VALUES
(1, 'Poonam Somdutt Lad ', 'Managing Director', 'I, Mrs. Poonam Somdutt Lad , was in the IT industry for 12 years ,I’m a BSC (Botany ) Graduate and MBA (HR) by qualification. They say life has it’s own twists and turns some turns you know and some come as a surprise so have been the case with me .After having a career with IT industry for about 12 years and then all of a sudden taking sabbatical to take care and be full time with the family was not an easy call but now when I look back it was my best decision. The fun is to break the monotony and move to your higher planned and designed destiny. Little I knew this was the beginning of a new found world where I would be having a complete family time and exploring areas which I never thought I would ever. Enjoyed my time setting up my garden ,learning to cook ,the most interesting part I could pursue my long stranded interest  towards the subject of psychology  as the mystic world and reading people always mesmerised me. Also, what really interest and got me curious towards this wonderful topic of midbrain power was mainly because one would hear from lot of people about feeling exhausted, tired, loosing memory, feeling lonely some complains around kids they would not listen, repsect for elders is not seen, very aggressive, fearful, lack decision making capacity etc. On studying various topics I realized that it’s mainly coming from an area of imbalance, discontentment messages being sent to your brain repeatedly, brain has literally forgotten it’s stop button once it’s get onto thinking pattern. This made me take my hobby to the next level and got myself certified …', 'Child and Adoloscent Psycology:Enhancing Potential from (JPIP)DMITREIKI Level 1 & Level 2Chakra MeditationISO 90012008', 'I personally believe if you train your brain it can take care of you in all circumstances, and put you away from feeling victimised , self-pity and other negative emotions. Positive thinking helps build good relationship, effective communication by balancing our emotional quotient And hence I created this platform named Brain Power to develop the right confidence level and attitude to be a winner in life. ', 'They say the best knowledge is when you share with the society and help them benefit so felt like creating a platform which will cater and help resolve problems and help people and mainly children to understand the significance and infinite and immense power of brain. True challenge of our brain is when it learns to accept a new change positive or negative and keep us in balanced, active and enthusiastic way and help us enjoy every moment of life. It’s well said that positive thinking is more than just a tagline. It changes the way we behave. And I firmly believe that when I’m positive, it not only makes me better but it also makes those around me better', 'To empower generations with a sense of belief, confidence and competitiveness to create their well-being and more importantly a sense of oneness to contribute to the society.\r\n\r\n', '-	To conduct Mid-Brain Activation Program which can help the participants to use their right and left brain functions simultaneously and effectively. This will enable the participants to become competitive in today’s complex environment and at the same time instil within oneself a sense of responsibility to contributes towards our society\r\n-	To conduct scientific study of the fingerprints of the participants and enable them to understand their potentials and at the same time weakness which can help them to improve their deliverables and in turn achieve well-being\r\n\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `aboutpage_images`
--

DROP TABLE IF EXISTS `aboutpage_images`;
CREATE TABLE IF NOT EXISTS `aboutpage_images` (
  `img_id` int(10) NOT NULL AUTO_INCREMENT,
  `aboutpage_id` int(10) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `image_path` varchar(255) NOT NULL,
  PRIMARY KEY (`img_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aboutpage_images`
--

INSERT INTO `aboutpage_images` (`img_id`, `aboutpage_id`, `image_name`, `image_path`) VALUES
(2, 1, '02-large.jpg', 'uploads/aboutpage/02-large.jpg'),
(3, 1, '04-large.jpg', 'uploads/aboutpage/04-large.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
CREATE TABLE IF NOT EXISTS `course` (
  `course_id` int(10) NOT NULL AUTO_INCREMENT,
  `course_name` varchar(255) NOT NULL,
  `course_fees` varchar(255) NOT NULL,
  `course_details` text NOT NULL,
  `user_id` int(10) NOT NULL,
  PRIMARY KEY (`course_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`course_id`, `course_name`, `course_fees`, `course_details`, `user_id`) VALUES
(1, 'Adult courseupdated', '15000', 'bdcndknc k,d ck', 1),
(2, 'Adult course', '15000', 'bdcndknc k,d ck', 1);

-- --------------------------------------------------------

--
-- Table structure for table `gallery_desc`
--

DROP TABLE IF EXISTS `gallery_desc`;
CREATE TABLE IF NOT EXISTS `gallery_desc` (
  `title_id` int(10) NOT NULL AUTO_INCREMENT,
  `img_title` varchar(255) NOT NULL,
  `img_desc` text NOT NULL,
  `created_at` timestamp NOT NULL,
  `user_id` int(10) NOT NULL,
  PRIMARY KEY (`title_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_desc`
--

INSERT INTO `gallery_desc` (`title_id`, `img_title`, `img_desc`, `created_at`, `user_id`) VALUES
(1, 'dsadfsgdg', 'this is gud updated', '2019-01-15 01:27:41', 1),
(5, 'course', 'zsdfxcghjbklm,', '2019-01-15 01:49:21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `gallery_images`
--

DROP TABLE IF EXISTS `gallery_images`;
CREATE TABLE IF NOT EXISTS `gallery_images` (
  `img_id` int(10) NOT NULL AUTO_INCREMENT,
  `image_name` varchar(255) NOT NULL,
  `title_id` int(10) NOT NULL,
  `image_path` varchar(255) NOT NULL,
  PRIMARY KEY (`img_id`),
  KEY `title_id` (`title_id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_images`
--

INSERT INTO `gallery_images` (`img_id`, `image_name`, `title_id`, `image_path`) VALUES
(11, '02-large.jpg', 1, 'uploads/gallery/02-large.jpg'),
(2, '02-large.jpg', 1, 'uploads/gallery/02-large.jpg'),
(25, '02-large.jpg', 1, 'uploads/gallery/02-large.jpg'),
(26, '02-small.jpg', 1, 'uploads/gallery/02-small.jpg'),
(27, '02-large.jpg', 5, 'uploads/gallery/02-large.jpg'),
(28, '02-small.jpg', 5, 'uploads/gallery/02-small.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `global_settings`
--

DROP TABLE IF EXISTS `global_settings`;
CREATE TABLE IF NOT EXISTS `global_settings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `instagram` varchar(255) NOT NULL,
  `about_brainpower` text NOT NULL,
  `did_you_know` text NOT NULL,
  `total_stud` varchar(255) NOT NULL,
  `total_courses` varchar(255) NOT NULL,
  `total_teacher` varchar(255) NOT NULL,
  `total_class` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `global_settings`
--

INSERT INTO `global_settings` (`id`, `address`, `phone`, `email`, `facebook`, `twitter`, `instagram`, `about_brainpower`, `did_you_know`, `total_stud`, `total_courses`, `total_teacher`, `total_class`) VALUES
(1, 'Abc society,flat no 402,near pccoe,akurdi,pune', '8889997779', 'abc@gmail.com', 'www.facebook.com', 'www.twitter.com', 'www.instagram.com', '                                                                                                                                                                                                                 Brain Power is all about undertaking Brain Training exercises which can significantly improve the functions of the Brain. Brain training is beneficial for people regardless of their age. Children can develop their cognitive functions much faster, adults can get an edge at work, while older people can keep their mind fit through Brain training exercises.                         ', 'Did you know an average person only utilises less than 10% of the brain’s potential? That means have 90% of latent potential waiting to be untapped! We are probably more familiar with the terms ‘left brain’, ‘right brain’, and the characteristics related to each side of the brain. But did you know that we can use both sides of the brain interchangeably and achieve effective results? So for the same we need to activate our brain. So What does it mean to activate’ your brain? Is our brain not ‘activated’? Why do we need to ‘activate’ it? And how do you know when your brain is ‘activated’? \r\n\r\n', '1000', '3', '10', '10');

-- --------------------------------------------------------

--
-- Table structure for table `news_events`
--

DROP TABLE IF EXISTS `news_events`;
CREATE TABLE IF NOT EXISTS `news_events` (
  `news_id` int(10) NOT NULL AUTO_INCREMENT,
  `news_title` varchar(255) NOT NULL,
  `news_desc` text NOT NULL,
  `news_img` varchar(255) NOT NULL,
  `user_id` int(10) NOT NULL,
  `category` varchar(10) NOT NULL,
  `dates` varchar(255) NOT NULL,
  `times` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  PRIMARY KEY (`news_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news_events`
--

INSERT INTO `news_events` (`news_id`, `news_title`, `news_desc`, `news_img`, `user_id`, `category`, `dates`, `times`, `location`) VALUES
(1, 'sdfhjknupdated', 'cbjshvcjcz nznnbz ', '013_-_Copy1.jpg', 1, '', '0000-00-00 00:00:00', '', ''),
(3, 'Testing news', 'dnasvdasbcms ', '01-large4.jpg', 1, '', '0000-00-00 00:00:00', '', ''),
(4, 'testing events from events to news eqd', 'ks l M  MX JSBVSVSJVJVvrfwfw', '06-small.jpg', 1, 'news', '10/9/17', '5 PM', 'Pune'),
(5, 'abcdddd12345 updated news', 'N XMCVXBVN,XCVncsmdms mnsd', '01-large7.jpg', 1, 'news', '10/2/2019', '5 PM', 'baner');

-- --------------------------------------------------------

--
-- Table structure for table `stories`
--

DROP TABLE IF EXISTS `stories`;
CREATE TABLE IF NOT EXISTS `stories` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `story_title` varchar(255) NOT NULL,
  `story_desc` text NOT NULL,
  `story_by` varchar(255) NOT NULL,
  `story_loc` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stories`
--

INSERT INTO `stories` (`id`, `story_title`, `story_desc`, `story_by`, `story_loc`) VALUES
(1, 'Perfect Trainer updatd', 'cn cnc n k kdj cjsljcnlkaxcx,cm c', 'Sanket Chidrawar', 'mumbai'),
(3, 'best teaching', 'sdfghjkljhbvcgfxdzz', 'kiran waychal', 'mumbai');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blogs`
--

DROP TABLE IF EXISTS `tbl_blogs`;
CREATE TABLE IF NOT EXISTS `tbl_blogs` (
  `blog_id` int(10) NOT NULL AUTO_INCREMENT,
  `blog_title` varchar(255) NOT NULL,
  `blog_desc` text NOT NULL,
  `blog_img` varchar(255) NOT NULL,
  `user_id` int(10) NOT NULL,
  `created_at` timestamp NOT NULL,
  PRIMARY KEY (`blog_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_blogs`
--

INSERT INTO `tbl_blogs` (`blog_id`, `blog_title`, `blog_desc`, `blog_img`, `user_id`, `created_at`) VALUES
(1, 'abcd updated', 'nancmvchdvccvdhbccmnmmsbsbsnsm', '0131.jpg', 1, '0000-00-00 00:00:00'),
(4, 'blog 2', 'this is testing', '04-small.jpg', 1, '2019-01-15 01:20:41');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_enquiry`
--

DROP TABLE IF EXISTS `tbl_enquiry`;
CREATE TABLE IF NOT EXISTS `tbl_enquiry` (
  `enquiry_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`enquiry_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_enquiry`
--

INSERT INTO `tbl_enquiry` (`enquiry_id`, `name`, `email`, `contact`, `subject`, `message`) VALUES
(1, 'sayali', 'sayali.role265@gmail.com', '89865982', 'abcd', 'this is testing'),
(2, 'Brain', 'sayalirolecool@gmail.com', '24356789', 'abcd', 'lajckhsbckhckmnc,kzn,kcn '),
(3, 'sayali', 'kiran@gmail.com', '24356789', 'abcd', 'Your Messageswedsgergergdgeghrh');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE IF NOT EXISTS `tbl_users` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `user_img` varchar(255) DEFAULT NULL,
  `id` int(10) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `username`, `password`, `firstname`, `lastname`, `user_img`, `id`) VALUES
(1, 'sayali', '123', 'sayali', 'Role', '04-large1.jpg', 1),
(2, 'sanket', '123', 'sanket', 'chidrawar', 'CAM_IMG_1495353767004.jpg', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_details`
--

DROP TABLE IF EXISTS `tbl_user_details`;
CREATE TABLE IF NOT EXISTS `tbl_user_details` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_firstname` varchar(255) NOT NULL,
  `user_lastname` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_contact` varchar(255) NOT NULL,
  `user_address` text NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_img` varchar(255) NOT NULL,
  `user_id` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_details`
--

INSERT INTO `tbl_user_details` (`id`, `user_firstname`, `user_lastname`, `user_email`, `user_contact`, `user_address`, `username`, `password`, `user_img`, `user_id`) VALUES
(1, 'sayali', 'Role', 'sayali@gmail.com', '1234567878', 'pune', 'sayali', '123', '04-large1.jpg', 1),
(2, 'sanket', 'chidrawar', 'sank@gmail.com', '1234567878', 'akurdi', 'sanket', '123', 'CAM_IMG_1495353767004.jpg', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
