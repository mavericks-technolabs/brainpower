<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'FrontController/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['index'] = 'FrontController/index';
$route['about'] = 'FrontController/about';
$route['midbrain'] = 'FrontController/midbrain';
$route['dmit'] = 'FrontController/dmit';
$route['chakrameditation'] = 'FrontController/chakrameditation';
$route['gallery'] = 'FrontController/gallery';
$route['blog'] = 'FrontController/blog';
$route['blog_details'] = 'FrontController/blog_details';
$route['event'] = 'FrontController/event';
$route['more_events'] = 'FrontController/more_events';
$route['contact'] = 'FrontController/contact';
$route['more_news'] = 'FrontController/more_news';


$route['login_form'] = 'LoginController/index';
$route['dashboard'] = 'HomeController/dashboard';

$route['all_users'] = 'HomeController/all_user';
$route['add_users'] = 'HomeController/add_user';
$route['edit_user/(:any)'] = 'HomeController/edit_user/$1';
$route['delete_user/(:num)/(:any)'] = 'HomeController/delete_user/$1/$2';

$route['all_gallery'] = 'HomeController/all_gallery';
$route['add_gallery'] = 'HomeController/add_gallery';
$route['edit_gallery'] = 'HomeController/edit_gallery';
$route['edit_gallery/(:any)'] = 'HomeController/edit_gallery/$1';
$route['delete_images/(:any)/(:any)'] = 'HomeController/delete_images/$1/$2';
$route['delete_gallery/(:any)'] = 'HomeController/delete_gallery/$1';

$route['all_courses'] = 'HomeController/all_courses';
$route['add_course'] = 'HomeController/add_course';
$route['editcourse/(:any)'] = 'HomeController/edit_course/$1';
$route['delete_course/(:any)'] = 'HomeController/delete_course/$1';

$route['all_newsevents'] = 'HomeController/all_newsevents';
$route['add_newsevents'] = 'HomeController/addnewsevents';
$route['edit_newsevents/(:any)'] = 'HomeController/edit_newsevents/$1';
$route['delete_news/(:num)/(:any)'] = 'HomeController/delete_news/$1/$2';

$route['all_blogs'] = 'HomeController/all_blogs';
$route['add_blogs'] = 'HomeController/addblogs';
$route['edit_blogs/(:any)'] = 'HomeController/edit_blogs/$1';
$route['delete_blogs/(:num)/(:any)'] = 'HomeController/delete_blogs/$1/$2';

$route['all_stories'] = 'HomeController/all_stories/';
$route['add_success_stories'] = 'HomeController/add_success_stories/';
$route['edit_success_stories/(:any)'] = 'HomeController/edit_success_stories/$1';
$route['delete_story/(:any)'] = 'HomeController/delete_story/$1';

$route['all_aboutpage'] = 'HomeController/all_aboutpage';
$route['edit_aboutpage/(:any)'] = 'HomeController/edit_aboutpage/$1';
$route['delete_about_images/(:num)/(:any)'] = 'HomeController/delete_about_images/$1/$2';

$route['all_global_settings'] = 'HomeController/all_global_settings';
$route['edit_global_settings/(:any)'] = 'HomeController/edit_global_settings/$1';

$route['profile'] = 'HomeController/profile';
$route['all_enquiries'] = 'HomeController/all_enquiries';



