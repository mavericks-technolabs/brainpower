  <?php

  class sucessstory_model extends CI_Model
  {
      public function stories_list()
        {
            $query = $this->db->select("*")
                                ->from('stories')
                                ->get();
            return $query->result();

        }
        public function add_stories($array )
         {
             return $this->db->insert('stories', $array);

        }

        public function get_stories($value)
        {
            $q = $this->db->select("*")
                        ->where('id',$value)
                        ->get('stories');                       
            return $q->row();        
        }
        public function get_stories_view()
        {
            $q = $this->db->select("*")
                            ->get('stories');                       
            return $q->result();        
        }
        public function insertstory($id,$storyedit)
        {
             return $this->db->where('id',$id)
                             ->update('stories', $storyedit);

        }
        public function delete_story($value)
        {
             return  $this->db->where('id', $value)
                              ->delete("stories");
        }
      }

