<?php

class Blogs_model extends CI_Model
{
	
	public function add_blogs($blogdata)
	{

		 return $this->db->insert('tbl_blogs', $blogdata);

	}
	public function blogs_list()
	{
	    $user_id = $this->session->userdata('user_id');
	    $query = $this->db
								->select("*")
								->from('tbl_blogs')
								->where('user_id', $user_id)
								->get();
								//print_r($this->db->last_query());exit();
			return $query->result();
	}
	/*public function fontend_news_list()
	{
		 $query = $this->db
								->select("*")
								->from('news_events')
								->get();
								//print_r($this->db->last_query());exit();
			return $query->result();
	}
	public function update_retrive($news_id)
		{
			$this->db->where('news_id',$news_id)
						->from('news_events');
			$q = $this->db->get();
			return $q->result();
		}*/
	 public function get_blogs($value)
        {
    
            $q = $this->db->select("*")
            		    ->where('blog_id',$value)
                        ->get('tbl_blogs');
                        //print_r($this->db->last_query());exit();
                return $q->row();
        }

        public function get_blogs_edit()
        {
    
            $q = $this->db->select("*")
                        ->get('tbl_blogs');
                        //print_r($this->db->last_query());exit();
                return $q->row();
        }
         public function update_blogs($blog_id,$editdata)
        {

          return $this->db->where('blog_id',$blog_id)
                    ->update('tbl_blogs', $editdata);
                      
                   
        }

         public function delete_blogs($value)
        {
            $abc = $this->db->where('blog_id', $value)
            		->delete("tbl_blogs");

            return $abc;
        }
}
?>