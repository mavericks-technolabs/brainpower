<?php

class Loginmodel extends CI_Model
{
	public function login_valid( $username, $password)
	{
		$q = $this->db->where(['username'=>$username,'password'=>$password])
						->get('tbl_users');
			
		if ( $q->num_rows() )
		{
			return $q->row()->user_id;				
		}
		else
		{
			return FALSE;				
		}
	}

	public function num_rows()
	{
		$query = $this->db->query('SELECT * FROM tbl_users');
		return $query->num_rows();
	}

	public function get_user($value)
    {
        $q=$this->db->select("*")
                    ->where('user_id',$value)
                    ->get('tbl_users');

        return $q->row();
            // print_r($q);
        }
}