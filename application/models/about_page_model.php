<?php

	class about_page_model extends CI_Model
	{
		public function about_list()
		{
			$query = $this->db->select("*")
								->from('aboutpage')
								->get();
			return $query->result();
		}
		public function get_aboutpage($value)
        {
    		$q = $this->db->select("*")
            		   		->where('aboutpage_id',$value)
                        	->get('aboutpage');

                return $q->row();
        }
          public function get_about_view()
        {
            $q = $this->db->select("*")
                            ->get('aboutpage');

                return $q->result();
        }

         public function updateabout($company)
        {

            return $this->db->update('aboutpage', $company);        
        }

		public function get_multi_images($value)
        {
        	$q = $this->db->select("*")
                		->where('aboutpage_id', $value)
                        ->get("aboutpage_images");
			return $q->result();
        }
        public function get_multi_images_view()
        {
            $q = $this->db->select("*")
                        ->get("aboutpage_images");
            return $q->result();
        }


		public function update_about_images($aboutpage_id , Array $data)
		{
			return $this->db
					    ->where('aboutpage_id',$aboutpage_id)
					    ->update('aboutpage',$data );

		}
		public function insert($data = array())
    	{
	        $insert = $this->db->insert_batch('aboutpage_images',$data);
	        return $insert?true:false;
    	}
    	 public function get_about_image($img_id)
        {
            $q = $this->db->where('img_id', $img_id)
                            ->get("aboutpage_images");
	        return $q->row();
        }
         public function del_about_image($img_id)
        {
          
            $q=$this->db->where('img_id', $img_id)
                     ->delete('aboutpage_images');
            return $q;                    
        }
    }