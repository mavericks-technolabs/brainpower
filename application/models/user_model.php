<?php

class User_model extends CI_Model
{
	
	public function add_user($array,$user_table)
	{
		$abc= $this->db->insert('tbl_user_details', $array);
		$id=$this->db->insert_id();
		
		$user_table['id']=$id;
		
		$data= $this->db->insert('tbl_users', $user_table);
		return $abc;

	}
	public function user_list()
	{
		$user_id = $this->session->userdata('user_id');
		$query = $this->db
						 ->select("*")
						 ->from('tbl_user_details')
             ->where('id !=', $user_id)
						 ->get();
								//print_r($this->db->last_query());exit();
			return $query->result();
	}

	public function get_user($value)
     {
    
         $q = $this->db->select("*")
            		    ->where('id',$value)
                        ->get('tbl_user_details');

         return $q->row();
      }
    public function insertuser($id,$useredit,$user_table_edit)
      {

        $data = $this->db->where('id',$id)
                         ->update('tbl_user_details', $useredit);

        $abc = $this->db->set('username', $user_table_edit['username'])
                    	->set('password', $user_table_edit['password'])
                    	->set('firstname', $user_table_edit['firstname'])
                    	->set('lastname', $user_table_edit['lastname'])
                    	->set('user_img', $user_table_edit['user_img'])
                    	->where('id',$user_table_edit['id'])
                    	->update('tbl_users');
		return $data;          
        }
    public function get_user_edit()
    {
    
        $q = $this->db->select("*")
                      ->get('tbl_user_details');

        return $q->row();
    }
    public function delete_user($value)
    {
        $abc = $this->db->where('id', $value)
            		    ->delete("tbl_user_details");
            	

        $this->db->where('id',$value)
                 ->delete('tbl_users');

        return $abc;
        }
}