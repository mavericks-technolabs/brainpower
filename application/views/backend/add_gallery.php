<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js" ></script>
<style type="text/css">
    .swal-button {
  padding: 7px 19px;
  border-radius: 2px;
  background-color: #4962B3;
  font-size: 12px;
  border: 1px solid #3e549a;
  text-shadow: 0px -1px 0px rgba(0, 0, 0, 0.3);
}
.swal-footer {
  background-color: rgb(245, 248, 250);
  margin-top: 32px;
  border-top: 1px solid #E9EEF1;
  overflow: hidden;
}
.swal-overlay {
  background-color: #8abe51;
}
</style>
<?php if ($this->session->flashdata('message')): ?>
                            <script>
                                swal({
                                    title: "Warning",
                                    text: "<?php echo $this->session->flashdata('message'); ?>",
                                    icon: "warning",
                                    showConfirmButton: false,
                                    type: 'success'
                                });
                            </script>
                    <?php endif; ?>
 <div class="page-wrapper">
            <div class="container-fluid">
                <!--.row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"> Add Gallery</div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <?php echo form_hidden('user_id',$this->session->userdata('user_id')); 
                                    ?>
                                 <?php echo form_open_multipart('HomeController/addgallery',['class'=>'form-horizontal form-bordered']); ?>
                                 
                                    <!-- <form action="#" class="form-horizontal form-bordered"> -->
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Title</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter title" name="img_title" class="form-control" value="<?php echo set_value('img_title') ?>">
                                                    <?php echo form_error('img_title');?>
                                                 </div>

                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Description</label>
                                                <div class="col-md-6">
                                                    <textarea name="img_desc" class="form-control" rows="5" value="<?php echo set_value('img_desc') ?>"></textarea>
                                                    <?php echo form_error('img_desc');?>
                                                </div>
                                         </div>
                                         <div class="form-group">
                                            <label class="control-label col-md-2">Select Picture</label>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" name="files[]" multiple/>
                                         </div>
                                        </div>
                                        <div class="form-actions" style="padding-top: 85px;padding-bottom: 20px;">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                                                            <?php echo form_reset(['class'=>'btn btn-primary','name'=>'reset','value'=>'Reset'])?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--./row-->
                
            </div>
        </div>