        <div class="page-wrapper">
            <div class="container-fluid">
                <!-- /.row -->
                <!-- .row -->
                 <?php echo form_open_multipart('HomeController/updateprofile');?>
                <?php echo form_hidden('id',$use_rows->id) ?>
                <?php echo form_hidden('user_img',$use_rows->user_img) ?>

                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="white-box">
                            <div class="user-bg"> <img width="100%" alt="user" src="../plugins/images/large/img1.jpg">
                                <div class="overlay-box">
                                    <div class="user-content">
                                        <?php
                                                        $base = base_url()."uploads/users/";
                                                        $path = $base.$use_rows->user_img;
                                                  ?>
                                        <a href="javascript:void(0)"><img src="<?= $path ?>" height="150" width="150" alt="img"></a>
                                        </div>
                                </div>
                            </div>
                            <div class="user-btm-box">
                               <h4 style="text-align: center;"><?=$use_rows->user_firstname?> <?=$use_rows->user_lastname?></h4>
                            </div>
                        </div>
                    </div>
                     <div class="col-md-8 col-xs-12">
                        <div class="white-box">
                           <h3>My Profile</h3><hr><br>
                    <form class="form-horizontal form-material">
                          <div class="row">
                           <div class="form-group">
                            <div class="col-md-3">
                                 <label class="col-md-12">First Name</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="user_firstname" value="<?php echo set_value('user_firstname', $use_rows->user_firstname) ?>" class="form-control form-control-line">
                                <?php echo form_error('user_firstname');?>      
                            </div>
                            </div>

                          </div>
                          <!--  --><br>
                             <div class="row">
                           <div class="form-group">
                            <div class="col-md-3">
                                 <label class="col-md-12">Last Name</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="user_lastname" value="<?php echo set_value('user_lastname', $use_rows->user_lastname) ?>" class="form-control form-control-line"> 
                                <?php echo form_error('user_lastname');?>     
                            </div>
                            </div>

                          </div>
                          <!--  --><br>

                          <div class="row">
                           <div class="form-group">
                            <div class="col-md-3">
                                 <label class="col-md-12">Email</label>
                            </div>
                            <div class="col-md-9">
                                <input type="email" name="user_email" value="<?php echo set_value('user_email', $use_rows->user_email) ?>" class="form-control form-control-line">   
                                <?php echo form_error('user_email');?>   
                            </div>
                            </div>

                          </div>
                          <!--  --><br>

                           <div class="row">
                           <div class="form-group">
                            <div class="col-md-3">
                                 <label class="col-md-12">Contact No.</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="user_contact" value="<?php echo set_value('user_contact', $use_rows->user_contact) ?>" class="form-control form-control-line">  
                                <?php echo form_error('user_contact');?>    
                            </div>
                            </div>

                          </div>
                          <!--  --><br>

                           <div class="row">
                           <div class="form-group">
                            <div class="col-md-3">
                                 <label class="col-md-12">Address</label>
                            </div>
                            <div class="col-md-9">
                                <textarea type="text" name="user_address" class="form-control form-control-line"><?=$use_rows->user_address?></textarea>
                                <?php echo form_error('user_address');?>
                                <!-- <input type="text" placeholder="8787879909" class="form-control form-control-line">       -->
                            </div>
                            </div>

                          </div>
                          <br>
                          <div class="row">
                            <div class="form-group">
                                <div class="col-md-3">
                                    <label class="col-md-12">Select Picture</label>
                                </div>
                                <div class="col-md-9">
                                 <?php echo form_upload(['class'=>'form-control','name'=>'userfile','type'=>'file','required']) ?>
                            </div>
                            </div>
                          </div>
                          <!--  --><br>

                          <div class="row">
                           <div class="form-group">
                            <div class="col-md-3">
                                 <label class="col-md-12">Username</label>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="username" value="<?php echo set_value('username', $use_rows->username) ?>" class="form-control form-control-line">
                                <?php echo form_error('username');?>      
                            </div>
                            </div>

                          </div>
                          <!--  --><br>

                           <div class="row">
                           <div class="form-group">
                            <div class="col-md-3">
                                 <label class="col-md-12">Password</label>
                            </div>
                            <div class="col-md-9">
                                <input type="password" name="password" value="<?php echo set_value('password', $use_rows->password) ?>" class="form-control form-control-line" id="myInput">  
                                <?php echo form_error('password');?>    
                            </div>
                            </div>

                          </div>
                          <div class="col-md-3">
                          </div>
                          <div class="col-md-9">
                          <input type="checkbox" onclick="myFunction()">Show Password
                          </div>
                          <script type="text/javascript">
                              function myFunction() {
                              var x = document.getElementById("myInput");
                                if (x.type === "password") 
                                {
                                     x.type = "text";
                                } else {
                                  x.type = "password";
                                }
                             }       
                          </script>

                          <div class="form-actions" style="padding-top: 85px;padding-bottom: 20px;">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                                                            <?php echo form_reset(['class'=>'btn btn-primary','name'=>'reset','value'=>'Reset'])?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                      </form>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
               
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
