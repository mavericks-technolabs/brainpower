<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js" ></script>
<style type="text/css">
    .swal-button {
  padding: 7px 19px;
  border-radius: 2px;
  background-color: #4962B3;
  font-size: 12px;
  border: 1px solid #3e549a;
  text-shadow: 0px -1px 0px rgba(0, 0, 0, 0.3);
}
.swal-footer {
  background-color: rgb(245, 248, 250);
  margin-top: 32px;
  border-top: 1px solid #E9EEF1;
  overflow: hidden;
}
</style>
<?php if ($this->session->flashdata('welcome')): ?>
                            <script>
                                swal({
                                    title: "Welcome",
                                    text: "<?php echo $this->session->flashdata('welcome'); ?>",
                                    icon: "success",
                                    timer: 1500,
                                    type: 'success'
                                });
                            </script>
                    <?php endif; ?>
        <div class="page-wrapper">
            <!-- ===== Page-Container ===== -->
            <div class="container-fluid">
                <div class="row colorbox-group-widget">
                    <div class="col-md-3 col-sm-6 info-color-box">
                        <div class="white-box">
                            <div class="media bg-primary">
                                <div class="media-body">
                                    <h3 class="info-count">154 <span class="pull-right"><i class="mdi mdi-checkbox-marked-circle-outline"></i></span></h3>
                                    <p class="info-text font-12">Bookings</p>
                                    <p class="info-ot font-15">Target<span class="label label-rounded">198</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 info-color-box">
                        <div class="white-box">
                            <div class="media bg-success">
                                <div class="media-body">
                                    <h3 class="info-count">68 <span class="pull-right"><i class="mdi mdi-comment-text-outline"></i></span></h3>
                                    <p class="info-text font-12">Complaints</p>
                                    <p class="info-ot font-15">Total Pending<span class="label label-rounded">154</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 info-color-box">
                        <div class="white-box">
                            <div class="media bg-danger">
                                <div class="media-body">
                                    <h3 class="info-count">&#36;9475 <span class="pull-right"><i class="mdi mdi-coin"></i></span></h3>
                                    <p class="info-text font-12">Profit</p>
                                    <p class="info-ot font-15">Pending<span class="label label-rounded">236</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 info-color-box">
                        <div class="white-box">
                            <div class="media bg-warning">
                                <div class="media-body">
                                    <h3 class="info-count">&#36;124,356 <span class="pull-right"><i class="mdi mdi-coin"></i></span></h3>
                                    <p class="info-text font-12">Total Profit</p>
                                    <p class="info-ot font-15">Pending<span class="label label-rounded">782</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

