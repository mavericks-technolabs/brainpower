 <div class="page-wrapper">
            <div class="container-fluid">
                <!--.row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"> Add Success Stories</div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <?php echo form_open('HomeController/editstory',['class'=>'form-horizontal form-bordered']); ?>
                                    <?php echo form_hidden('id',$story_row->id) ?>
                                 
                                    <!-- <form action="#" class="form-horizontal form-bordered"> -->
                                         <div class="form-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Title</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter title" name="story_title" class="form-control" value="<?php echo set_value('story_title', $story_row->story_title) ?>">
                                                    <?php echo form_error('story_title');?>
                                                 </div>

                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Details</label>
                                                <div class="col-md-6">
                                                    <textarea name="story_desc" placeholder="enter details" class="form-control" rows="5"><?= $story_row->story_desc?></textarea>
                           
                                                    <?php echo form_error('story_desc');?>
                                                </div>
                                         </div>
                                         <div class="form-group">
                                                <label class="control-label col-md-2">Story By</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter name of the person" name="story_by" class="form-control" value="<?php echo set_value('story_by', $story_row->story_by) ?>">
                                                    <?php echo form_error('story_by');?>
                                                 </div>

                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Designation/Location</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter details" name="story_loc" class="form-control" value="<?php echo set_value('story_loc', $story_row->story_loc) ?>">
                                                    <?php echo form_error('story_loc');?>
                                                 </div>

                                            </div>
                                         
                                        <div class="form-actions" style="padding-top: 85px;padding-bottom: 20px;">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                                                            <?php echo form_reset(['class'=>'btn btn-primary','name'=>'reset','value'=>'Reset'])?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--./row-->
                
            </div>
        </div>