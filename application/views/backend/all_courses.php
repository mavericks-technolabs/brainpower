<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from www.cubictheme.ga/cubic-html/data-table.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 09 Jan 2019 13:14:00 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
    <title>Brainpower</title>
    <!-- ===== Bootstrap CSS ===== -->
    <link href="<?php echo base_url();?>assets/backend/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- ===== Plugin CSS ===== -->
    <link href="<?php echo base_url();?>assets/backend/plugins/components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="../../cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- ===== Animation CSS ===== -->
    <link href="<?php echo base_url();?>assets/backend/css/animate.css" rel="stylesheet">
    <!-- ===== Custom CSS ===== -->
    <link href="<?php echo base_url();?>assets/backend/css/style.css" rel="stylesheet">
    <!-- ===== Color CSS ===== -->
    <link href="<?php echo base_url();?>assets/backend/css/colors/default.css" id="theme" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js" ></script>
<style type="text/css">
    .swal-button {
  padding: 7px 19px;
  border-radius: 2px;
  color: #ffff;
  background-color: #4962B3;
  font-size: 12px;
  border: 1px solid #3e549a;
  text-shadow: 0px -1px 0px rgba(0, 0, 0, 0.3);
}
.swal-footer {
  background-color: rgb(245, 248, 250);
  margin-top: 32px;
  border-top: 1px solid #E9EEF1;
  overflow: hidden;
}
</style>

</head>

<body class="mini-sidebar">
    <?php if ($this->session->flashdata('error')): ?>
    <script>
        swal({
            title: "Done",
            text: "<?php echo $this->session->flashdata('error'); ?>",
            icon: "success",
            timer: 1500,
            showConfirmButton: false,
            type: 'success'
        });
    </script>
<?php endif; ?>
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <!-- ===== Top-Navigation ===== -->
               <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <a class="navbar-toggle font-20 hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class="fa fa-bars"></i>
                </a>
                <div class="top-left-part">
                    <a class="logo" href="index-2.html">
                        <b>
                            <img src="" />
                        </b>
                        <span>
                            <img src=""class="dark-logo" />
                        </span>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-left hidden-xs">
                    <li>
                        <a href="javascript:void(0)" class="sidebartoggler font-20 waves-effect waves-light"><i class="icon-arrow-left-circle"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
        <!-- ===== Top-Navigation-End ===== -->
        <!-- ===== Left-Sidebar ===== -->
       <aside class="sidebar">
            <div class="scroll-sidebar">
                <div class="user-profile">
                    <div class="dropdown user-pro-body">
                        <div class="profile-image">
                             <?php  
                                            $base = base_url()."uploads/users/";
                                           $path = $base.$user_row->user_img;
                                         ?>
                            <img src="<?= $path ?>" alt="user-img" class="img-circle">
                            <a href="javascript:void(0);" class="dropdown-toggle u-dropdown text-blue" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <span class="badge badge-danger">
                                    <i class="fa fa-angle-down"></i>
                                </span>
                            </a>
                            <ul class="dropdown-menu animated flipInY">
                                <li><a href="<?php echo base_url('profile');?>"><i class="fa fa-user"></i> Profile</a></li>
                                
                                <li><a href="<?php echo base_url('LoginController/logout');?>"><i class="fa fa-power-off"></i> Logout</a></li>
                            </ul>
                        </div>
                        <p class="profile-text m-t-15 font-16"><a href="javascript:void(0);">  <?php echo $user_row->firstname ?> <?php echo $user_row->lastname ?></a></p>
                    </div>
                </div>
                <nav class="sidebar-nav">
                    <ul id="side-menu">
                        <li>
                            <a class="active waves-effect" href="<?php echo base_url('dashboard')?>" aria-expanded="false"><i class="fa fa-dashboard"></i> <span class="hide-menu"> Dashboard</a>
                        </li>
                         <li>
                            <a class="active waves-effect" href="<?php echo base_url('all_users')?>" aria-expanded="false"><i class="fa fa-users"></i> <span class="hide-menu">Users</a>
                        </li>
                         <li>
                            <a class="active waves-effect" href="<?php echo base_url('all_gallery')?>" aria-expanded="false"><i class="fa fa-file-picture-o"></i> <span class="hide-menu">Gallery</a>
                        </li>
                        <li>
                            <a class="active waves-effect" href="<?php echo base_url('all_courses')?>" aria-expanded="false"><i class="fa  fa-graduation-cap"></i> <span class="hide-menu">Courses</a>
                        </li>
                        <li>
                            <a class="active waves-effect" href="<?php echo base_url('all_newsevents')?>" aria-expanded="false"><i class="fa   fa-calendar"></i> <span class="hide-menu">News & Events</a>
                        </li>
                        <li>
                            <a class="active waves-effect" href="<?php echo base_url('all_blogs')?>" aria-expanded="false"><i class="fa fa-folder"></i> <span class="hide-menu">Blogs</a>
                        </li>
                        <li>
                            <a class="active waves-effect" href="<?php echo base_url('all_stories')?>" aria-expanded="false"><i class="fa fa-bar-chart-o"></i> <span class="hide-menu">Success Stories</a>
                        </li>
                        <li>
                            <a class="active waves-effect" href="<?php echo base_url('all_aboutpage')?>" aria-expanded="false"><i class="fa fa-laptop"></i> <span class="hide-menu">About page</a>
                        </li>
                        <li>
                            <a class="active waves-effect" href="<?php echo base_url('all_global_settings')?>" aria-expanded="false"><i class="fa fa-institution (alias)"></i> <span class="hide-menu">Global Setting</a>
                        </li>
                        <li>
                            <a class="active waves-effect" href="<?php echo base_url('all_enquiries')?>" aria-expanded="false"><i class="fa fa-envelope"></i> <span class="hide-menu">Enquiries</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- ===== Left-Sidebar-End ===== -->
        <!-- Page Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                <!-- /row -->
                 <?php echo form_hidden('title_id',$this->session->userdata("title_id")); ?>
                      <?php echo form_hidden('img_id',$this->session->userdata('img_id')); ?>
                      <?php echo form_hidden('image_path',$this->session->userdata('image_path')); ?>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">All Courses</h3>
                            <a href="<?php echo base_url('add_course');?>" id="addRow" class="btn btn-info" style="margin-bottom: 20px;" >
                            Add New <i class="fa fa-plus" style="color: white;"></i>
                                        </a>
                            <div class="table-responsive">
                                <table id="myTable" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Sr No.</th>
                                           <th>Course Name</th>
                                            <th>Course Fees</th>
                                            <th>Action</th>
                                    
                                        </tr> 
                                    </thead>
                                    <tbody>
                                       <?php if (count( $courseall )):
                                                    $i=1;
                                                    
                                                    foreach ($courseall as $courses ) :?>
                                                    <tr>
                                                        <td><?= $i; ?></td>
                                                        <td><?= $courses->course_name ?></td>
                                                        <td><?= $courses->course_fees ?></td>
                                                        <td>
                                                           <a href="<?php echo base_url();?>editcourse/<?=$courses->course_id?>" class="btn btn-primary btn-xs">
                                                               <i class="fa fa-pencil"></i>
                                                            </a>
                                                            <!-- <a href="<?php echo base_url();?>delete_course/<?=$courses->course_id?>" class="btn btn-danger btn-xs">
                                                                 <i class="fa fa-trash-o "></i>
                                                            </a> -->
                                                            <?php $id=$courses->course_id ?>
                                                            <a id="<?php echo $courses->course_id?>" href="javascript:void(0);" onclick="delete_course(this.id)" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
                                                        </td>
                                                                       
                                                    </tr>
                                                    <?php 
                                                    $i++;
                                                    endforeach; ?>

                                                    <?php else: ?>
                                                        <tr>
                                                            <td colspan="3">
                                                                No Records Found.
                                                            </td>
                                                        </tr>
                                                    <?php endif; ?>
                                                    
                                    </tbody>
                                </table>
                                  <script type="text/javascript">
                                    function delete_course(id)
                                    {
                                        var url="<?php echo base_url();?>";
                                        swal(
                                        {
                                            title: "Are you sure?",
                                            text: "You really want to delete this course?",
                                            icon: "warning",
                                            buttons: true,
                                            dangerMode: true,
                                        })
                                        .then((willDelete) => 
                                        {
                                                                    
                                            if (willDelete)
                                            {
                                                window.location = url+"delete_course/"+id;
                                                timer: 1500;
                                                swal("Your Course was deleted!",
                                                {
                                                    
                                                    icon: "success",
                                                    timer: 1000,
                                                });
                                            }
                                            else
                                            {
                                                swal("Your file is safe!");
                                            }
                                        });
                                    }
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
            footer class="footer t-a-c">
                <div class="bg-white p-20 "> <center> 2019 © Brainpower/ Design & Developed By Mavericks Technolabs(India) Pvt. Ltd.</a> </center>        </div>
            </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/backend/plugins/components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>assets/backend/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?php echo base_url();?>assets/backend/js/sidebarmenu.js"></script>
    <!--slimscroll JavaScript -->
    <script src="<?php echo base_url();?>assets/backend/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url();?>assets/backend/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url();?>assets/backend/js/custom.js"></script>
    <script src="<?php echo base_url();?>assets/backend/plugins/components/datatables/jquery.dataTables.min.js"></script>
    <!-- start - This is for export functionality only -->
    <script src="../../cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="../../cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="../../cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="../../cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="../../cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="../../cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="../../cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
    <script>
    $(function() {
        $('#myTable').DataTable();

        var table = $('#example').DataTable({
            "columnDefs": [{
                "visible": false,
                "targets": 2
            }],
            "order": [
                [2, 'asc']
            ],
            "displayLength": 25,
            "drawCallback": function(settings) {
                var api = this.api();
                var rows = api.rows({
                    page: 'current'
                }).nodes();
                var last = null;
                api.column(2, {
                    page: 'current'
                }).data().each(function(group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                        last = group;
                    }
                });
            }
        });
        // Order by the grouping
        $('#example tbody').on('click', 'tr.group', function() {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                table.order([2, 'desc']).draw();
            } else {
                table.order([2, 'asc']).draw();
            }
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    <!--Style Switcher -->
    <script src="../plugins/components/styleswitcher/jQuery.style.switcher.js"></script>
</body>


<!-- Mirrored from www.cubictheme.ga/cubic-html/data-table.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 09 Jan 2019 13:14:05 GMT -->
</html>
