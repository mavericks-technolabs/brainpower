 <div class="page-wrapper">
            <div class="container-fluid">
                <!--.row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"> Add News & Events</div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <?php echo form_hidden('user_id',$this->session->userdata('user_id')); 
                                    ?>
                                 <?php echo form_open_multipart('HomeController/add_newsevents',['class'=>'form-horizontal form-bordered']); ?>
                                 
                                    <!-- <form action="#" class="form-horizontal form-bordered"> -->
                                        <div class="form-body" id="title">
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Title</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter title" name="news_title" class="form-control" value="<?php echo set_value('news_title') ?>">
                                                    <?php echo form_error('news_title');?>
                                                 </div>

                                            </div>
                                            <div class="form-group" title="news_desc">
                                                <label class="control-label col-md-2">Description</label>
                                                <div class="col-md-6">
                                                    <textarea name="news_desc" placeholder="enter description" class="form-control" rows="5" value="<?php echo set_value('news_desc') ?>"></textarea>
                                                    <?php echo form_error('news_desc');?>
                                                </div>
                                         </div>
                                         <div class="form-group">
                                            <label class="control-label col-md-2">Select Picture</label>
                                            <div class="col-md-6">
                                               <?php echo form_upload(['class'=>'form-control','name'=>'userfile','type'=>'file','required']) ?>
                                         </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Category</label>
                                                <div class="col-md-6">
                                                        <select class="form-control" name="category" id="category" onchange="check(this);">
                                                            <option value="<?php echo set_value('category') ?>">Select</option>
                                                            <option value="news">News</option>
                                                            <option value="Events" id="Events">Events</option>
                                                        </select>
                                                </div>
                                             </div>
                                             <div class="form-group" id="date">
                                                <label class="control-label col-md-2">Date</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="dd/mm/yyyy" name="dates" class="form-control" value="<?php echo set_value('dates') ?>">
                                                    <?php echo form_error('dates');?>
                                                 </div>

                                            </div>
                                            <div class="form-group" id="mypayment" style="display: none;">
                                                <label class="control-label col-md-2">Time</label>
                                                <div class="col-md-6">
                                                    <input type="text" id="txtField" placeholder="enter time" name="times" class="form-control" value="<?php echo set_value('times') ?>" >
                                                    <?php echo form_error('times');?>
                                                 </div>

                                            </div>
                                            <div class="form-group" id="abc" style="display: none;">
                                                <label class="control-label col-md-2">Location</label>
                                                <div class="col-md-6">
                                                    <input type="text"  id="txtField" placeholder="enter location" name="location" class="form-control" value="<?php echo set_value('location') ?>">
                                                    <?php echo form_error('location');?>
                                                 </div>

                                            </div>
                                            <script type="text/javascript">
                                             function check()
                                                {
                                                    var dropdown = document.getElementById("category");
                                                    var current_value = dropdown.options[dropdown.selectedIndex].value;

                                                    if (current_value == "Events")
                                                    {
                                                        document.getElementById("mypayment").style.display = "block";
                                                        document.getElementById("abc").style.display = "block";
                                                    }
                                                    else 
                                                    {
                                                         document.getElementById("mypayment").style.display = "none";
                                                        document.getElementById("abc").style.display = "none";
                                                        
                                                    }
                                                }
                                            </script>
                                        <div class="form-actions" style="padding-top: 85px;padding-bottom: 20px;">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                                                            <?php echo form_reset(['class'=>'btn btn-primary','name'=>'reset','value'=>'Reset'])?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--./row-->
                
            </div>
        </div>