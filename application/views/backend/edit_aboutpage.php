 <div class="page-wrapper">
            <div class="container-fluid">
                <!--.row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">Edit About page</div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                <?php echo form_open_multipart('HomeController/edit_about_set',['class'=>'form-horizontal form-bordered']); ?>
                                  <?php echo form_hidden('aboutpage_id',$about_row->aboutpage_id) ?>
                                 
                                    <!-- <form action="#" class="form-horizontal form-bordered"> -->
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Name</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter name" name="name" class="form-control" value="<?php echo set_value('name',$about_row->name); ?>">
                                                    <?php echo form_error('name');?>
                                                 </div>

                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Designation</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter designation" name="desig" class="form-control" value="<?php echo set_value('desig',$about_row->desig); ?>">
                                                    <?php echo form_error('desig');?>
                                                 </div>

                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">About me</label>
                                                <div class="col-md-6">
                                                    <textarea name="about_me" placeholder="enter your details" class="form-control" rows="5" ><?= $about_row->about_me; ?></textarea>
                                                    <?php echo form_error('about_me');?>
                                                </div>
                                         </div>
                                         <div class="form-group">
                                                <label class="control-label col-md-2">Certified In</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter details" name="certified_in" class="form-control" value="<?php echo set_value('certified_in',$about_row->certified_in); ?>">
                                                    <?php echo form_error('certified_in');?>
                                                 </div>

                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Personal Statement</label>
                                                <div class="col-md-6">
                                                    <textarea name="personal_stat" placeholder="enter details" class="form-control" rows="5" ><?= $about_row->personal_stat; ?></textarea>
                                                    <?php echo form_error('personal_stat');?>
                                                </div>
                                         </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Other Statement</label>
                                                <div class="col-md-6">
                                                    <textarea name="other_stat" placeholder="enter details" class="form-control" rows="5" ><?= $about_row->other_stat; ?></textarea>
                                                    <?php echo form_error('other_stat');?>
                                                </div>
                                         </div>
                                          <div class="form-group">
                                                <label class="control-label col-md-2">Mission</label>
                                                <div class="col-md-6">
                                                    <textarea name="mission" placeholder="enter mission" class="form-control" rows="5" ><?= $about_row->mission; ?></textarea>
                                                    <?php echo form_error('mission');?>
                                                </div>
                                         </div>
                                          <div class="form-group">
                                                <label class="control-label col-md-2">Vision</label>
                                                <div class="col-md-6">
                                                    <textarea name="vission" placeholder="enter vission" class="form-control" rows="5" ><?= $about_row->vission; ?></textarea>
                                                    <?php echo form_error('vission');?>
                                                </div>
                                         </div>
                                         <div class="form-group">
                                            <label class="control-label col-md-2">Select Picture</label>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" name="files[]" multiple/>
                                         </div>
                                        </div>
                                         <?php foreach ($about_img as $gall_image):?>
                                             <?php
                                                     $base= base_url();
                                                     $img=$gall_image->image_path;
                                                     $path= $base.$img;

                                                                                     
                                             ?>  
                                            <a href="<?php echo base_url();?>delete_about_images/<?=$about_row->aboutpage_id?>/<?=$gall_image->img_id?>" class="btn btn-danger btn-xs">
                                            <i class="fa fa-trash-o "></i></a>  
                                            <img src="<?= $path ?>" width="150" height="150">
                                                            
                                                    <?php endforeach; ?>

                                        <div class="form-actions" style="padding-top: 85px;padding-bottom: 20px;">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                                                            <?php echo form_reset(['class'=>'btn btn-primary','name'=>'reset','value'=>'Reset'])?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--./row-->
                
            </div>
        </div>