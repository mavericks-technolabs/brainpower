            <!-- ===== Page-Container-End ===== -->
            <footer class="footer t-a-c">
                <div class="bg-white p-20 "> <center> 2019 © Brainpower/ Design & Developed By Mavericks Technolabs(India) Pvt. Ltd.</a> </center>        </div>
            </footer>
        </div>
        <!-- ===== Page-Content-End ===== -->
    </div>
    <!-- ===== Main-Wrapper-End ===== -->
    <!-- ==============================
        Required JS Files
    =============================== -->
    <!-- ===== jQuery ===== -->
    <script src="<?php echo base_url();?>assets/backend/plugins/components/jquery/dist/jquery.min.js"></script>
    <!-- ===== Bootstrap JavaScript ===== -->
    <script src="<?php echo base_url();?>assets/backend/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- ===== Slimscroll JavaScript ===== -->
    <script src="<?php echo base_url();?>assets/backend/js/jquery.slimscroll.js"></script>
    <!-- ===== Wave Effects JavaScript ===== -->
    <script src="<?php echo base_url();?>assets/backend/js/waves.js"></script>
    <!-- ===== Menu Plugin JavaScript ===== -->
    <script src="<?php echo base_url();?>assets/backend/js/sidebarmenu.js"></script>
    <!-- ===== Custom JavaScript ===== -->
    <script src="<?php echo base_url();?>assets/backend/js/custom.js"></script>
    <!-- ===== Plugin JS ===== -->
    <script src="<?php echo base_url();?>assets/backend/plugins/components/datatables/jquery.dataTables.min.js"></script>
     <script src="../../cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="../../cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="../../cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="../../cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="../../cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="../../cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="../../cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>assets/backend/plugins/components/chartist-js/dist/chartist.min.js"></script>
    <script src="<?php echo base_url();?>assets/backend/plugins/components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
    <script src='<?php echo base_url();?>assets/backend/plugins/components/moment/moment.js'></script>
    <script src='<?php echo base_url();?>assets/backend/plugins/components/fullcalendar/fullcalendar.js'></script>
    <script src="js/db2.js"></script>
    <!-- ===== Style Switcher JS ===== -->
    <script src="<?php echo base_url();?>assets/backend/plugins/components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>