 <div class="page-wrapper">
            <div class="container-fluid">
                <!--.row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"> Edit Gloal Setting</div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                
                    
                                    <?php echo form_open('HomeController/edit_global_set',['class'=>'form-horizontal form-bordered']); ?>
                                     <?php echo form_hidden('id',$setting_row->id) ?>
                                 
                                    <!-- <form action="#" class="form-horizontal form-bordered"> -->
                                         <div class="form-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Address</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter address" name="address" class="form-control" value="<?php echo set_value('address', $setting_row->address) ?>">
                                                    <?php echo form_error('address');?>
                                                 </div>

                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Email</label>
                                                <div class="col-md-6">
                                                    <input type="email" placeholder="enter email" name="email" class="form-control" value="<?php echo set_value('email', $setting_row->email) ?>">
                                                    <?php echo form_error('email');?>
                                                 </div>

                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Contact No</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter contact no" name="phone" class="form-control" value="<?php echo set_value('phone', $setting_row->phone) ?>">
                                                    <?php echo form_error('phone');?>
                                                 </div>

                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Facebook Link</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter facebook link" name="facebook" class="form-control" value="<?php echo set_value('facebook', $setting_row->facebook) ?>">
                                                    <?php echo form_error('facebook');?>
                                                 </div>

                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Twitter Link</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter twitter link" name="twitter" class="form-control" value="<?php echo set_value('twitter', $setting_row->twitter) ?>">
                                                    <?php echo form_error('twitter');?>
                                                 </div>

                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Instagram Link</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter instagram link" name="instagram" class="form-control" value="<?php echo set_value('instagram', $setting_row->instagram) ?>">
                                                    <?php echo form_error('instagram');?>
                                                 </div>

                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">About Brainpower</label>
                                                <div class="col-md-6">
                                                    <textarea name="about_brainpower" placeholder="enter about brainpower" class="form-control" rows="5">
                                                    <?= $setting_row->about_brainpower?></textarea>
                                                    <?php echo form_error('about_brainpower
                                                    ');?>
                                                </div>
                                         </div>
                                         <div class="form-group">
                                                <label class="control-label col-md-2">Did you know</label>
                                                <div class="col-md-6">
                                                    <textarea name="did_you_know" placeholder="enter details"class="form-control" rows="5"><?= $setting_row->did_you_know?></textarea>
                                                    <?php echo form_error('did_you_know');?>
                                                </div>
                                         </div>
                                         <div class="form-group">
                                                <label class="control-label col-md-2">Total Students</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter total no of students" name="total_stud" class="form-control" value="<?php echo set_value('total_stud', $setting_row->total_stud) ?>">
                                                    <?php echo form_error('total_stud');?>
                                                 </div>

                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Total Courses</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter total no of courses" name="total_courses" class="form-control" value="<?php echo set_value('total_courses', $setting_row->total_courses) ?>">
                                                    <?php echo form_error('total_courses');?>
                                                 </div>

                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Total Classrooms</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter total no of classrooms" name="total_class" class="form-control" value="<?php echo set_value('total_class', $setting_row->total_class) ?>">
                                                    <?php echo form_error('total_class');?>
                                                 </div>

                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Total Teacher</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter total no of teachers" name="total_teacher" class="form-control" value="<?php echo set_value('total_teacher', $setting_row->total_teacher) ?>">
                                                    <?php echo form_error('total_teacher');?>
                                                 </div>

                                            </div>
                                         
                                        <div class="form-actions" style="padding-top: 85px;padding-bottom: 20px;">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                                                            <?php echo form_reset(['class'=>'btn btn-primary','name'=>'reset','value'=>'Reset'])?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--./row-->
                
            </div>
        </div>