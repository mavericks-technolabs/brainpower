 <div class="page-wrapper">
            <div class="container-fluid">
                <!--.row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">Edit Gallery</div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                <?php echo form_hidden('user_id',$this->session->userdata('user_id')); ?>
                                <?php echo form_open_multipart('HomeController/update_gallery',['class'=>'form-horizontal form-bordered']); ?>
                                <?php echo form_hidden ('title_id',$this->session->userdata('title_id'))?>
                                <?php echo form_hidden('title_id',$img_row->title_id)?>
                                <?php echo form_hidden('img_id',$this->session->userdata('img_id')); ?>
                                <?php echo form_hidden('image_old_path',$this->session->userdata('image_old_path')) ?>
                               
                                 
                                    <!-- <form action="#" class="form-horizontal form-bordered"> -->
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Title</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter title" name="img_title" class="form-control" value="<?php echo set_value('img_title',$img_row->img_title); ?>">
                                                    <?php echo form_error('img_title');?>
                                                 </div>

                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Description</label>
                                                <div class="col-md-6">
                                                    <textarea name="img_desc" class="form-control" rows="5" ><?= $img_row->img_desc; ?></textarea>
                                                    <?php echo form_error('img_desc');?>
                                                </div>
                                         </div>
                                         <div class="form-group">
                                            <label class="control-label col-md-2">Select Picture</label>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" name="files[]" multiple/>
                                         </div>
                                        </div>
                                         <?php foreach ($gall_img as $gall_image):?>
                                             <?php
                                                     $base= base_url();
                                                     $img=$gall_image->image_path;
                                                     $path= $base.$img;

                                                                                     
                                             ?>  
                                            <a href="<?php echo base_url();?>delete_images/<?=$img_row->title_id?>/<?=$gall_image->img_id?>" class="btn btn-danger btn-xs">
                                            <i class="fa fa-trash-o "></i></a>  
                                            <img src="<?= $path ?>" width="150" height="150">
                                                            
                                                    <?php endforeach; ?>

                                        <div class="form-actions" style="padding-top: 85px;padding-bottom: 20px;">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                                                            <?php echo form_reset(['class'=>'btn btn-primary','name'=>'reset','value'=>'Reset'])?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--./row-->
                
            </div>
        </div>