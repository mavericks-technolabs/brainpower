 <div class="page-wrapper">
            <div class="container-fluid">
                <!--.row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">Edit Blogs</div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                 <?php echo form_open_multipart('HomeController/editblogs',['class'=>'form-horizontal form-bordered']); ?>
                                  <?php echo form_hidden('blog_id',$blog_row->blog_id) ?>
                                 <?php echo form_hidden('blog_img',$blog_row->blog_img) ?>
                                 
                                    <!-- <form action="#" class="form-horizontal form-bordered"> -->
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Title</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter title" name="blog_title" class="form-control" value="<?php echo set_value('blog_title', $blog_row->blog_title) ?>">
                                                    <?php echo form_error('blog_title');?>
                                                 </div>

                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Description</label>
                                                <div class="col-md-6">
                                                    <textarea name="blog_desc" placeholder="enter description" class="form-control" rows="5"><?= $blog_row->blog_desc; ?></textarea>
                                                    <?php echo form_error('blog_desc');?>
                                                </div>
                                         </div>
                                         <div class="form-group">
                                            <label class="control-label col-md-2">Select Picture</label>
                                            <div class="col-md-6">
                                               <?php echo form_upload(['class'=>'form-control','name'=>'userfile','type'=>'file','required']) ?>
                                         </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2">News Image</label>
                                            <div class="col-md-6">
                                                 <?php
                                                            $base = base_url()."uploads/blogs/";
                                                            $path = $base.$blog_row->blog_img;
                                                        ?>
                                                        <img src="<?= $path ?>" width="100" height="100">
                                               
                                         </div>
                                        </div> 
                                        <div class="form-actions" style="padding-top: 85px;padding-bottom: 20px;">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                                                            <?php echo form_reset(['class'=>'btn btn-primary','name'=>'reset','value'=>'Reset'])?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--./row-->
                
            </div>
        </div>