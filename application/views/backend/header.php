<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from www.cubictheme.ga/cubic-html/index2.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 09 Jan 2019 13:13:11 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
    <title>Brainpower</title>
    <!-- ===== Bootstrap CSS ===== -->
    <link href="<?php echo base_url();?>assets/backend/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- ===== Plugin CSS ===== -->
     <link href="<?php echo base_url();?>assets/backend/plugins/components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="../../cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/backend/plugins/components/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/backend/plugins/components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <link href='<?php echo base_url();?>assets/backend/plugins/components/fullcalendar/fullcalendar.css' rel='stylesheet'>
    <!-- ===== Animation CSS ===== -->
    <link href="<?php echo base_url();?>assets/backend/css/animate.css" rel="stylesheet">
    <!-- ===== Custom CSS ===== -->
    <link href="<?php echo base_url();?>assets/backend/css/style.css" rel="stylesheet">
    <!-- ===== Color CSS ===== -->
    <link href="<?php echo base_url();?>assets/backend/css/colors/default.css" id="theme" rel="stylesheet">
</head>
<script type="text/javascript">
    function start()
    {
        check();
    }
</script>

<body class="mini-sidebar" onload="start(this);">
    <!-- ===== Main-Wrapper ===== -->
    <div id="wrapper">
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <!-- ===== Top-Navigation ===== -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <a class="navbar-toggle font-20 hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class="fa fa-bars"></i>
                </a>
                <div class="top-left-part">
                    <a class="logo" href="index-2.html">
                        <b>
                            <img src="" />
                        </b>
                        <span>
                            <img src=""class="dark-logo" />
                        </span>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-left hidden-xs">
                    <li>
                        <a href="javascript:void(0)" class="sidebartoggler font-20 waves-effect waves-light"><i class="icon-arrow-left-circle"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
        <!-- ===== Top-Navigation-End ===== -->
        <!-- ===== Left-Sidebar ===== -->
        <aside class="sidebar">
            <div class="scroll-sidebar">
                <div class="user-profile">
                    <div class="dropdown user-pro-body">
                        <div class="profile-image">
                                        <?php  
                                            $base = base_url()."uploads/users/";
                                           $path = $base.$user_row->user_img;
                                         ?>
                            <img src="<?= $path ?>" alt="user-img" class="img-circle">
                            <a href="javascript:void(0);" class="dropdown-toggle u-dropdown text-blue" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <span class="badge badge-danger">
                                    <i class="fa fa-angle-down"></i>
                                </span>
                            </a>
                            <ul class="dropdown-menu animated flipInY">
                                <li><a href="<?php echo base_url('profile');?>"><i class="fa fa-user"></i> Profile</a></li>
                                <li><a href="<?php echo base_url('LoginController/logout');?>"><i class="fa fa-power-off"></i> Logout</a></li>
                            </ul>
                        </div>
                        <p class="profile-text m-t-15 font-16"><a href="javascript:void(0);"> <?php echo $user_row->firstname ?> <?php echo $user_row->lastname ?></a></p>
                    </div>
                </div>
                <nav class="sidebar-nav">
                    <ul id="side-menu">
                        <li>
                            <a class="active waves-effect" href="<?php echo base_url('dashboard')?>" aria-expanded="false"><i class="fa fa-dashboard"></i> <span class="hide-menu"> Dashboard</a>
                        </li>
                         <li>
                            <a class="active waves-effect" href="<?php echo base_url('all_users')?>" aria-expanded="false"><i class="fa fa-users"></i> <span class="hide-menu">Users</a>
                        </li>
                         <li>
                            <a class="active waves-effect" href="<?php echo base_url('all_gallery')?>" aria-expanded="false"><i class="fa fa-file-picture-o"></i> <span class="hide-menu">Gallery</a>
                        </li>
                        <li>
                            <a class="active waves-effect" href="<?php echo base_url('all_courses')?>" aria-expanded="false"><i class="fa  fa-graduation-cap"></i> <span class="hide-menu">Courses</a>
                        </li>
                        <li>
                            <a class="active waves-effect" href="<?php echo base_url('all_newsevents')?>" aria-expanded="false"><i class="fa   fa-calendar"></i> <span class="hide-menu">News & Events</a>
                        </li>
                        <li>
                            <a class="active waves-effect" href="<?php echo base_url('all_blogs')?>" aria-expanded="false"><i class="fa fa-folder"></i> <span class="hide-menu">Blogs</a>
                        </li>
                        <li>
                            <a class="active waves-effect" href="<?php echo base_url('all_stories')?>" aria-expanded="false"><i class="fa fa-bar-chart-o"></i> <span class="hide-menu">Success Stories</a>
                        </li>
                         <li>
                            <a class="active waves-effect" href="<?php echo base_url('all_aboutpage')?>" aria-expanded="false"><i class="fa fa-laptop"></i> <span class="hide-menu">About page</a>
                        </li>
                        <li>
                            <a class="active waves-effect" href="<?php echo base_url('all_global_settings')?>" aria-expanded="false"><i class="fa fa-institution (alias)"></i> <span class="hide-menu">Global Setting</a>
                        </li>
                        <li>
                            <a class="active waves-effect" href="<?php echo base_url('all_enquiries')?>" aria-expanded="false"><i class="fa fa-envelope"></i> <span class="hide-menu">Enquiries</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>