 <div class="page-wrapper">
            <div class="container-fluid">
                <!--.row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"> Add Courses</div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                
                    
                                    <?php echo form_open('HomeController/editcourse',['class'=>'form-horizontal form-bordered']); ?>
                                     <?php echo form_hidden('course_id',$cour_row->course_id) ?>
                                 
                                    <!-- <form action="#" class="form-horizontal form-bordered"> -->
                                         <div class="form-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Course</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter course title" name="course_name" class="form-control" value="<?php echo set_value('course_name', $cour_row->course_name) ?>">
                                                    <?php echo form_error('course_name');?>
                                                 </div>

                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Course Details</label>
                                                <div class="col-md-6">
                                                    <textarea name="course_details" class="form-control" rows="5"><?= $cour_row->course_details?></textarea>
                                                    <?php echo form_error('course_details');?>
                                                </div>
                                         </div>
                                         <div class="form-group">
                                                <label class="control-label col-md-2">Course Fees</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter course fees" name="course_fees" class="form-control" value="<?php echo set_value('course_fees', $cour_row->course_fees) ?>">
                                                    <?php echo form_error('course_fees');?>
                                                 </div>

                                            </div>
                                         
                                        <div class="form-actions" style="padding-top: 85px;padding-bottom: 20px;">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                                                            <?php echo form_reset(['class'=>'btn btn-primary','name'=>'reset','value'=>'Reset'])?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--./row-->
                
            </div>
        </div>