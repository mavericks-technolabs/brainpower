 <div class="page-wrapper">
            <div class="container-fluid">
                <!--.row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"> Add User</div>
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <?php echo form_hidden('user_id',$this->session->userdata('user_id')); 
                                    ?>
                                    <?php echo form_hidden('id',$this->session->userdata('id')); ?>
                                 <?php echo form_open_multipart('HomeController/adduser',['class'=>'form-horizontal form-bordered']); ?>
                                 
                                    <!-- <form action="#" class="form-horizontal form-bordered"> -->
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="control-label col-md-2">First Name</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter firstname" name="user_firstname" class="form-control" onkeypress="return lettersOnly(event)" value="<?php echo set_value('user_firstname') ?>">
                                                    <?php echo form_error('user_firstname');?>
                                                 </div>

                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Last Name</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter lastname" name="user_lastname" class="form-control" onkeypress="return lettersOnly(event)" value="<?php echo set_value('user_lastname') ?>"> 
                                                    <?php echo form_error('user_lastname');?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Email</label>
                                                <div class="col-md-6">
                                                    <input type="email" class="form-control" id="exampleInputEmail1" name="user_email" placeholder="enter email" value="<?php echo set_value('user_email') ?>"> 
                                                    <?php echo form_error('user_email');?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Contact No</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter contact no " name="user_contact" class="form-control" onkeypress="return isNumberKey(event)" maxlength="10" minlength="10" value="<?php echo set_value('user_contact') ?>">
                                                    <?php echo form_error('user_contact');?> 
                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                                function isNumberKey(evt)
                                                {
                                                    var charCode = (evt.which) ? evt.which : event.keyCode
                                                    if (charCode > 31 && (charCode < 48 || charCode > 57))
                                                    return false;
                                                    return true;
                                                }

                                               function lettersOnly(evt) {
                                                   evt = (evt) ? evt : event;
                                                   var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
                                                      ((evt.which) ? evt.which : 0));
                                                   if (charCode > 31 && (charCode < 65 || charCode > 90) &&
                                                      (charCode < 97 || charCode > 122)) {
                                                      alert("Enter letters only.");
                                                      return false;
                                                   }
                                                   return true;
                                                 }
                                            </script>
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Address</label>
                                                <div class="col-md-6">
                                                    <textarea name="user_address" class="form-control" rows="5" value="<?php echo set_value('user_address') ?>"></textarea>
                                                    <?php echo form_error('user_address');?>
                                                </div>
                                         </div>
                                         <div class="form-group">
                                            <label class="control-label col-md-2">Select Picture</label>
                                            <div class="col-md-6">
                                                <?php echo form_upload(['class'=>'form-control','name'=>'userfile','type'=>'file','required']) ?>
                                         </div>
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label col-md-2">Username</label>
                                                <div class="col-md-6">
                                                    <input type="text" placeholder="enter username" name="username" class="form-control" value="<?php echo set_value('username') ?>"> 
                                                    <?php echo form_error('username');?>
                                                     </div>
                                        </div>
                                        <div class="form-group">
                                                <label class="control-label col-md-2">Password</label>
                                                <div class="col-md-6">
                                                    <input type="password"  name="password" class="form-control" id="exampleInputpwd2" placeholder="enter password" value="<?php echo set_value('password') ?>">
                                                    <?php echo form_error('password');?>
                                        </div>
                                        <div class="form-actions" style="padding-top: 85px;padding-bottom: 20px;">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                                                            <?php echo form_reset(['class'=>'btn btn-primary','name'=>'reset','value'=>'Reset'])?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--./row-->
                
            </div>
        </div>