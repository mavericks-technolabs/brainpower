<style type="text/css">
   .view_more_btn{
   background: #fc7f0c;
   width: 128px;
   height: 39px;
   float: right;
   font-size: 13px;
   font-weight: bold;
   }
</style>
<!-- Page Header Start here -->
<section class="page-header section-notch">
   <div class="overlay">
      <div class="container">
         <h3>All Events</h3>
         <ul>
             <li><a href="<?php echo base_url('index');?>">Home</a></li>
            <li>-</li>
            <li><a href="<?php echo base_url('more_events');?>">Events</a></li>
         </ul>
      </div>
      <!-- container -->
   </div>
   <!-- overlay -->
</section>
<!-- page header -->
<!-- Page Header End here -->
<!-- Event Start here -->
<section class="event event-two padding-120">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="event-items">
               <div class="row">
                 <!--  <center>
                     <h3 style="color: #0d539b;margin-top: -54px;">Our Events</h3>
                  </center>
                  <br><br> -->
                  <div class="col-md-4 col-sm-12 col-xs-12">
                     <div class="event-item">
                        <div class="event-image">
                           <img src="<?php echo base_url();?>assets/images/event/event_01.jpg" alt="event image" class="img-responsive">
                           <div class="date">
                              <span>24</span>
                              <p>March</p>
                           </div>
                        </div>
                        <div class="event-content">
                           <h4>Imagination Classes</h4>
                           <ul>
                              <li><span><i class="fa fa-clock-o" aria-hidden="true"></i></span>08:00 am - 10:00 am</li>
                              <li><span><i class="fa fa-home" aria-hidden="true"></i></span>218 New Elephant Road Dhaka</li>
                           </ul>
                           <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                           <button class="view_more_btn" type="submit" data-toggle="modal" data-target="#event_more_one">View Details</button>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4 col-sm-12 col-xs-12">
                     <div class="event-item">
                        <div class="event-image">
                           <img src="<?php echo base_url();?>assets/images/event/event_05.jpg" alt="event image" class="img-responsive">
                           <div class="date">
                              <span>24</span>
                              <p>March</p>
                           </div>
                        </div>
                        <div class="event-content">
                           <h4>Imagination Classes</h4>
                           <ul>
                              <li><span><i class="fa fa-clock-o" aria-hidden="true"></i></span>08:00 am - 10:00 am</li>
                              <li><span><i class="fa fa-home" aria-hidden="true"></i></span>218 New Elephant Road Dhaka</li>
                           </ul>
                           <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                           <button class="view_more_btn" type="submit" data-toggle="modal" data-target="#event_more_two">View Details</button>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4 col-sm-12 col-xs-12">
                     <div class="event-item">
                        <div class="event-image">
                           <img src="<?php echo base_url();?>assets/images/event/event_02.jpg" alt="event image" class="img-responsive">
                           <div class="date">
                              <span>24</span>
                              <p>March</p>
                           </div>
                        </div>
                        <div class="event-content">
                           <h4>Imagination Classes</h4>
                           <ul>
                              <li><span><i class="fa fa-clock-o" aria-hidden="true"></i></span>08:00 am - 10:00 am</li>
                              <li><span><i class="fa fa-home" aria-hidden="true"></i></span>218 New Elephant Road Dhaka</li>
                           </ul>
                           <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                           <button class="view_more_btn" type="submit" data-toggle="modal" data-target="#event_more_three">View Details</button>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- row -->
            </div>
            <!-- event items --><br>
             <div class="event-items">
               <div class="row">
                 <!--  <center>
                     <h3 style="color: #0d539b;margin-top: -54px;">Our Events</h3>
                  </center>
                  <br><br> -->
                  <div class="col-md-4 col-sm-12 col-xs-12">
                     <div class="event-item">
                        <div class="event-image">
                           <img src="<?php echo base_url();?>assets/images/event/event_01.jpg" alt="event image" class="img-responsive">
                           <div class="date">
                              <span>24</span>
                              <p>March</p>
                           </div>
                        </div>
                        <div class="event-content">
                           <h4>Imagination Classes</h4>
                           <ul>
                              <li><span><i class="fa fa-clock-o" aria-hidden="true"></i></span>08:00 am - 10:00 am</li>
                              <li><span><i class="fa fa-home" aria-hidden="true"></i></span>218 New Elephant Road Dhaka</li>
                           </ul>
                           <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                           <button class="view_more_btn" type="submit" data-toggle="modal" data-target="#event_more_one">View Details</button>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4 col-sm-12 col-xs-12">
                     <div class="event-item">
                        <div class="event-image">
                           <img src="<?php echo base_url();?>assets/images/event/event_05.jpg" alt="event image" class="img-responsive">
                           <div class="date">
                              <span>24</span>
                              <p>March</p>
                           </div>
                        </div>
                        <div class="event-content">
                           <h4>Imagination Classes</h4>
                           <ul>
                              <li><span><i class="fa fa-clock-o" aria-hidden="true"></i></span>08:00 am - 10:00 am</li>
                              <li><span><i class="fa fa-home" aria-hidden="true"></i></span>218 New Elephant Road Dhaka</li>
                           </ul>
                           <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                           <button class="view_more_btn" type="submit" data-toggle="modal" data-target="#event_more_two">View Details</button>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4 col-sm-12 col-xs-12">
                     <div class="event-item">
                        <div class="event-image">
                           <img src="<?php echo base_url();?>assets/images/event/event_02.jpg" alt="event image" class="img-responsive">
                           <div class="date">
                              <span>24</span>
                              <p>March</p>
                           </div>
                        </div>
                        <div class="event-content">
                           <h4>Imagination Classes</h4>
                           <ul>
                              <li><span><i class="fa fa-clock-o" aria-hidden="true"></i></span>08:00 am - 10:00 am</li>
                              <li><span><i class="fa fa-home" aria-hidden="true"></i></span>218 New Elephant Road Dhaka</li>
                           </ul>
                           <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                           <button class="view_more_btn" type="submit" data-toggle="modal" data-target="#event_more_three">View Details</button>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- row -->
            </div>
         </div>
      </div><br>
    
   </div>
   <!-- container -->
</section>
<!-- event blog -->
<!-- event End here -->
<!-- First Modal -->
<div class="modal fade" id="event_more_one" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <center>
               <h4 class="modal-title" style="color: #ee257c;">Imagination Classes</h4>
            </center>
         </div>
         <div class="modal-body">
            <center><img src="<?php echo base_url();?>assets/images/event/event_01.jpg" alt="event image" class="img-responsive"></center>
            <br>
            <div class="event-content" style="line-height: 20px;">
               <center>
                  <p style="color: #1eb1ea;font-weight: bold;"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;&nbsp;08:00 am - 10:00 am </p>
               </center>
               <center>
                  <p style="color: #a30202; font-weight: bold;margin-top: -14px;"><i class="fa fa-home" aria-hidden="true"></i>&nbsp;&nbsp;218 New Elephant Road Dhaka</p>
               </center>
            </div>
            <div class="row">
               <div class="col-md-10 col-md-offset-1">
                  <p style="text-align: justify;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
   </div>
</div>
<!--First Modal -->
<!--Second Modal -->
<div class="modal fade" id="event_more_two" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <center>
               <h4 class="modal-title" style="color: #ee257c;">Imagination Classes</h4>
            </center>
         </div>
         <div class="modal-body">
            <center> <img src="<?php echo base_url();?>assets/images/event/event_05.jpg" alt="event image" class="img-responsive"></center>
            <br>
            <div class="event-content" style="line-height: 20px;">
               <center>
                  <p style="color: #1eb1ea;font-weight: bold;"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;&nbsp;08:00 am - 10:00 am </p>
               </center>
               <center>
                  <p style="color: #a30202; font-weight: bold;margin-top: -14px;"><i class="fa fa-home" aria-hidden="true"></i>&nbsp;&nbsp;218 New Elephant Road Dhaka</p>
               </center>
            </div>
            <div class="row">
               <div class="col-md-10 col-md-offset-1">
                  <p style="text-align: justify;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
   </div>
</div>
<!--Second Modal -->
<!--Third Modal -->
<div class="modal fade" id="event_more_three" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <center>
               <h4 class="modal-title" style="color: #ee257c;">Imagination Classes</h4>
            </center>
         </div>
         <div class="modal-body">
            <center><img src="<?php echo base_url();?>assets/images/event/event_02.jpg" alt="event image" class="img-responsive"></center>
            <br>
            <div class="event-content" style="line-height: 20px;">
               <center>
                  <p style="color: #1eb1ea;font-weight: bold;"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;&nbsp;08:00 am - 10:00 am </p>
               </center>
               <center>
                  <p style="color: #a30202; font-weight: bold;margin-top: -14px;"><i class="fa fa-home" aria-hidden="true"></i>&nbsp;&nbsp;218 New Elephant Road Dhaka</p>
               </center>
            </div>
            <div class="row">
               <div class="col-md-10 col-md-offset-1">
                  <p style="text-align: justify;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
   </div>
</div>
<!--Third Modal -->
