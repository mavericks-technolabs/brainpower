<style>
#more1 {display: none;}
#more2 {display: none;}
#more3 {display: none;}
</style>
<style type="text/css">
   .view_more_btn{
    background: #fc7f0c;
    width: 128px;
    height: 39px;
    float: right;
    font-size: 13px;
    font-weight: bold;
   }
</style>

<!-- Page Header Start here -->
<section class="page-header section-notch">
   <div class="overlay">
      <div class="container">
         <h3>Our News & Events</h3>
         <ul>
            <li><a href="index.php">Home</a></li>
            <li>-</li>
            <li>Mews & Events</li>
         </ul>
      </div>
      <!-- container -->
   </div>
   <!-- overlay -->
</section>
<!-- page header -->
<!-- Page Header End here -->
<!-- Event Start here -->
<section class="event event-two padding-120">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div class="event-items">
               <div class="row"> 
                  <center><h3 style="color: #0d539b;margin-top: -54px;">Our Events</h3></center><br><br>
                  <div class="col-md-4 col-sm-12 col-xs-12">
                     <div class="event-item">
                        <div class="event-image">
                           <img src="<?php echo base_url();?>assets/images/event/event_01.jpg" alt="event image" class="img-responsive">
                           <div class="date">
                              <span>24</span>
                              <p>March</p>
                           </div>
                        </div>
                        <div class="event-content">
                           <h4>Imagination Classes</h4>
                           <ul>
                              <li><span><i class="fa fa-clock-o" aria-hidden="true"></i></span>08:00 am - 10:00 am</li>
                              <li><span><i class="fa fa-home" aria-hidden="true"></i></span>218 New Elephant Road Dhaka</li>
                           </ul>
                           <p>Dratcaly novate fuly rarched an plication awesome theme education that plication creative theme education.</p>
                            <button class="view_more_btn" type="submit" data-toggle="modal" data-target="#event_one">View Details</button>
                        </div>
                     </div>
                  </div>
                   <div class="col-md-4 col-sm-12 col-xs-12">
                     <div class="event-item">
                        <div class="event-image">
                           <img src="<?php echo base_url();?>assets/images/event/event_05.jpg" alt="event image" class="img-responsive">
                           <div class="date">
                              <span>24</span>
                              <p>March</p>
                           </div>
                        </div>
                        <div class="event-content">
                           <h4>Imagination Classes</h4>
                           <ul>
                              <li><span><i class="fa fa-clock-o" aria-hidden="true"></i></span>08:00 am - 10:00 am</li>
                              <li><span><i class="fa fa-home" aria-hidden="true"></i></span>218 New Elephant Road Dhaka</li>
                           </ul>
                           <p>Dratcaly novate fuly rarched an plication awesome theme education that plication creative theme education.</p>
                           <button class="view_more_btn" type="submit" data-toggle="modal" data-target="#event_two">View Details</button>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4 col-sm-12 col-xs-12">
                     <div class="event-item">
                        <div class="event-image">
                           <img src="<?php echo base_url();?>assets/images/event/event_02.jpg" alt="event image" class="img-responsive">
                           <div class="date">
                              <span>24</span>
                              <p>March</p>
                           </div>
                        </div>
                        <div class="event-content">
                           <h4>Imagination Classes</h4>
                           <ul>
                              <li><span><i class="fa fa-clock-o" aria-hidden="true"></i></span>08:00 am - 10:00 am</li>
                              <li><span><i class="fa fa-home" aria-hidden="true"></i></span>218 New Elephant Road Dhaka</li>
                           </ul>
                           <p>Dratcaly novate fuly rarched an plication awesome theme education that plication creative theme education.</p>
                           <button class="view_more_btn" type="submit" data-toggle="modal" data-target="#event_three">View Details</button>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- row -->
            </div>
            <!-- event items -->
         </div>

      </div><br><br>
        <center><h3 style="color: #4a14b3;">Our News</h3></center><br><br>
        <div class="row">
         <div class="col-md-4">
            <center> <img src="<?php echo base_url();?>assets/images/event/event_04.jpg" alt="event image" class="img-responsive" style="width: 100%;" ></center><br>
            <center><h4 style="color: #92278f;">News Title One</h4></center><br>
            <p style="text-align: justify;">Asertively recaptiualize business benefits rofesionally timely data with and premium timely with premium products sertively recaptiualize business benefits rofesionally timely data an with and premium timely awesome premium products.<span id="dots1">...</span><span id="more1">It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span><label onclick="myFunction('dots1','more1','myBtn1')" id="myBtn1" style="color: #07509a;">Read more</label></p>
            <p style="float: right;"><i class="fa fa-calendar-o" aria-hidden="true"></i>&nbsp;&nbsp;01-01-2019</p>
         </div>
         <div class="col-md-4">
            <center> <img src="<?php echo base_url();?>assets/images/event/event_05.jpg" alt="event image" class="img-responsive" style="width: 100%;" ></center><br>
            <center><h4 style="color: #a40202;">News Title Two</h4></center><br>
           <p style="text-align: justify;">Asertively recaptiualize business benefits rofesionally timely data with and premium timely with premium products sertively recaptiualize business benefits rofesionally timely data an with and premium timely awesome premium products.<span id="dots2">...</span><span id="more2">It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span><label onclick="myFunction('dots2','more2','myBtn2')" id="myBtn2" style="color: #07509a;">Read more</label></p>
           <p style="float: right;"><i class="fa fa-calendar-o" aria-hidden="true"></i>&nbsp;&nbsp;01-01-2019</p>
         </div>
         <div class="col-md-4">
            <center> <img src="<?php echo base_url();?>assets/images/event/event_06.jpg" alt="event image" class="img-responsive" style="width: 100%;" ></center><br>
            <center><h4 style="color: #2abdf2;">News Title Three</h4></center><br>
           <p style="text-align: justify;">Asertively recaptiualize business benefits rofesionally timely data with and premium timely with premium products sertively recaptiualize business benefits rofesionally timely data an with and premium timely awesome premium products.<span id="dots3">...</span><span id="more3">It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span><label onclick="myFunction('dots3','more3','myBtn3')" id="myBtn3" style="color: #07509a;">Read more</label></p>
           <p style="float: right;"><i class="fa fa-calendar-o" aria-hidden="true"></i>&nbsp;&nbsp;01-01-2019</p>
            <p></p>
         </div>
        </div>
   </div>
   <!-- container -->
</section>
<!-- event blog -->
<!-- event End here -->
<!-- First Modal -->
  <div class="modal fade" id="event_one" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         <center> <h4 class="modal-title" style="color: #ee257c;">Imagination Classes</h4></center>
        </div>
        <div class="modal-body">
          <center><img src="<?php echo base_url();?>assets/images/event/event_01.jpg" alt="event image" class="img-responsive"></center><br>
           <div class="event-content" style="line-height: 20px;">

            <center><p style="color: #1eb1ea;font-weight: bold;"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;&nbsp;08:00 am - 10:00 am </p></center>
           <center><p style="color: #a30202; font-weight: bold;margin-top: -14px;"><i class="fa fa-home" aria-hidden="true"></i>&nbsp;&nbsp;218 New Elephant Road Dhaka</p></center>
      </div>
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
               <p style="text-align: justify;">Dratcaly novate fuly rarched an plication awesome theme education that plication creative theme education.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
</div>
  <!--First Modal -->

  <!--Second Modal -->
  <div class="modal fade" id="event_two" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         <center> <h4 class="modal-title" style="color: #ee257c;">Imagination Classes</h4></center>
        </div>
        <div class="modal-body">
          <center> <img src="<?php echo base_url();?>assets/images/event/event_05.jpg" alt="event image" class="img-responsive"></center><br>
           <div class="event-content" style="line-height: 20px;">

            <center><p style="color: #1eb1ea;font-weight: bold;"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;&nbsp;08:00 am - 10:00 am </p></center>
           <center><p style="color: #a30202; font-weight: bold;margin-top: -14px;"><i class="fa fa-home" aria-hidden="true"></i>&nbsp;&nbsp;218 New Elephant Road Dhaka</p></center>
      </div>
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
               <p style="text-align: justify;">Dratcaly novate fuly rarched an plication awesome theme education that plication creative theme education.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
</div>
  <!--Second Modal -->


   <!--Third Modal -->
  <div class="modal fade" id="event_three" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         <center> <h4 class="modal-title" style="color: #ee257c;">Imagination Classes</h4></center>
        </div>
        <div class="modal-body">
          <center><img src="<?php echo base_url();?>assets/images/event/event_02.jpg" alt="event image" class="img-responsive"></center><br>
           <div class="event-content" style="line-height: 20px;">

            <center><p style="color: #1eb1ea;font-weight: bold;"><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;&nbsp;08:00 am - 10:00 am </p></center>
           <center><p style="color: #a30202; font-weight: bold;margin-top: -14px;"><i class="fa fa-home" aria-hidden="true"></i>&nbsp;&nbsp;218 New Elephant Road Dhaka</p></center>
      </div>
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
               <p style="text-align: justify;">Dratcaly novate fuly rarched an plication awesome theme education that plication creative theme education.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
</div>
  <!--Third Modal -->

  <script>
function myFunction(var1,var2,var3) {
  var dots = document.getElementById(var1);
  var moreText = document.getElementById(var2);
  var btnText = document.getElementById(var3);

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Read more"; 
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Read less"; 
    moreText.style.display = "inline";
  }
}
</script>
