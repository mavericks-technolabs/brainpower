<!-- Page Header Start here -->
<section class="page-header section-notch">
   <div class="overlay">
      <div class="container">
         <h3>About Us</h3>
         <ul>
            <li><a href="<?php echo base_url('index');?>">Home</a></li>
            <li>-</li>
            <li><a href="<?php echo base_url('about');?>">About</a></li>
         </ul>
      </div>
      <!-- container -->
   </div>
   <!-- overlay -->
</section>
<!-- page header -->
<!-- Page Header End here -->
<!-- teacher-details start here -->
<section class="teacher-details padding-120">
   <div class="container">
      <div class="row">
         <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="teacher-image">
               <img src="<?php echo base_url();?>assets/images/about/unknown.jpg" alt=" image" class="img-responsive"><br>
               <ul class="teacher-address">
                  <li><span><i class="fa fa-home" aria-hidden="true" style="padding: 5px;"></i></span>Pune, Maharashtra</li>
                  <li><span><i class="fa fa-phone" aria-hidden="true" style="padding: 5px;"></i></span>+0101010111</li>
                  <li><span><i class="fa fa-envelope-o" aria-hidden="true" style="padding: 5px;"></i></span>contact@brainpower.com</li>
                  <!-- <li><span><i class="fa fa-globe" aria-hidden="true"></i></span>www.LabArtisan.com</li> -->
               </ul>
               <ul class="social-default">
                  <li><a href="#"><i class="fa fa-facebook" aria-hidden="true" style="margin-top: 13px;"></i></a></li>
                  <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true" style="margin-top: 13px;"></i></a></li>
                  <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true" style="margin-top: 13px;"></i></a></li>
                  <li><a href="#"><i class="fa fa-twitter" aria-hidden="true" style="margin-top: 13px;"></i></a></li>
                  <!-- <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true" style="margin-top: 13px;"></i></a></li> -->
               </ul>
            </div>
         </div>
         <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="teacher-content">
               <h4>Mrs. Poonam Somdutt Lad </h4>
               <span>Managing Director</span>
               <p style="text-align: justify;">I, Mrs. Poonam Somdutt Lad , was in the IT industry for 12 years ,I’m a BSC (Botany ) Graduate and MBA (HR) by qualification. They say life has it’s own twists and turns some turns you know and some come as a surprise so have been the case with me .After having a career with IT industry for about 12 years and then all of a sudden taking sabbatical to take care and be full time with the family was not an easy call but now when I look back it was my best decision. The fun is to break the monotony and move to your higher planned and designed destiny. Little I knew this was the beginning of a new found world where I would be having a complete family time and exploring areas which I never thought I would ever. Enjoyed my time setting up my garden ,learning to cook ,the most interesting part I could pursue my long stranded interest  towards the subject of psychology  as the mystic world and reading people always mesmerised me. Also, what really interest and got me curious towards this wonderful topic of midbrain power was mainly because one would hear from lot of people about feeling exhausted, tired, loosing memory, feeling lonely some complains around kids they would not listen, repsect for elders is not seen, very aggressive, fearful, lack decision making capacity etc. On studying various topics I realized that it’s mainly coming from an area of imbalance, discontentment messages being sent to your brain repeatedly, brain has literally forgotten it’s stop button once it’s get onto thinking pattern. This made me take my hobby to the next level and got myself certified … <br>
                  <b style="color: #365899;">Child and Adoloscent Psycology: Enhancing Potential from (JPIP)<br>
                  DMIT <br>
                  REIKI Level 1 & Level 2 <br>
                  Chakra Meditation <br>
                  ISO 90012008
                  </b>
               </p>
            </div>
         </div>
      </div>
      <!-- row -->
      <div class="teacher-statement">
         <h4 style="font-size: 24px;">Personal Statement</h4>
         <p style="color: #1d1818;"><i>I personally believe if you train your brain it can take care of you in all circumstances, and put you away from feeling victimised , self-pity and other negative emotions. Positive thinking helps build good relationship, effective communication by balancing our emotional quotient And hence I created this platform named Brain Power to develop the right confidence level and attitude to be a winner in life.</i> </p>
         <p>They say the best knowledge is when you share with the society and help them benefit so felt like creating a platform which will cater and help resolve problems and help people and mainly children to understand the significance and infinite and immense power of brain. True challenge of our brain is when it learns to accept a new change positive or negative and keep us in balanced, active and enthusiastic way and help us enjoy every moment of life. It’s well said that positive thinking is more than just a tagline. It changes the way we behave. And I firmly believe that when I’m positive, it not only makes me better but it also makes those around me better. </p>
      </div>
   </div>
   <!-- container -->
</section>
<!-- teacher-details -->
<!-- teacher-details end here -->
<!-- Achievements Start here -->
<section class="achievements section-notch">
   <div class="overlay padding-120">
      <div class="container">
         <div class="row">
            <div class="col-md-3 col-sm-3 col-12">
               <div class="achievement-item">
                  <i class="icon flaticon-student"></i>
                  <span class="counter">320</span><span>+</span>
                  <p>Total Students</p>
               </div>
               <!-- achievement item -->
            </div>
            <div class="col-md-3 col-sm-3 col-12">
               <div class="achievement-item">
                  <i class="icon flaticon-construction"></i>
                  <span class="counter">12</span>
                  <p>Class Rooms</p>
               </div>
               <!-- achievement item -->
            </div>
            <div class="col-md-3 col-sm-3 col-12">
               <div class="achievement-item">
                  <i class="icon flaticon-school-bus"></i>
                  <span class="counter">24</span>
                  <p>Schools bus</p>
               </div>
               <!-- achievement item -->
            </div>
            <div class="col-md-3 col-sm-3 col-12">
               <div class="achievement-item">
                  <i class="icon flaticon-people"></i>
                  <span class="counter">15</span>
                  <p>Total Teachers</p>
               </div>
               <!-- achievement item -->
            </div>
         </div>
         <!-- row -->
      </div>
      <!-- container -->
   </div>
   <!-- overlay -->
</section>
<!-- achievements -->
<!-- Achievements End here --><br><br><br><br><br>
<div class="container">
   <div class="col-md-10 col-md-offset-1">
      <div class="row" style="border: solid #b3b0b06e 2px; box-shadow: 3px 3px 1px #b1acac;">
         <div class="col-md-6" style="background-image: url('assets/images/about/m.jpg');height: 300px;background-size: cover;">
         </div>
         <div class="col-md-6">
            <br>
            <center>
               <h4 style="color: #ee257c;">Our Vision</h4>
            </center>
            <br>
            <p>To empower generations with a sense of belief, confidence and competitiveness to create their well-being and more importantly a sense of oneness to contribute to the society.</p>
         </div>
      </div>
      <!--  --><br>
      <div class="row" style="border: solid #b3b0b06e 2px; box-shadow: 3px 3px 1px #b1acac;">
         <div class="col-md-6">
            <br>
            <center>
               <h4 style="color: #ee257c;">Our Mission</h4>
            </center>
            <br>
            <p> To conduct Mid-Brain Activation Program which can help the participants to use their right and left brain functions simultaneously and effectively. This will enable the participants to become competitive in today’s complex environment and at the same time instil within oneself a sense of responsibility to contributes towards our society</p>
         </div>
         <div class="col-md-6" style="background-image: url('assets/images/banner/banner_06.jpg');height: 300px;background-size: cover;">
         </div>
      </div>
   </div>
</div>
<br><br><br>

<div class="container">
   
      <center>
         <h3 style="color: #92278f;">Certifications</h3>
      </center>
      <section class="gallery padding-120">
   <div class="container">
      <div class="gallery-items">
         <div class="gallery-item branding packing">
            <div class="gallery-image">
               <img src="<?php echo base_url();?>assets/images/about/cert.jpg" alt="gallery image" class="img-responsive" style="height: 270px; border: solid #9e9c9e 2px;">
               <div class="gallery-overlay">
                  <div class="bg"></div>
               </div>
               <div class="gallery-content">
                  <a href="<?php echo base_url();?>assets/images/about/cert.jpg" data-rel="lightcase:myCollection"><i class="icon flaticon-expand"></i></a>
               </div>
            </div>
         </div>

         <div class="gallery-item branding packing">
            <div class="gallery-image">
               <img src="<?php echo base_url();?>assets/images/about/cert2.jpg" alt="gallery image" class="img-responsive" style="height: 270px; border: solid #9e9c9e 2px;">
               <div class="gallery-overlay">
                  <div class="bg"></div>
               </div>
               <div class="gallery-content">
                  <a href="<?php echo base_url();?>assets/images/about/cert2.jpg" data-rel="lightcase:myCollection"><i class="icon flaticon-expand"></i></a>
               </div>
            </div>
         </div>

         <div class="gallery-item branding packing">
            <div class="gallery-image">
               <img src="<?php echo base_url();?>assets/images/about/cert3.jpg" alt="gallery image" class="img-responsive" style="height: 270px; border: solid #9e9c9e 2px;">
               <div class="gallery-overlay">
                  <div class="bg"></div>
               </div>
               <div class="gallery-content">
                  <a href="<?php echo base_url();?>assets/images/about/cert3.jpg" data-rel="lightcase:myCollection"><i class="icon flaticon-expand"></i></a>
               </div>
            </div>
         </div>

         <div class="gallery-item branding packing">
            <div class="gallery-image">
               <img src="<?php echo base_url();?>assets/images/about/cert4.jpg" alt="gallery image" class="img-responsive" style="height: 270px; border: solid #9e9c9e 2px;">
               <div class="gallery-overlay">
                  <div class="bg"></div>
               </div>
               <div class="gallery-content">
                  <a href="<?php echo base_url();?>assets/images/about/cert4.jpg" data-rel="lightcase:myCollection"><i class="icon flaticon-expand"></i></a>
               </div>
            </div>
         </div>
         
         <div class="gallery-item branding packing">
            <div class="gallery-image">
               <img src="<?php echo base_url();?>assets/images/about/cert.jpg" alt="gallery image" class="img-responsive" style="height: 270px; border: solid #9e9c9e 2px;">
               <div class="gallery-overlay">
                  <div class="bg"></div>
               </div>
               <div class="gallery-content">
                  <a href="<?php echo base_url();?>assets/images/about/cert.jpg" data-rel="lightcase:myCollection"><i class="icon flaticon-expand"></i></a>
               </div>
            </div>
         </div>

         <div class="gallery-item branding packing">
            <div class="gallery-image">
               <img src="<?php echo base_url();?>assets/images/about/cert2.jpg" alt="gallery image" class="img-responsive" style="height: 270px; border: solid #9e9c9e 2px;">
               <div class="gallery-overlay">
                  <div class="bg"></div>
               </div>
               <div class="gallery-content">
                  <a href="<?php echo base_url();?>assets/images/about/cert2.jpg" data-rel="lightcase:myCollection"><i class="icon flaticon-expand"></i></a>
               </div>
            </div>
         </div>

      </div>
      <!-- gallery items -->
   </div>
   <!-- container -->
</section>
 <!--  <div class="row">
    <div class="col-md-4">
       <img src="<?php echo base_url();?>assets/images/about/cert.jpg">
    </div>
    <div class="col-md-4">
       <img src="<?php echo base_url();?>assets/images/about/cert2.jpg">
    </div>
    <div class="col-md-4">
       <img src="<?php echo base_url();?>assets/images/about/cert3.jpg">
    </div>
   </div><br> -->
   <!--  <div class="row">
    <div class="col-md-4">
       <img src="<?php echo base_url();?>assets/images/about/cert2.jpg">
    </div>
    <div class="col-md-4">
       <img src="<?php echo base_url();?>assets/images/about/cert3.jpg">
    </div>
    <div class="col-md-4">
       <img src="<?php echo base_url();?>assets/images/about/cert.jpg">
    </div>
   </div> -->
</div>
