<style type="text/css">
   .view_more_btn{
   background: #fc7f0c;
   width: 128px;
   height: 39px;
   float: right;
   font-size: 13px;
   font-weight: bold;
   }
</style>
<!-- Page Header Start here -->
<section class="page-header section-notch">
   <div class="overlay">
      <div class="container">
         <h3>All News</h3>
         <ul>
            <li><a href="index.php">Home</a></li>
            <li>-</li>
            <li>Mews & Events</li>
         </ul>
      </div>
      <!-- container -->
   </div>
   <!-- overlay -->
</section>
<!-- page header -->
<!-- Page Header End here -->
<!-- Event Start here -->
<section class="event event-two padding-120">
   <div class="container">
      <div class="row">
         <div class="col-md-4">
            <center> <img src="<?php echo base_url();?>assets/images/event/event_04.jpg" alt="event image" class="img-responsive" style="width: 100%;" ></center>
            <br>
            <center>
               <h4 style="color: #92278f;">News Title One</h4>
            </center>
            <br>
            <p style="text-align: justify;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <div class="row">
               <div class="col-md-6">
                  <p><i class="fa fa-calendar-o" aria-hidden="true"></i>&nbsp;&nbsp;01-01-2019</p>
               </div>
               <div class="col-md-6">
                  <button class="view_more_btn" type="submit" data-toggle="modal" data-target="#news_one">View Details</button>
               </div>
            </div>
         </div>
         <div class="col-md-4">
            <center> <img src="<?php echo base_url();?>assets/images/event/event_05.jpg" alt="event image" class="img-responsive" style="width: 100%;" ></center>
            <br>
            <center>
               <h4 style="color: #a40202;">News Title Two</h4>
            </center>
            <br>
            <p style="text-align: justify;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <div class="row">
               <div class="col-md-6">
                  <p><i class="fa fa-calendar-o" aria-hidden="true"></i>&nbsp;&nbsp;01-01-2019</p>
               </div>
               <div class="col-md-6">
                  <button class="view_more_btn" type="submit" data-toggle="modal" data-target="#news_two">View Details</button>
               </div>
            </div>
         </div>
         <div class="col-md-4">
            <center> <img src="<?php echo base_url();?>assets/images/event/event_06.jpg" alt="event image" class="img-responsive" style="width: 100%;" ></center>
            <br>
            <center>
               <h4 style="color: #2abdf2;">News Title Three</h4>
            </center>
            <br>
            <p style="text-align: justify;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            <div class="row">
               <div class="col-md-6">
                  <p><i class="fa fa-calendar-o" aria-hidden="true"></i>&nbsp;&nbsp;01-01-2019</p>
               </div>
               <div class="col-md-6">
                  <button class="view_more_btn" type="submit" data-toggle="modal" data-target="#news_three">View Details</button>
               </div>
            </div>
            <p></p>
         </div>
      </div><br>
   </div>
   <!-- container -->
</section>
<!-- event blog -->
<!-- event End here -->
<!-- First Modal -->
<div class="modal fade" id="news_one" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <center>
               <h4 class="modal-title" style="color: #ee257c;">News Title One</h4>
            </center>
         </div>
         <div class="modal-body">
            <center><img src="<?php echo base_url();?>assets/images/event/event_04.jpg" alt="event image" class="img-responsive"></center>
            <br>
            <div class="event-content" style="line-height: 20px;">
               <center>
                  <p style="color: #61235e;font-weight: bold;"><i class="fa fa-calendar-o" aria-hidden="true"></i> &nbsp;&nbsp; Date:&nbsp;&nbsp;16-01-2019 </p>
               </center>
            </div>
            <div class="row">
               <div class="col-md-10 col-md-offset-1">
                  <p style="text-align: justify;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
   </div>
</div>
<!--First Modal -->
<!-- First Modal -->
<div class="modal fade" id="news_two" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <center>
               <h4 class="modal-title" style="color: #ee257c;">News Title Two</h4>
            </center>
         </div>
         <div class="modal-body">
            <center><img src="<?php echo base_url();?>assets/images/event/event_05.jpg" alt="event image" class="img-responsive"></center>
            <br>
            <div class="event-content" style="line-height: 20px;">
               <center>
                  <p style="color: #61235e;font-weight: bold;"><i class="fa fa-calendar-o" aria-hidden="true"></i> &nbsp;&nbsp; Date:&nbsp;&nbsp;16-01-2019 </p>
               </center>
            </div>
            <div class="row">
               <div class="col-md-10 col-md-offset-1">
                  <p style="text-align: justify;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
   </div>
</div>
<!--First Modal -->
<!-- First Modal -->
<div class="modal fade" id="news_three" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <center>
               <h4 class="modal-title" style="color: #ee257c;">News Title Three</h4>
            </center>
         </div>
         <div class="modal-body">
            <center><img src="<?php echo base_url();?>assets/images/event/event_06.jpg" alt="event image" class="img-responsive"></center>
            <br>
            <div class="event-content" style="line-height: 20px;">
               <center>
                  <p style="color: #61235e;font-weight: bold;"><i class="fa fa-calendar-o" aria-hidden="true"></i> &nbsp;&nbsp; Date:&nbsp;&nbsp;16-01-2019 </p>
               </center>
            </div>
            <div class="row">
               <div class="col-md-10 col-md-offset-1">
                  <p style="text-align: justify;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
   </div>
</div>
<!--First Modal -->