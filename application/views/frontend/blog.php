<!-- Page Header Start here -->
<section class="page-header section-notch">
   <div class="overlay">
      <div class="container">
         <h3>Our Blog</h3>
         <ul>
            <li><a href="<?php echo base_url('index');?>">Home</a></li>
            <li>-</li>
            <li><a href="<?php echo base_url('blog');?>">Blog</a></li>
         </ul>
            
      </div>
      <!-- container -->
   </div>
   <!-- overlay -->
</section>
<!-- page header -->
<!-- Page Header End here --><br><br>
<!-- Blog Post Start here -->
<section class="classes padding-120">
   <div class="container">
      <div class="row">
         <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="class-item">
               <div class="image">
                  <img src="<?php echo base_url();?>assets/images/classes/class_01.jpg" alt="class image" class="img-responsive">
               </div>
               <div class="content">
                  <h4><a href="<?php echo base_url('blog_details');?>">Mid Brain</a></h4>
                  <p><span><i class="fa fa-calendar-o" aria-hidden="true"></i></span>&nbsp;&nbsp;16-01-2019</p>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
               </div>
               <div class="address">
                  <center>
                     <a href=" <?php echo base_url('blog_details');?>" style="text-decoration: none;">
                        <p style="font-weight: bold;">View Details</p>
                     </a>
                  </center>
               </div>
            </div>
            <!-- class item -->
         </div>
         <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="class-item">
               <div class="image">
                  <img src="<?php echo base_url();?>assets/images/classes/class_02.jpg" alt="class image" class="img-responsive">
               </div>
               <div class="content">
                  <h4><a href="<?php echo base_url('blog_details');?>">DMIT</a></h4>
                  <p><span><i class="fa fa-calendar-o" aria-hidden="true"></i></span>&nbsp;&nbsp;16-01-2019</p>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
               </div>
               <div class="address">
                  <center>
                     <a href="<?php echo base_url('blog_details');?>" style="text-decoration: none;">
                        <p style="font-weight: bold;">View Details</p>
                     </a>
                  </center>
               </div>
            </div>
            <!-- class item -->
         </div>
         <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="class-item">
               <div class="image">
                  <img src="<?php echo base_url();?>assets/images/classes/class_03.jpg" alt="class image" class="img-responsive">
               </div>
               <div class="content">
                  <h4><a href="<?php echo base_url('blog_details');?>">Chatra Meditation</a></h4>
                  <p><span><i class="fa fa-calendar-o" aria-hidden="true"></i></span>&nbsp;&nbsp;16-01-2019</p>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
               </div>
               <div class="address">
                  <center>
                     <a href="<?php echo base_url('blog_details');?>" style="text-decoration: none;">
                        <p style="font-weight: bold;">View Details</p>
                     </a>
                  </center>
               </div>
            </div>
            <!-- class item -->
         </div>
      </div>
      <!-- row -->
    
      </div>

   </div>
</section>
<!-- Blog Post End here -->
