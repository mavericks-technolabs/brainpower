Page Header Start here -->
<section class="page-header section-notch">
   <div class="overlay">
      <div class="container">
         <h3>DMIT</h3>
         <ul>
             <li><a href="<?php echo base_url('index');?>">Home</a></li>
            <li>-</li>
            <li><a href="<?php echo base_url('dmit');?>">Courses</a></li>
         </ul>
      </div>
      <!-- container -->
   </div>
   <!-- overlay -->
</section>
<!-- page header -->
<!-- Page Header End here -->
<!-- Blog Post Start here -->
<section class="singel-class padding-120">
   <div class="container">
   <div class="row">
      <div class="col-md-8">
         <div class="single-post">
            <div class="post-image">
               <img src="<?php echo base_url();?>assets/images/classes/class-single.jpg" alt="post image" class="img-responsive">
            </div>
            <div class="post-content">
               <h5>DMIT</h5>
               <p>Dermatoglyphics (from ancient Greek derma = skin, glyph = carving) is the scientific study of fingerprints. The term was coined by Dr. Harold Cummins, the father of American fingerprint analysis. The process of fingerprint identification had been used for several hundred years now.
                  Scientists researched skin ridge patterns and established that the fingerprint patterns actually develop in the womb and are fully formed by the fourth month of pregnancy.
               </p>
               <h5>Why DMIT?</h5>
               <p>As you know, the Indian education system, focuses more on cramming information in the minds of our children, rather than giving them true knowledge, which they can use to shape a successful career. Our system also overlooks the specific learning needs of each child, who is unique, resulting in immense stress.</p>
               <p>Shockingly, above 12,000 students commit suicides in India due to exam related stress. This number is alarming and makes us wonder where our system is going wrong!!!!!!!</p>
               <p>You, as parents need to take an active role in understanding the special intrinsic potential of your child, and design suitable learning methods around your child needs. This can be achieved through DMIT.  DMI Assessment technique has been developed by scientists and research experts from World renowned universities and is based on knowledge from Genetics, Embryology, Dermatoglyphics, Psychology and Neuroscience. </p>
            </div>
         </div>
         <!-- single post -->
      </div>
      <div class="col-md-4">
         <div class="sidebar">
            <div class="sidebar-item">
               <form>
                  <input type="text" name="text" placeholder="Search Your Catagorie...">
                  <button><i class="fa fa-search" aria-hidden="true"></i></button>
               </form>
            </div>
            <!-- sidebar item -->
            <div class="sidebar-item">
               <ul class="class-details">
                  <li>
                     <div class="name"><i class="flaticon-pencil"></i>Start Date</div>
                     <div class="info">24 March 2017</div>
                  </li>
                  <li>
                     <div class="name"><i class="flaticon-student"></i>Years Old</div>
                     <div class="info">6-8 Years</div>
                  </li>
                  <li>
                     <div class="name"><i class="flaticon-symbols"></i>Class Size</div>
                     <div class="info">20-30 Kids</div>
                  </li>
                  <li>
                     <div class="name"><i class="flaticon-school-bus"></i>Carry Time</div>
                     <div class="info">5 Hours/6 Days</div>
                  </li>
                  <li>
                     <div class="name"><i class="flaticon-book"></i>Coures Duration</div>
                     <div class="info">10-12 Month</div>
                  </li>
                  <li>
                     <div class="name"><i class="flaticon-alarm-clock"></i>Class Time</div>
                     <div class="info">9:30am-5:30pm</div>
                  </li>
                  <li>
                     <div class="name"><i class="flaticon-like-2"></i>Rating</div>
                     <div class="info rating">
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                     </div>
                  </li>
                  <li>
                     <div class="name"><i class="flaticon-money-bag"></i>Tution Free</div>
                     <div class="info">$ 250.00</div>
                  </li>
               </ul>
            </div>
            <!-- sidebar item -->
         </div>
      </div>
   </div>
</div>
   <!--  --><br>
   <div class="container">
      <div class="col-md-12">
         <p>After years of extensive research he defined 8 intelligences   which are – </p>
         <!--  --><br>
         <!-- <div class="col-md-10 col-md-offset-1"> -->
            <div class="row">
               <div class="col-md-5">
                  <center> <img src="<?php echo base_url();?>assets/images/gallery/gallery_07.jpg" style="width: 339px; height: 264px;"></center>
               </div>
               <div class="col-md-7">
                  <center>
                     <h4 style="color: #ee257c;">Visual-Spatial Intelligence</h4>
                  </center>
                  <br>
                  <ul style="list-style-type:circle; margin-left: 20px;">
                     <li>The ability to think in pictures and visualize future results</li>
                     <li>The ability to imagine things in your mind’s eye</li>
                     <li> The ability to perceive spatial information</li>
                     <li>Those with strong spatial intelligence are often proficient at solving puzzles. This intelligence is high in artists, photographers, pilots, painters and architects.</li>
                  </ul>
               </div>
            </div>
            <!--  --><br><br>
            <div class="row">
               <div class="col-md-7">
                  <center>
                     <h4 style="color: #14b7f1;">Verbal Linguistic Intelligence</h4>
                  </center>
                  <br>
                  <ul style="list-style-type:circle; margin-left: 20px;">
                     <li>The ability to read, write, and communicate with words</li>
                     <li>The ability to use language to express one’s thoughts and to understand other people orally or in writing</li>
                     <li>They tend to learn best by reading, taking notes, listening to lectures, and discussion and debate.</li>
                     <li>This intelligence is high in writers, lawyers, philosophers, journalists, politicians and teachers.</li>
                  </ul>
               </div>
               <div class="col-md-5">
                  <center> <img src="<?php echo base_url();?>assets/images/gallery/gallery_04.jpg" style="width: 339px; height: 264px;"></center>
               </div>
            </div>
            <!--  --><br><br>
            <div class="row">
               <div class="col-md-5">
                  <center> <img src="<?php echo base_url();?>assets/images/gallery/gallery_15.jpg" style="width: 339px; height: 264px;"></center>
               </div>
               <div class="col-md-7">
                  <center>
                     <h4 style="color: green;">Intra Personal Intelligence</h4>
                  </center>
                  <br>
                  <ul style="list-style-type:circle; margin-left: 20px;">
                     <li>The ability to distinguish among an individual’s own feelings, to accurate mental models of themselves, and use them to make decisions about life.</li>
                     <li>The capacity to know one’s self, Careers which suit those with this intelligence include philosophers, psychologists, theologians, writers and scientists.</li>
                  </ul>
               </div>
            </div>
            <!--  --><br><br>
            <div class="row">
               <div class="col-md-7">
                  <center>
                     <h4 style="color: #a40202;">Logical Mathematical Intelligence</h4>
                  </center>
                  <br>
                  <ul style="list-style-type:circle; margin-left: 20px;">
                     <li>The ability to reason and calculate</li>
                     <li>Enables individuals to use and appreciate abstract relations</li>
                     <li>The ability to manipulate numbers, quantities, operations, etc.</li>
                     <li>Many scientists, mathematicians, engineers, doctors and economists function in this level of intelligences.</li>
                  </ul>
               </div>
               <div class="col-md-5">
                  <center> <img src="<?php echo base_url();?>assets/images/gallery/gallery_bg_13.jpg" style="width: 339px; height: 264px;"></center>
               </div>
            </div>
            <!--  --><br><br>
            <div class="row">
               <div class="col-md-5">
                  <center> <img src="<?php echo base_url();?>assets/images/gallery/gallery_08.jpg" style="width: 339px; height: 264px;"></center>
               </div>
               <div class="col-md-7">
                  <center>
                     <h4 style="color: #92278f;">Naturalistic Intelligence</h4>
                  </center>
                  <br>
                  <ul style="list-style-type:circle; margin-left: 20px;">
                     <li>Allows one to distinguish among, classify, and use features of the environment</li>
                     <li>The ability to discriminate among living things and to see patterns in the natural world</li>
                     <li>Careers which suit those with this intelligence include wild Life Photographer, naturalists, conservationists, gardeners and farmers.</li>
                  </ul>
               </div>
            </div>
            <!--  --><br><br>
            <div class="row">
               <div class="col-md-7">
                  <center>
                     <h4 style="color: #08509a;">Inter Personal Intelligence</h4>
                  </center>
                  <br>
                  <ul style="list-style-type:circle; margin-left: 20px;">
                     <li>Enables individuals to recognize and make distinctions among others’ feelings and intentions</li>
                     <li>The ability to work effectively with others and display empathy</li>
                     <li>Careers which suit those with this intelligence include politicians, managers, teachers, and social workers.</li>
                  </ul>
               </div>
               <div class="col-md-5">
                  <center> <img src="<?php echo base_url();?>assets/images/gallery/gallery_10.jpg" style="width: 339px; height: 264px;"></center>
               </div>
            </div>
            <!--  --><br><br>
            <div class="row">
               <div class="col-md-5">
                  <center> <img src="<?php echo base_url();?>assets/images/gallery/gallery_bg_03.jpg" style="width: 339px; height: 264px;"></center>
               </div>
               <div class="col-md-7">
                  <center>
                     <h4 style="color: #8dc63f;">Bodily Kinesthetic Intelligence</h4>
                  </center>
                  <br>
                  <ul style="list-style-type:circle; margin-left: 20px;">
                     <li>Allows individuals to use all or part of one’s body to create products, solve problems, or present ideas and emotions.</li>
                     <li>Using the body in highly differentiated ways for expressive, recreational, or goal directed purposes</li>
                     <!--  <li>People who have this intelligence usually enjoy acting or performing, and in general they are good at building and making things.</li> -->
                     <li>Careers which suit those with this intelligence include athletes, dancers, actors, surgeons, builders and soldiers</li>
                  </ul>
               </div>
            </div>
            <!--  --><br><br>
            <div class="row">
               <div class="col-md-7">
                  <center>
                     <h4 style="color: #fcb614;">Musical Rhythmic Intelligence</h4>
                  </center>
                  <br>
                  <p style=" margin-left: 20px;">By undergoing fingerprint test, one can assess these above mentioned intelligence level within themselves.</p>
               </div>
               <div class="col-md-5">
                  <center> <img src="<?php echo base_url();?>assets/images/gallery/gallery_bg_05.jpg" style="width: 339px; height: 264px;"></center>
               </div>
            </div>
         <!-- </div> -->
      </div>
   </div>
</section>
<!-- Blog Post End here