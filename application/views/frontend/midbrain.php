<!-- Page Header Start here -->
<section class="page-header section-notch">
   <div class="overlay">
      <div class="container">
         <h3>Mid Brain</h3>
         <ul>
            <li><a href="<?php echo base_url('index');?>">Home</a></li>
            <li>-</li>
            <li><a href="<?php echo base_url('midbrain');?>">Courses</a></li>
         </ul>
      </div>
      <!-- container -->
   </div>
   <!-- overlay -->
</section>
<!-- page header -->
<!-- Page Header End here -->
<!-- Blog Post Start here -->
<section class="singel-class padding-120">
   <div class="container">
      <div class="row">
         <div class="col-md-8">
            <div class="single-post">
               <div class="post-image">
                  <img src="<?php echo base_url();?>assets/images/classes/1.jpg" alt="post image" class="img-responsive">
               </div>
               <div class="post-content">
                  <h5>What is MidBrain?</h5>
                  <br>
                  <p>MidBrain is a device and bridge located between left and right brain and act as dominance for the device, which is also a key to open the subconscious. The MidBrain manages the function of both the left and right brain. It is a portion of the central nervous system associate with vision, hearing, sleep, action/reaction, walk etc. It is located below the central cortex. 
                     Once Midbrain is activated, it can balance the usage and coordination between left and right brain. Besides that, a person whose midbrain is activated, he or she not only has an extraordinary memory, but also great potential to develop the right brain, thereby improving attention, critical thinking skills and creativity.
                  </p>
                  <!--  -->
                  <h5>What is MidBrain activation Program?</h5>
                  <br>
                  <ul>
                     <li> MidBrain activation program allows the brain to function as a whole part of brain. </li>
                     <li>It is based on interactive methodology which motivates the participant to perform in a very healthy environment. </li>
                     <li>The program helps the participant to effectively use their right and left brain functions simultaneously and effectively</li>
                     <li>During the program the participants are exposed to various audio and visual aids & special brain simulation exercises.</li>
                     <li>They learn to relax their mind with specific meditative techniques and music</li>
                  </ul>
               </div>
            </div>
            <!-- single post -->
         </div>
         <div class="col-md-4">
            <div class="sidebar">
               <div class="sidebar-item">
                  <ul class="class-details">
                     <li>
                        <div class="name"><i class="flaticon-pencil"></i>Start Date</div>
                        <div class="info">24 March 2017</div>
                     </li>
                     <li>
                        <div class="name"><i class="flaticon-student"></i>Years Old</div>
                        <div class="info">6-8 Years</div>
                     </li>
                     <li>
                        <div class="name"><i class="flaticon-symbols"></i>Class Size</div>
                        <div class="info">20-30 Kids</div>
                     </li>
                     <li>
                        <div class="name"><i class="flaticon-school-bus"></i>Carry Time</div>
                        <div class="info">5 Hours/6 Days</div>
                     </li>
                     <li>
                        <div class="name"><i class="flaticon-book"></i>Coures Duration</div>
                        <div class="info">10-12 Month</div>
                     </li>
                     <li>
                        <div class="name"><i class="flaticon-alarm-clock"></i>Class Time</div>
                        <div class="info">9:30am-5:30pm</div>
                     </li>
                     <li>
                        <div class="name"><i class="flaticon-like-2"></i>Rating</div>
                        <div class="info rating">
                           <i class="fa fa-star" aria-hidden="true"></i>
                           <i class="fa fa-star" aria-hidden="true"></i>
                           <i class="fa fa-star" aria-hidden="true"></i>
                           <i class="fa fa-star" aria-hidden="true"></i>
                           <i class="fa fa-star" aria-hidden="true"></i>
                        </div>
                     </li>
                     <li>
                        <div class="name"><i class="flaticon-money-bag"></i>Tution Free</div>
                        <div class="info">$ 250.00</div>
                     </li>
                  </ul>
               </div>
               <!-- sidebar item -->
            </div>
            <div class="sidebar">
               <h5>Who can be the Participants:</h5>
               <p>Child aged between 5 -15 years</p>
               <h5>Course Structure:</h5>
               <ul>
                  <li> 2 days workshop for 7 hours per day ( mostly on weekends)</li>
                  <li> Follow-up sessions</li>
               </ul>
               <br>
               <h5><b style="color: #a40202;">Important note to Parents:</b></h5>
               <i>Midbrain Activation is a proven mind development program based on scientific approach and motivational-teaching method. There are no elements of religion or magic in the entire coursework.</i>
            </div>
         </div>
      </div>
      <br><br>
      <center>
         <h4 style="color: #ee257c;font-size: 28px;">Methods Used</h4>
      </center>
      <br><br>
      <div class="row">
         <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
            <div style="border: solid #b3b1b140 2px;border-bottom: solid green 4px;height:250px;">
               <br>
               <center> <i class="fa fa-child" aria-hidden="true" style="color: green; font-size: 44px;"></i></center>
               <br>
               <center>
                  <h6 style="font-size: 22px;color: green;">Brain Gym</h6>
               </center>
               <br>
               <center>
                  <p style="margin: 5px;">Brain Gym method is used to Increase Brain Performance.</p>
               </center>
            </div>
         </div>
         <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
            <div style="border: solid #b3b1b140 2px;border-bottom: solid #a40202 4px;height:250px;">
               <br>
               <center> <i class="fa fa-eye" aria-hidden="true" style="color: #a40202; font-size: 44px;"></i></center>
               <br>
               <center>
                  <h6 style="font-size: 22px;color: #a40202;">Eyeball Exercise</h6>
               </center>
               <br>
               <center>
                  <p style="margin: 5px;">Eyeball Exercise method is used to increase the reading speed.</p>
               </center>
            </div>
         </div>
         <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
            <div style="border: solid #b3b1b140 2px;border-bottom: solid #13b8f2 4px;height:250px;">
               <br>
               <center> <i class="fa fa-graduation-cap" aria-hidden="true" style="color: #13b8f2; font-size: 44px;"></i></center>
               <br>
               <center>
                  <h6 style="font-size: 22px;color: #13b8f2;">Meditation</h6>
               </center>
               <br>
               <center>
                  <p style="margin: 5px;">Affirmation, Video-audio, Games.</p>
               </center>
            </div>
         </div>
      </div><br>
      <div class="row">
         <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
            <div style="border: solid #b3b1b140 2px;border-bottom: solid #92278f 4px;height:250px;">
               <br>
               <center> <i class="fa fa-user-secret" aria-hidden="true" style="color: #92278f; font-size: 44px;"></i></center>
               <br>
               <center>
                  <h6 style="font-size: 22px;color: #92278f;">Brain Activation</h6>
               </center>
               <br>
               <center>
                  <p style="margin: 5px;">Brain activation with music therapy is used to activate the brain for speed working.</p>
               </center>
            </div>
         </div>
         <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
            <div style="border: solid #b3b1b140 2px;border-bottom: solid #fcb614 4px;height:250px;">
               <br>
               <center> <i class="fa fa-gratipay" aria-hidden="true" style="color: #fcb614; font-size: 44px;color: #fcb614;"></i></center>
               <br>
               <center>
                  <h6 style="font-size: 22px;color: #fcb614;">Dance Therapy</h6>
               </center>
               <br>
               <center>
                  <p style="margin: 5px;">Dance Therapy is used to for the relaxation of mind.</p>
               </center>
            </div>
         </div>
         <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
            <div style="border: solid #b3b1b140 2px;border-bottom: solid  #d61b12 4px;height:250px;">
               <br>
               <center> <i class="fa fa-youtube-play" aria-hidden="true" style="color: #d61b12; font-size: 44px;"></i></center>
               <br>
               <center>
                  <h6 style="font-size: 22px;color: #d61b12;">Fun and Joyful Activities</h6>
               </center>
               <br>
               <center>
                  <p style="margin: 5px;">Keep reading to learn about why Health Coaching is the career you’ve been looking for.</p>
               </center>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- Blog Post End here -->