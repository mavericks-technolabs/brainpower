<!-- Banner Start here -->
<section class="banner section-notch">
   <div class="banner-slider swiper-container">
      <div class="swiper-wrapper">
         <div class="banner-item slide-two swiper-slide">
            <div class="banner-overlay"></div>
            <div class="container">
               <div class="banner-content">
                  <h3>EMPOWERING GENERATIONS</h3>
                  <p>Brain Power is all about undertaking Brain Training exercises which can significantly improve the functions of the Brain.</p>
                  <br><br>
               </div>
               <!-- banner-content -->
            </div>
            <!-- container -->
         </div>
         <!-- slide item -->
         <div class="banner-item slide-one swiper-slide">
            <div class="banner-overlay"></div>
            <div class="container">
               <div class="banner-content">
                  <h3>WELCOME TO BRAINPOWER</h3>
                  <!-- <h2>Best For Education</h2> -->
                  <p>Brain Power is all about undertaking Brain Training exercises which can significantly improve the functions of the Brain.</p>
                  <br><br>
               </div>
               <!-- banner-content -->
            </div>
            <!-- container -->
         </div>
         <!-- slide item -->
      </div>
      <!-- swiper-wrapper -->
      <div class="swiper-pagination"></div>
   </div>
   <!-- swiper-container -->
</section>
<!-- banner -->
<!-- Banner End here -->
<!-- facility Start here -->
<section class="facility padding-120">
   <div class="container">
      <div class="row">
         <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="facility-item">
               <span class="icon flaticon-symbols"></span>
               <h4>Active Learning</h4>
               <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div>
            <!-- facility item -->
         </div>
         <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="facility-item">
               <span class="icon flaticon-avatar"></span>
               <h4>Expert Teachers</h4>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div>
            <!-- facility item -->
         </div>
         <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="facility-item">
               <span class="icon flaticon-world"></span>
               <h4>Strategi Location</h4>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div>
            <!-- facility item -->
         </div>
         <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="facility-item">
               <span class="icon flaticon-line-chart"></span>
               <h4>Full Day Programs</h4>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div>
            <!-- facility item -->
         </div>
      </div>
      <!-- row -->
   </div>
   <!-- container -->
</section>
<!-- facility -->
<!-- facility End here -->
<!-- About Start here -->
<section class="about section-notch">
   <div class="overlay padding-120">
      <div class="container">
         <div class="row">
            <div class="col-md-6">
               <br><br><br>
               <div class="about-image">
                  <img src="<?php echo base_url();?>assets/images/about/about.png" alt="about image" class="img-responsive">
               </div>
            </div>
            <div class="col-md-6">
               <div class="about-content">
                  <h3>About Our Brainpower</h3>
                  <p>Brain Power is all about undertaking Brain Training exercises which can significantly improve the functions of the Brain. Brain training is beneficial for people regardless of their age. Children can develop their cognitive functions much faster, adults can get an edge at work, while older people can keep their mind fit through Brain training exercises.</p>
                  <br>
                  <h3>Did You Know ?</h3>
                  <br>
                  <p>An average person only utilises less than 10% of the brain’s potential? That means have 90% of latent potential waiting to be untapped! We are probably more familiar with the terms ‘left brain’, ‘right brain’, and the characteristics related to each side of the brain. But did you know that we can use both sides of the brain interchangeably and achieve effective results? So for the same we need to activate our brain. So What does it mean to activate’ your brain? Is our brain not ‘activated’? Why do we need to ‘activate’ it? And how do you know when your brain is ‘activated’? </p>
               </div>
               <!-- about content -->
            </div>
         </div>
         <!-- row -->
      </div>
      <!-- container -->
   </div>
   <!-- overlay -->
</section>
<!-- about -->
<!-- About End here -->
<!-- Classes Start here -->
<section class="classes padding-120">
   <div class="container">
      <div class="section-header">
         <h3>Our Courses</h3>
         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
      <div class="row">
         <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="class-item">
               <div class="image">
                  <img src="<?php echo base_url();?>assets/images/classes/class_01.jpg" alt="class image" class="img-responsive">
               </div>
               <ul class="schedule">
                  <li>
                     <span>Class Size</span>
                     <span>30 - 40</span>
                  </li>
                  <li>
                     <span>Years Old</span>
                     <span>5 - 6</span>
                  </li>
                  <li>
                     <span>Tution Fee</span>
                     <span>$1500</span>
                  </li>
               </ul>
               <div class="content">
                  <h4><a href="<?php echo base_url('midbrain');?>">Mid Brain</a></h4>
                  <p><span>Class Time</span> : 08:00 am - 10:00 am</p>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
               </div>
               <div class="address">
                  <center>
                     <a href=" " style="text-decoration: none;">
                        <p style="font-weight: bold;">View More</p>
                     </a>
                  </center>
               </div>
            </div>
            <!-- class item -->
         </div>
         <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="class-item">
               <div class="image">
                  <img src="<?php echo base_url();?>assets/images/classes/class_02.jpg" alt="class image" class="img-responsive">
               </div>
               <ul class="schedule">
                  <li>
                     <span>Class Size</span>
                     <span>30 - 40</span>
                  </li>
                  <li>
                     <span>Years Old</span>
                     <span>5 - 6</span>
                  </li>
                  <li>
                     <span>Tution Fee</span>
                     <span>$1500</span>
                  </li>
               </ul>
               <div class="content">
                  <h4><a href="<?php echo base_url('dmit');?>">DMIT</a></h4>
                  <p><span>Class Time</span> : 08:00 am - 10:00 am</p>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
               </div>
               <div class="address">
                  <center>
                     <a href="<?php echo base_url('dmit');?>" style="text-decoration: none;">
                        <p style="font-weight: bold;">View More</p>
                     </a>
                  </center>
               </div>
            </div>
            <!-- class item -->
         </div>
         <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="class-item">
               <div class="image">
                  <img src="<?php echo base_url();?>assets/images/classes/class_03.jpg" alt="class image" class="img-responsive">
               </div>
               <ul class="schedule">
                  <li>
                     <span>Class Size</span>
                     <span>30 - 40</span>
                  </li>
                  <li>
                     <span>Years Old</span>
                     <span>5 - 6</span>
                  </li>
                  <li>
                     <span>Tution Fee</span>
                     <span>$1500</span>
                  </li>
               </ul>
               <div class="content">
                  <h4><a href="<?php echo base_url('chakrameditation');?>">Chatra Meditation</a></h4>
                  <p><span>Class Time</span> : 08:00 am - 10:00 am</p>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
               </div>
               <div class="address">
                  <center>
                     <a href="<?php echo base_url('chakrameditation');?>" style="text-decoration: none;">
                        <p style="font-weight: bold;">View More</p>
                     </a>
                  </center>
               </div>
            </div>
            <!-- class item -->
         </div>
      </div>
      <!-- row -->
   </div>
   <!-- container -->
</section>
<!-- classes -->
<!-- Classes End here -->
<!-- Gallery Start here -->
<section class="gallery padding-110">
   <div class="container">
      <div class="section-header">
         <h3>Gallery</h3>
         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
      <ul class="gallery-menu">
         <li class="active" data-filter="*">All</li>
         <li data-filter=".branding">Mid Brain</li>
         <li data-filter=".development">DMIT</li>
         <li data-filter=".packing">Chakra Meditation</li>
         <li data-filter=".photography">Others</li>
      </ul>
      <div class="gallery-items">
         <div class="gallery-item branding packing">
            <div class="gallery-image">
               <img src="<?php echo base_url();?>assets/images/gallery/gallery_01.jpg" alt="gallery image" class="img-responsive">
               <div class="gallery-overlay">
                  <div class="bg"></div>
               </div>
               <div class="gallery-content">
                  <a href="<?php echo base_url();?>assets/images/gallery/gallery_bg_01.jpg" data-rel="lightcase:myCollection"><i class="icon flaticon-expand"></i></a>
                  <h4>Gallery</h4>
                  <div class="col-md-12">
                     <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                  </div>
               </div>
            </div>
         </div>
         <!-- gallery item -->
         <div class="gallery-item development photography">
            <div class="gallery-image">
               <img src="<?php echo base_url();?>assets/images/gallery/gallery_02.jpg" alt="gallery image" class="img-responsive">
               <div class="gallery-overlay">
                  <div class="bg"></div>
               </div>
               <div class="gallery-content">
                  <a href="<?php echo base_url();?>assets/images/gallery/gallery_bg_02.jpg" data-rel="lightcase:myCollection"><i class="icon flaticon-expand"></i></a>
                  <h4>Gallery</h4>
                  <div class="col-md-12">
                     <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                  </div>
               </div>
            </div>
         </div>
         <!-- gallery item -->
         <div class="gallery-item branding packing">
            <div class="gallery-image">
               <img src="<?php echo base_url();?>assets/images/gallery/gallery_03.jpg" alt="gallery image" class="img-responsive">
               <div class="gallery-overlay">
                  <div class="bg"></div>
               </div>
               <div class="gallery-content">
                  <a href="<?php echo base_url();?>assets/images/gallery/gallery_bg_03.jpg" data-rel="lightcase:myCollection"><i class="icon flaticon-expand"></i></a>
                  <h4>Gallery</h4>
                  <div class="col-md-12">
                     <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                  </div>
               </div>
            </div>
         </div>
         <!-- gallery item -->
         <div class="gallery-item development photography">
            <div class="gallery-image">
               <img src="<?php echo base_url();?>assets/images/gallery/gallery_04.jpg" alt="gallery image" class="img-responsive">
               <div class="gallery-overlay">
                  <div class="bg"></div>
               </div>
               <div class="gallery-content">
                  <a href="<?php echo base_url();?>assets/images/gallery/gallery_bg_04.jpg" data-rel="lightcase:myCollection"><i class="icon flaticon-expand"></i></a>
                  <h4>Gallery</h4>
                  <div class="col-md-12">
                     <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                  </div>
               </div>
            </div>
         </div>
         <!-- gallery item -->
         <div class="gallery-item branding packing">
            <div class="gallery-image">
               <img src="<?php echo base_url();?>assets/images/gallery/gallery_05.jpg" alt="gallery image" class="img-responsive">
               <div class="gallery-overlay">
                  <div class="bg"></div>
               </div>
               <div class="gallery-content">
                  <a href="<?php echo base_url();?>assets/images/gallery/gallery_bg_05.jpg" data-rel="lightcase:myCollection"><i class="icon flaticon-expand"></i></a>
                  <h4>Gallery</h4>
                  <div class="col-md-12">
                     <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                  </div>
               </div>
            </div>
         </div>
         <!-- gallery item -->
         <div class="gallery-item branding packing">
            <div class="gallery-image">
               <img src="<?php echo base_url();?>assets/images/gallery/gallery_06.jpg" alt="gallery image" class="img-responsive">
               <div class="gallery-overlay">
                  <div class="bg"></div>
               </div>
               <div class="gallery-content">
                  <a href="<?php echo base_url();?>assets/images/gallery/gallery_bg_06.jpg" data-rel="lightcase:myCollection"><i class="icon flaticon-expand"></i></a>
                  <h4>Gallery</h4>
                  <div class="col-md-12">
                     <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                  </div>
               </div>
            </div>
         </div>
         <!-- gallery item -->
      </div>
      <!-- gallery items -->
      <div class="gallery-button"><a href="<?php echo base_url('gallery');?>" class="button-default">View More Gallery</a></div>
   </div>
   <!-- container -->
</section>
<!-- gallery -->
<!-- Gallery End here --><br>
<!-- Achievements Start here -->
<section class="achievements section-notch">
   <div class="overlay padding-120">
      <div class="container">
         <div class="row">
            <div class="col-md-3 col-sm-3 col-12">
               <div class="achievement-item">
                  <i class="icon flaticon-student"></i>
                  <span class="counter">320</span><span>+</span>
                  <p>Total Students</p>
               </div>
               <!-- achievement item -->
            </div>
            <div class="col-md-3 col-sm-3 col-12">
               <div class="achievement-item">
                  <i class="icon flaticon-construction"></i>
                  <span class="counter">12</span>
                  <p>Class Rooms</p>
               </div>
               <!-- achievement item -->
            </div>
            <div class="col-md-3 col-sm-3 col-12">
               <div class="achievement-item">
                  <i class="icon flaticon-school-bus"></i>
                  <span class="counter">24</span>
                  <p>Schools bus</p>
               </div>
               <!-- achievement item -->
            </div>
            <div class="col-md-3 col-sm-3 col-12">
               <div class="achievement-item">
                  <i class="icon flaticon-people"></i>
                  <span class="counter">15</span>
                  <p>Total Teachers</p>
               </div>
               <!-- achievement item -->
            </div>
         </div>
         <!-- row -->
      </div>
      <!-- container -->
   </div>
   <!-- overlay -->
</section>
<!-- achievements -->
<!-- Achievements End here -->
<!-- Testimonial End here -->
<!-- Event Start here -->
<section class="event padding-120">
   <div class="container">
      <div class="section-header">
         <h3>Don't Miss Our Event</h3>
         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
      <div class="event-items">
         <div class="event-item wide">
            <div class="event-image">
               <img src="<?php echo base_url();?>assets/images/event/event_01.jpg" alt="event image" class="img-responsive">
               <div class="date">
                  <span>24</span>
                  <p>March</p>
               </div>
            </div>
            <div class="event-content">
               <h4>Imagination Classes</h4>
               <ul>
                  <li><span><i class="fa fa-clock-o" aria-hidden="true"></i></span>08:00 am - 10:00 am</li>
                  <li><span><i class="fa fa-home" aria-hidden="true"></i></span>218 New MG Road Pune</li>
               </ul>
               <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
               <br><br>
            </div>
         </div>
         <div class="event-item">
            <div class="event-image">
               <img src="<?php echo base_url();?>assets/images/event/event_02.jpg" alt="event image" class="img-responsive">
               <div class="date">
                  <span>24</span>
                  <p>March</p>
               </div>
            </div>
            <div class="event-content">
               <h4>Imagination Classes</h4>
               <ul>
                  <li><span><i class="fa fa-clock-o" aria-hidden="true"></i></span>08:00 am - 10:00 am</li>
                  <li><span><i class="fa fa-home" aria-hidden="true"></i></span>218 New MG Road Pune</li>
               </ul>
               <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
               <br><br>
            </div>
         </div>
         <div class="event-item wide right">
            <div class="event-image">
               <img src="<?php echo base_url();?>assets/images/event/event_03.jpg" alt="event image" class="img-responsive">
               <div class="date">
                  <span>24</span>
                  <p>March</p>
               </div>
            </div>
            <div class="event-content">
               <h4>Imagination Classes</h4>
               <ul>
                  <li><span><i class="fa fa-clock-o" aria-hidden="true"></i></span>08:00 am - 10:00 am</li>
                  <li><span><i class="fa fa-home" aria-hidden="true"></i></span>218 New MG Road Pune</li>
               </ul>
               <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
               <br><br>
            </div>
         </div>
      </div>
   </div><br>
      <center><div class="gallery-button"><a href="<?php echo base_url('event');?>" class="button-default">View More Events</a></div></center>
   <!-- container -->
</section>
<!-- event blog -->
<!-- Testimonial Start here -->
<section class="testimonial padding-90">
   <div class="container">
      <div class="section-header">
         <h3>What Parents Say</h3>
         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
      </div>
      <div class="testimonial-items" style="margin-bottom: -265px;">
         <div class="testimonial-slider swiper-container">
            <div class="swiper-wrapper">
               <div class="testimonial-item swiper-slide">
                  <div class="testimonial-details">
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                     <h4>Joly Smith <span>ui/ux Designer</span></h4>
                     <img src="<?php echo base_url();?>assets/images/testimonial/testimonial_icon_01.jpg" alt="testimonial icon" class="img-responsive">
                  </div>
                  <div class="testimonial-image">
                     <img src="<?php echo base_url();?>assets/images/testimonial/testimonial_01.jpg" alt="client image" class="img-responsive">
                  </div>
               </div>
               <!-- testimonial-item -->
               <div class="testimonial-item swiper-slide">
                  <div class="testimonial-details">
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                     <h4>Joly Smith <span>ui/ux Designer</span></h4>
                     <img src="<?php echo base_url();?>assets/images/testimonial/testimonial_icon_02.jpg" alt="testimonial icon" class="img-responsive">
                  </div>
                  <div class="testimonial-image">
                     <img src="<?php echo base_url();?>assets/images/testimonial/testimonial_02.jpg" alt="client image" class="img-responsive">
                  </div>
               </div>
               <!-- testimonial-item -->
               <div class="testimonial-item swiper-slide">
                  <div class="testimonial-details">
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                     <h4>Joly Smith <span>ui/ux Designer</span></h4>
                     <img src="<?php echo base_url();?>assets/images/testimonial/testimonial_icon_03.jpg" alt="testimonial icon" class="img-responsive">
                  </div>
                  <div class="testimonial-image">
                     <img src="<?php echo base_url();?>assets/images/testimonial/testimonial_03.jpg" alt="client image" class="img-responsive">
                  </div>
               </div>
               <!-- testimonial-item -->
               <div class="testimonial-item swiper-slide">
                  <div class="testimonial-details">
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                     <h4>Joly Smith <span>ui/ux Designer</span></h4>
                     <img src="<?php echo base_url();?>assets/images/testimonial/testimonial_icon_01.jpg" alt="testimonial icon" class="img-responsive">
                  </div>
                  <div class="testimonial-image">
                     <img src="<?php echo base_url();?>assets/images/testimonial/testimonial_01.jpg" alt="client image" class="img-responsive">
                  </div>
               </div>
               <!-- testimonial-item -->
               <div class="testimonial-item swiper-slide">
                  <div class="testimonial-details">
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                     <h4>Joly Smith <span>ui/ux Designer</span></h4>
                     <img src="<?php echo base_url();?>assets/images/testimonial/testimonial_icon_02.jpg" alt="testimonial icon" class="img-responsive">
                  </div>
                  <div class="testimonial-image">
                     <img src="<?php echo base_url();?>assets/images/testimonial/testimonial_02.jpg" alt="client image" class="img-responsive">
                  </div>
               </div>
               <!-- testimonial-item -->
               <div class="testimonial-item swiper-slide">
                  <div class="testimonial-details">
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                     <h4>Joly Smith <span>ui/ux Designer</span></h4>
                     <img src="<?php echo base_url();?>assets/images/testimonial/testimonial_icon_03.jpg" alt="testimonial icon" class="img-responsive">
                  </div>
                  <div class="testimonial-image">
                     <img src="<?php echo base_url();?>assets/images/testimonial/testimonial_03.jpg" alt="client image" class="img-responsive">
                  </div>
               </div>
               <!-- testimonial-item -->
            </div>
            <!-- swiper-wrapper -->
         </div>
         <!-- swiper-container -->
      </div>
      <!-- testimonial-items -->
   </div>
   <!-- container -->
</section>
<!-- testimonial -->
<!-- event End here -->