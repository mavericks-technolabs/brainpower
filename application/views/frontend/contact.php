<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js" ></script>
<style type="text/css">
    .swal-button {
  padding: 7px 19px;
  border-radius: 2px;
  color: #ffff;
  background-color: #4962B3;
  font-size: 12px;
  border: 1px solid #3e549a;
  text-shadow: 0px -1px 0px rgba(0, 0, 0, 0.3);
}
.swal-footer {
  background-color: rgb(245, 248, 250);
  margin-top: 32px;
  border-top: 1px solid #E9EEF1;
  overflow: hidden;
}
</style>
<?php if ($this->session->flashdata('message')): ?>
    <script>
        swal({
            title: "Done",
            text: "<?php echo $this->session->flashdata('message'); ?>",
            icon: "success",
            timer: 1500,
            showConfirmButton: false,
            type: 'success'
        });
    </script>
<?php endif; ?>
<style type="text/css">
   .cont_inpt{
   border-bottom: 2px solid #92278f;
   }
</style>

<!-- Page Header Start here -->
<section class="page-header section-notch">
   <div class="overlay">
      <div class="container">
         <h3>Our Contact</h3>
         <ul>

            <li><a href="<?php echo base_url('index');?>">Home</a></li>
            <li>-</li>
            <li><a href="<?php echo base_url('contact');?>">Contact Us</a></li>
      </div>
      <!-- container -->
   </div>
   <!-- overlay -->
</section>
<!-- page header -->
<!-- Page Header End here -->
<!-- Contact Start here -->
<section class="contact contact-page">
   <div class="contact-details padding-120">
      <div class="container">
         <div class="row">
            <div class="col-md-6" style="border: solid #8080804f 2px; box-shadow: 1px 1px 1px 1px #80808038;">
               <br>
               
              
                  <div class="row">
                     <center>
                        <h4 style="color: #92278f;">Enquiry Form</h4>
                     </center>
                     <br>
                      <?php echo form_open('FrontController/Contacts',['class'=>'contact-form']); ?>
                     <div class="col-md-10 col-md-offset-1">
                        <input type="text" name="name" placeholder="Your Name" class="contact-input cont_inpt"><br>
                        <input type="email" name="email" placeholder="Your Email" class="contact-input">
                        <input type="text" name="contact" placeholder="Your Number" class="contact-input">
                        <input type="text" name="subject" placeholder="Your Subject" class="contact-input">
                        <textarea rows="3" name="message" class="contact-input" placeholder="Your Message"></textarea>
                        <br>
                        <center><input type="submit" name="submit" value="Send Message" class="contact-button" ></center>
                        <br>
                     </div>
                  </div>
               </form>
            </div>
            <div class="col-md-6">
               <div class="row">
                  <br>
                  <center><img src="<?php echo base_url();?>assets/images/location.jpg" alt=" image" class="img-responsive" style="width: 40px;" ></center>
                  <center>
                     <h4 style="color: #76c2af;">Our Location</h4>
                     <p>Pune,Maharashtra <br>Pune,Maharashtra</p>
                  </center>
               </div>
               <br>
               <div class="row">
                  <center><img src="<?php echo base_url();?>assets/images/phone.png" alt=" image" class="img-responsive" style="width: 40px;" ></center>
                  <center>
                     <h4 style="color: #ff6e1f;">Contact Number</h4>
                     <p>9823262101</p>
                  </center>
               </div>
               <br>
               <div class="row">
                  <center><img src="<?php echo base_url();?>assets/images/email.png" alt=" image" class="img-responsive" style="width: 40px;" ></center>
                  <center>
                     <h4 style="color: #1e7ad7;">Email Address</h4>
                     <p>gupte_10@yahoo.com</p>
                  </center>
               </div>
            </div>
         </div>
      </div>
      <!-- container -->
   </div>
   <!-- contact-details -->
   <div class="container">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d121058.92836618835!2d73.79292732583261!3d18.524766325748654!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2bf2e67461101%3A0x828d43bf9d9ee343!2sPune%2C+Maharashtra!5e0!3m2!1sen!2sin!4v1547617396130" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>   
   </div>
</section>
<!-- Contact End here -->