<!-- Page Header Start here -->
<section class="page-header section-notch">
   <div class="overlay">
      <div class="container">
         <h3>Our School Gallery</h3>
         <ul>
             <li><a href="<?php echo base_url('index');?>">Home</a></li>
            <li>-</li>
            <li><a href="<?php echo base_url('gallery');?>">Gallery</a></li>
         </ul>
      </div>
      <!-- container -->
   </div>
   <!-- overlay -->
</section>
<!-- page header -->
<!-- Page Header End here -->
<!-- Gallery Start here -->
<section class="gallery padding-120">
   <div class="container">
      <ul class="gallery-menu">
         <li class="active" data-filter="*">All</li>
         <li data-filter=".branding">Mid Brain</li>
         <li data-filter=".development">DMIT</li>
         <li data-filter=".packing">Chakra Meditation</li>
         <li data-filter=".photography">Others</li>
      </ul>
      <div class="gallery-items">
         <div class="gallery-item branding packing">
            <div class="gallery-image">
               <img src="<?php echo base_url();?>assets/images/gallery/gallery_01.jpg" alt="gallery image" class="img-responsive">
               <div class="gallery-overlay">
                  <div class="bg"></div>
               </div>
               <div class="gallery-content">
                  <a href="<?php echo base_url();?>assets/images/gallery/gallery_bg_01.jpg" data-rel="lightcase:myCollection"><i class="icon flaticon-expand"></i></a>
                  <h4>Gallery</h4>
               </div>
            </div>
         </div>
         <!-- gallery item -->
         <div class="gallery-item development photography">
            <div class="gallery-image">
               <img src="<?php echo base_url();?>assets/images/gallery/gallery_02.jpg" alt="gallery image" class="img-responsive">
               <div class="gallery-overlay">
                  <div class="bg"></div>
               </div>
               <div class="gallery-content">
                  <a href="<?php echo base_url();?>assets/images/gallery/gallery_bg_02.jpg" data-rel="lightcase:myCollection"><i class="icon flaticon-expand"></i></a>
                  <h4>Gallery</h4>
               </div>
            </div>
         </div>
         <!-- gallery item -->
         <div class="gallery-item branding packing">
            <div class="gallery-image">
               <img src="<?php echo base_url();?>assets/images/gallery/gallery_03.jpg" alt="gallery image" class="img-responsive">
               <div class="gallery-overlay">
                  <div class="bg"></div>
               </div>
               <div class="gallery-content">
                  <a href="<?php echo base_url();?>assets/images/gallery/gallery_bg_03.jpg" data-rel="lightcase:myCollection"><i class="icon flaticon-expand"></i></a>
                  <h4>Gallery</h4>
               </div>
            </div>
         </div>
         <!-- gallery item -->
         <div class="gallery-item development photography">
            <div class="gallery-image">
               <img src="<?php echo base_url();?>assets/images/gallery/gallery_04.jpg" alt="gallery image" class="img-responsive">
               <div class="gallery-overlay">
                  <div class="bg"></div>
               </div>
               <div class="gallery-content">
                  <a href="<?php echo base_url();?>assets/images/gallery/gallery_bg_04.jpg" data-rel="lightcase:myCollection"><i class="icon flaticon-expand"></i></a>
                  <h4>Gallery</h4>
               </div>
            </div>
         </div>
         <!-- gallery item -->
         <div class="gallery-item branding packing">
            <div class="gallery-image">
               <img src="<?php echo base_url();?>assets/images/gallery/gallery_05.jpg" alt="gallery image" class="img-responsive">
               <div class="gallery-overlay">
                  <div class="bg"></div>
               </div>
               <div class="gallery-content">
                  <a href="<?php echo base_url();?>assets/images/gallery/gallery_bg_05.jpg" data-rel="lightcase:myCollection"><i class="icon flaticon-expand"></i></a>
                  <h4>Gallery</h4>
               </div>
            </div>
         </div>
         <!-- gallery item -->
         <div class="gallery-item branding packing">
            <div class="gallery-image">
               <img src="<?php echo base_url();?>assets/images/gallery/gallery_06.jpg" alt="gallery image" class="img-responsive">
               <div class="gallery-overlay">
                  <div class="bg"></div>
               </div>
               <div class="gallery-content">
                  <a href="<?php echo base_url();?>assets/images/gallery/gallery_bg_06.jpg" data-rel="lightcase:myCollection"><i class="icon flaticon-expand"></i></a>
                  <h4>Gallery</h4>
               </div>
            </div>
         </div>
         <!-- gallery item -->
         <div class="gallery-item development photography">
            <div class="gallery-image">
               <img src="<?php echo base_url();?>assets/images/gallery/gallery_04.jpg" alt="gallery image" class="img-responsive">
               <div class="gallery-overlay">
                  <div class="bg"></div>
               </div>
               <div class="gallery-content">
                  <a href="<?php echo base_url();?>assets/images/gallery/gallery_bg_04.jpg" data-rel="lightcase:myCollection"><i class="icon flaticon-expand"></i></a>
                  <h4>Gallery</h4>
               </div>
            </div>
         </div>
         <!-- gallery item -->
         <div class="gallery-item branding packing">
            <div class="gallery-image">
               <img src="<?php echo base_url();?>assets/images/gallery/gallery_05.jpg" alt="gallery image" class="img-responsive">
               <div class="gallery-overlay">
                  <div class="bg"></div>
               </div>
               <div class="gallery-content">
                  <a href="<?php echo base_url();?>assets/images/gallery/gallery_bg_05.jpg" data-rel="lightcase:myCollection"><i class="icon flaticon-expand"></i></a>
                  <h4>Gallery</h4>
               </div>
            </div>
         </div>
         <!-- gallery item -->
         <div class="gallery-item branding packing">
            <div class="gallery-image">
               <img src="<?php echo base_url();?>assets/images/gallery/gallery_06.jpg" alt="gallery image" class="img-responsive">
               <div class="gallery-overlay">
                  <div class="bg"></div>
               </div>
               <div class="gallery-content">
                  <a href="<?php echo base_url();?>assets/images/gallery/gallery_bg_06.jpg" data-rel="lightcase:myCollection"><i class="icon flaticon-expand"></i></a>
                  <h4>Gallery</h4>
               </div>
            </div>
         </div>
         <!-- gallery item -->
      </div>
      <!-- gallery items -->
   </div>
   <!-- container -->
</section>
<!-- gallery -->
<!-- Gallery End here -->