<html>
<head>
	<title>Brainpower |Login</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <script src="<?php echo base_url();?>assets/js/jquery-1.12.4.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <style type="text/css">
     .box_login{
    	border: solid #e0dede 2px;
        background-color: white;
        box-shadow: 1px 2px 1px 1px grey;
        }
        .login_btn{
        	background-color: #d8396c;
		    font-size: 15px;
		    font-weight: bold;
		    width: 89px;
		    color: white;
        }
    </style>
</head>
<body style="background-image: url('assets/images/log.jpg');background-size: cover;background-repeat: no-repeat;">
   <div class="container" style="margin-top: 300px;">
   	<div class="row">
   		<div class="col-md-6 col-md-offset-3 box_login">
   			<div class="row">
   				<div class="col-md-8 col-md-offset-2"><br>
   					<center><h3 style="color: #d8396c;">Login</h3></center><br>
   					<input type="text" name="name" placeholder="Username" class="contact-input form-control"><br>
   					<input type="password" name="name" placeholder="Password" class="contact-input form-control"><br>
                    <center><button type="submit" class="btn login_btn">Login</button></center>
   					<br><br>
   				</div>
   			</div>
   		</div>
   	</div>
   </div>
</body>
</html>


  