<style type="text/css">
  .quick_links{
    color: #565050;
    font-size: 17px;
  }
</style>
<!-- Footer Start here -->
<footer>
   <div class="footer-top">
      <div class="container">
         <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
               <div class="footer-item">
                  <div class="title">
                    <p style="font-size: 30px;"><span style="font-size: 45px;color: #ee257c;">B</span><span style="color: #16b6ef;">rain</span><span style="color: #ae00ff;">power</span></p>
                    <!-- <img src="<?php echo base_url();?>assets/images/logo.png" alt="logo" class="img-responsive"> --></div>
                  <div class="footer-about">
                     <p style="text-align: justify;">Brain Power is all about undertaking Brain Training exercises which can significantly improve the functions of the Brain. Brain training is beneficial for people regardless of their age. Children can develop their cognitive functions much faster, adults can get an edge at work, while older people can keep their mind fit through Brain training exercises.</p>
                  </div>
               </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
               <div class="footer-item">
                 <h4 class="title">Quick Links</h4>  
                 <div class="row">
                  <div class="col-md-6">
                    <a href="<?php echo base_url('index');?>" style="text-decoration: none;"><li class="quick_links ">Home</li></a>
                    <a href="<?php echo base_url('about');?>" style="text-decoration: none;"><li class="quick_links ">About</li></a>
                    <a href="<?php echo base_url('midbrain');?>" style="text-decoration: none;"><li class="quick_links ">Midbrain</li></a>
                    <a href="<?php echo base_url('dmit');?>" style="text-decoration: none;"><li class="quick_links ">DMIT</li></a>
                  </div>
                  <div class="col-md-6">
                    <a href="<?php echo base_url('gallery');?>" style="text-decoration: none;"><li class="quick_links ">Gallery</li></a>
                    <a href="<?php echo base_url('blog');?>" style="text-decoration: none;"><li class="quick_links ">Blog</li></a>
                    <a href="<?php echo base_url('event');?>" style="text-decoration: none;"><li class="quick_links ">News & Events</li></a>
                    <a href="<?php echo base_url('contact');?>" style="text-decoration: none;"><li class="quick_links ">Contact Us</li></a>
                  </div>
                 </div>
                 <!-- <ul style="list-style-type: circle;margin-left: 60px;">
                    <a href="<?php echo base_url('index');?>" style="text-decoration: none;"><li>Home</li></a>
                    <a href="<?php echo base_url('about');?>" style="text-decoration: none;"><li>About</li></a>
                    <a href="<?php echo base_url('midbrain');?>" style="text-decoration: none;"><li>Midbrain</li></a>
                    <a href="<?php echo base_url('dmit');?>" style="text-decoration: none;"><li>DMIT</li></a>
                    <a href="<?php echo base_url('gallery');?>" style="text-decoration: none;"><li>Gallery</li></a>
                    <a href="<?php echo base_url('blog');?>" style="text-decoration: none;"><li>Blog</li></a>
                    <a href="<?php echo base_url('event');?>" style="text-decoration: none;"><li>News & Events</li></a>
                    <a href="<?php echo base_url('contact');?>" style="text-decoration: none;"><li>Contact Us</li></a>
                 </ul> -->
               </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
               <div class="footer-item">
                   <h4 class="title">Contact Details</h4>
                  <div class="footer-about">
                     <ul>
                        <li><span><i class="fa fa-home" aria-hidden="true" style="padding: 6px;"></i></span> Pune,Maharashtra</li>
                        <li><span><i class="fa fa-phone" aria-hidden="true" style="padding: 6px;"></i></span> 9823262101</li>
                        <li><span><i class="fa fa-envelope-o" aria-hidden="true" style="padding: 6px;"></i></span> gupte_10@yahoo.com</li>
                        <li><span><i class="fa fa-globe" aria-hidden="true" style="padding: 6px;"></i></span>gupte_10@yahoo.com</li>
                     </ul>
                  </div>
               </div>
            </div>
   
         </div>
         <!-- row -->
      </div>
      <!-- container -->
   </div>
   <!-- footer top -->
   <div class="footer-bottom">
      <div class="container">
         <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
               <p>&copy; 2019. Designed & Developed By <a href="http://www.mavericksindia.com/home">Mavericks Technolabs(India) Pvt.Ltd.</a></p>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
               <ul class="social-default">
                  <li><a href="#"><i class="fa fa-facebook" aria-hidden="true" style="margin-top: 13px;"></i></a></li>
                  <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true" style="margin-top: 13px;"></i></a></li>
                  <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true" style="margin-top: 13px;"></i></a></li>
                  <li><a href="#"><i class="fa fa-twitter" aria-hidden="true" style="margin-top: 13px;"></i></a></li>
                 <!--  <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true" style="margin-top: 13px;"></i></a></li> -->
               </ul>
            </div>
         </div>
         <!-- row -->
      </div>
      <!-- container -->
   </div>
   <!-- footer bottom -->
</footer>
<a class="page-scroll scroll-top" href="#scroll-top"><i class="fa fa-angle-up" aria-hidden="true" style="margin-top: 16px;"></i></a>
<!-- Footer End here -->
<script src="<?php echo base_url();?>assets/js/jquery-1.12.4.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/isotope.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lightcase.js"></script>
<script src="<?php echo base_url();?>assets/js/waypoints.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.counterup.min.js"></script>
<script src="<?php echo base_url();?>assets/js/swiper.jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/circle-progress.min.js"></script>
<script src="<?php echo base_url();?>assets/js/velocity.min.js"></script>
<script src="<?php echo base_url();?>assets/js/quick-view.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.nstSlider.js"></script>
<script src="<?php echo base_url();?>assets/js/flexslider-min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url();?>assets/js/custom.js"></script>
</body>
</html>