<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title>Brainpower</title>
      <link href="<?php echo base_url();?>assets/images/favicon.png" rel="shortcut icon" type="image/vnd.microsoft.icon"/>
      <!-- Google fonts -->
      <link href="<?php echo base_url();?>assets/css/lora.css" rel="stylesheet">
      <!-- Bootstrap -->
      <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
      <!-- Font-awesome -->
      <link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet">
      <!-- Flaticon -->
      <link href="<?php echo base_url();?>assets/flaticon/flaticon.css" rel="stylesheet">
      <!-- lightcase -->
      <link href="<?php echo base_url();?>assets/css/lightcase.css" rel="stylesheet">
      <!-- Swiper -->
      <link href="<?php echo base_url();?>assets/css/swiper.css" rel="stylesheet">
      <!-- quick-view -->
      <link href="<?php echo base_url();?>assets/css/quick-view.css" rel="stylesheet">
      <!-- nstSlider -->
      <link href="<?php echo base_url();?>assets/css/jquery.nstSlider.css" rel="stylesheet">
      <!-- flexslider -->
      <link href="<?php echo base_url();?>assets/css/flexslider.css" rel="stylesheet">
      <!-- Style -->
      <link href="<?php echo base_url();?>assets/css/rtl.css" rel="stylesheet">
      <!-- Style -->
      <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
      <!-- Responsive -->
      <link href="<?php echo base_url();?>assets/css/responsive.css" rel="stylesheet">
   </head>
   <body id="scroll-top">
      <div class="mobile-menu-area">
         <a class="navbar-brand" href="<?php echo base_url('index');?>">
             <p style="font-size: 35px;"><span style="font-size: 50px;color: #ee257c;">B</span><span style="color: #16b6ef;">rain</span><span style="color: #ae00ff;">power</span></p><!-- <img src="<?php echo base_url();?>assets/images/logo.png" alt="logo" class="img-responsive"> --></a>
         <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="true">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         </button>
         <div class="mobile-menu">
            <ul class="m-menu">
               <li class="dropdown-submenu">
                  <a href="<?php echo base_url('index');?>">Home</a>
               </li>
               <li class="dropdown-submenu">
                  <a href="<?php echo base_url('about');?>">About</a>
               </li>
               <li class="dropdown-submenu">
                  <a href="#">Courses<span class="caret"></span></a>
                  <ul class="mobile-submenu">
                     <li><a href="<?php echo base_url('midbrain');?>">Mid Brain</a></li>
                     <li><a href="<?php echo base_url('dmit');?>">DMIT</a></li>
                     <li><a href="<?php echo base_url('chakrameditation');?>">Chakra Meditation</a></li>
                  </ul>
               </li>
               <li class="dropdown-submenu">
                  <a href="<?php echo base_url('gallery');?>">Gallery</a>
               </li>
               <li class="dropdown-submenu">
                  <a href="<?php echo base_url('blog');?>">Blog</a>
               </li>
               <li class="dropdown-submenu">
                  <a href="<?php echo base_url('event');?>">News & Events</a>
               </li>
               <li><a href="<?php echo base_url('contact');?>">Contact Us</a></li>
            </ul>
         </div>
      </div>
      <!--===============mobile-menu-area end===============-->
      <!-- Preloader start here -->
      <div id="loader-wrapper">
         <div id="loader"></div>
         <div class="loader-section section-left"></div>
         <div class="loader-section section-right"></div>
      </div>
      <!-- Preloader end here -->
      <header>
         <div class="header-top">
            <div class="container">
               <ul class="left">
                  <li><span><i class="fa fa-phone" aria-hidden="true"></i></span> Phone : 9823262101</li>
                  <!--  <li><span><i class="fa fa-clock-o" aria-hidden="true"></i></span> Opening Time : 9:30am-5:30pm</li> -->
                  <li><span><i class="fa fa-home" aria-hidden="true"></i></span> Address : Pune,Maharashtra</li>
               </ul>
              
            </div>
            <!-- container -->
         </div>
         <!-- header top -->
         <div class="main-menu">
            <nav class="navbar ">
               <div class="container">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                     <span class="sr-only">Toggle navigation</span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     </button>
                     <a class="navbar-brand" href="<?php echo base_url('index');?>"><!-- <img src="<?php echo base_url();?>assets/images/logo.png" alt="logo" class="img-responsive"> -->
                         <p style="font-size: 30px;"><span style="font-size: 45px;color: #ee257c;">B</span><span style="color: #16b6ef;">rain</span><span style="color: #ae00ff;">power</span></p>
                     </a>
                  </div>
                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                           <a href="<?php echo base_url('index');?>">Home </a>
                        </li>
                        <li class="dropdown">
                           <a href="<?php echo base_url('about');?>">About </a>
                        </li>
                        <li class="dropdown">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Courses <span class="caret"></span></a>
                           <ul class="dropdown-menu">
                              <li><a href="<?php echo base_url('midbrain');?>">Mid Brain</a></li>
                              <li><a href="<?php echo base_url('dmit');?>">DMIT</a></li>
                              <li><a href="<?php echo base_url('chakrameditation');?>">Chakra Meditation</a></li>
                           </ul>
                        </li>
                        <li class="dropdown">
                           <a href="<?php echo base_url('gallery');?>">Gallery </a>
                        </li>
                        <li class="dropdown">
                           <a href="<?php echo base_url('blog');?>">Blog </a>
                        </li>
                        <li class="dropdown">
                           <a href="<?php echo base_url('event');?>">News & Events </a>
                        </li>
                        <li><a href="<?php echo base_url('contact');?>">Contact</a></li>
                     </ul>
                  </div>
                  <!-- /.navbar-collapse -->
               </div>
               <!-- /.container -->
            </nav>
         </div>
         <!-- main menu -->
      </header>
      <!-- header End here -->