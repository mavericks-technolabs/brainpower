<?php

 /**
  * 
  */
 class FrontController extends CI_Controller
 {
 	
 	public function index()
 	{   
 		// $this->load->helper('html');
 		$this->load->view('include/header');
 		$this->load->view('frontend/index');
 		$this->load->view('include/footer');
 	}

 	public function about()
 	{   
 		// $this->load->helper('html');
 		$this->load->view('include/header');
 		$this->load->view('frontend/about');
 		$this->load->view('include/footer');
 	}

 	public function midbrain()
 	{   
 		// $this->load->helper('html');
 		$this->load->view('include/header');
 		$this->load->view('frontend/midbrain');
 		$this->load->view('include/footer');
 	}

 	public function dmit()
 	{   
 		// $this->load->helper('html');
 		$this->load->view('include/header');
 		$this->load->view('frontend/dmit');
 		$this->load->view('include/footer');
 	}

 	public function chakrameditation()
 	{   
 		// $this->load->helper('html');
 		$this->load->view('include/header');
 		$this->load->view('frontend/chakrameditation');
 		$this->load->view('include/footer');
 	}

 	public function gallery()
 	{   
 		// $this->load->helper('html');
 		$this->load->view('include/header');
 		$this->load->view('frontend/gallery');
 		$this->load->view('include/footer');
 	}

 	public function blog()
 	{   
 		// $this->load->helper('html');
 		$this->load->view('include/header');
 		$this->load->view('frontend/blog');
 		$this->load->view('include/footer');
 	}

    public function blog_details()
    {   
        // $this->load->helper('html');
        $this->load->view('include/header');
        $this->load->view('frontend/blog_details');
        $this->load->view('include/footer');
    }

 	public function event()
 	{   
 		// $this->load->helper('html');
 		$this->load->view('include/header');
 		$this->load->view('frontend/event');
 		$this->load->view('include/footer');
 	}
    public function more_events()
    {   
        // $this->load->helper('html');
        $this->load->view('include/header');
        $this->load->view('frontend/more_events');
        $this->load->view('include/footer');
    }
    public function more_news()
    {   
        // $this->load->helper('html');
        $this->load->view('include/header');
        $this->load->view('frontend/more_news');
        $this->load->view('include/footer');
    }


 	public function contact()
 	{   
 		// $this->load->helper('html');
 		$this->load->view('include/header');
 		$this->load->view('frontend/contact');
 		$this->load->view('include/footer');
 	}
 	public function Contacts()
	{
    
		$this->load->library('form_validation');

			$this->form_validation->set_rules('name','name','required');
			$this->form_validation->set_rules('email','email','required');
			$this->form_validation->set_rules('contact','contact','required');
			$this->form_validation->set_rules('subject','subject','required');
			$this->form_validation->set_rules('message','message','required');
			if ( $this->form_validation->run() )
			{
				$name=$this->input->post("name");
                $contact=$this->input->post("contact");
                $email=$this->input->post("email");
                $subject=$this->input->post("subject");
               	$message=$this->input->post("message");

				$enqu_data=['name'=>$name,'contact'=>$contact,'email'=>$email,'message'=>$message,'subject'=>$subject];
                $this->load->model('enquiry_model');

                if($this->enquiry_model->add_enquiry($enqu_data))
                    {
                    	$message="Message Sent Successfully.";
                        $this->session->set_flashdata('message',$message);
                        $this->session->set_flashdata('status','btn-success');
                        redirect(base_url('contact'));
                    }
                    else
                    {
                        $this->load->view('frontend/contact');
                    }
			}
			else
			{
                $this->load->view('include/header');
		 		$this->load->view('frontend/contact');
		 		$this->load->view('include/footer');
			}
	}
 }