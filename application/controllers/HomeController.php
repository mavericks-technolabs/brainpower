<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller
{

	public function dashboard()
	{
         $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
		$this->load->view('backend/header',['user_row'=>$user_row]);
		$this->load->view('backend/dashboard');
		$this->load->view('backend/footer');

	}
	public function all_user()
	{
        $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
        $this->load->model('user_model');
        $users = $this->user_model->user_list();
		$this->load->view('backend/all_users',['user_row'=>$user_row,'users'=>$users]);
	}
	public function add_user()
	{
         $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
		$this->load->view('backend/header',['user_row'=>$user_row]);
		$this->load->view('backend/add_users');
		$this->load->view('backend/footer');

	}
	public function adduser()
	{

		$config=[
               		'upload_path'=>'./uploads/users/',
                	'allowed_types'=>'jpg|gif|png|jpeg',
                ];
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
		  $this->load->library('form_validation');

		$this->form_validation->set_rules('user_firstname','user firstname','required');
		$this->form_validation->set_rules('user_lastname','user lastname','required');
		$this->form_validation->set_rules('user_email','user email','required');
        $this->form_validation->set_rules('user_contact','user contact','required');
        $this->form_validation->set_rules('user_address','user address','required');
        $this->form_validation->set_rules('username','username','required');
        $this->form_validation->set_rules('password','password','required');
            
            $this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");
			if($this->form_validation->run())
			{

                $config=[
                    'upload_path'=>'./uploads/users/',
                    'allowed_types'=>'jpg|gif|png|jpeg',
                ];
                $this->load->library('upload',$config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('userfile'))
                {
				    $fname=$this->input->post("user_firstname");
                    $lname=$this->input->post("user_lastname");
                    $mail=$this->input->post("user_email");
               	    $con=$this->input->post("user_contact");
               	    $user_address=$this->input->post("user_address");
               	    $uname=$this->input->post("username");
               	    $pass=$this->input->post("password");

                    $uid=$this->session->userdata('user_id');

                    $data=$this->upload->data();

                    $image_path=$data["raw_name"].$data['file_ext'];
                
                    $userdatas = ['user_firstname'=>$fname,'user_lastname'=>$lname,'user_email'=>$mail,'user_contact'=>$con,'user_address'=>$user_address,'username'=>$uname,'password'=>$pass,'user_id'=>$uid,'user_img'=>$image_path];
                  

                    $user_table=['username'=>$uname,'password'=>$pass,'firstname'=>$fname,'lastname'=>$lname,'user_img'=>$image_path];

 
                    $this->load->model('user_model');

                    if($this->user_model->add_user($userdatas,$user_table))
                    {
                        $error="New User Added Successfully.";
                        $this->session->set_flashdata('error',$error);
                        redirect(base_url('all_users'));
                    }
                    else
                    {
                        $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
                        $this->load->view('backend/header',['user_row'=>$user_row]);
                        $this->load->view('backend/add_users');
                        $this->load->view('backend/footer');
                    }
                }
                else
                {
                    $fname=$this->input->post("user_firstname");
                    $lname=$this->input->post("user_lastname");
                    $mail=$this->input->post("user_email");
               	    $con=$this->input->post("user_contact");
               	    $user_address=$this->input->post("user_address");
               	    $uname=$this->input->post("username");
               	    $pass=$this->input->post("password");

                    $uid=$this->session->userdata('user_id');
                
                    $image_path="dummy.png";
                
                     $userdatas = ['user_firstname'=>$fname,'user_lastname'=>$lname,'user_email'=>$mail,'user_contact'=>$con,'user_address'=>$user_address,'username'=>$uname,'password'=>$pass,'user_id'=>$uid,'user_img'=>$image_path];
                    
                    

                    $user_table=['username'=>$uname,'password'=>$pass,'firstname'=>$fname,'lastname'=>$lname,'user_img'=>$image_path];
 					
                    $this->load->model('user_model');

                    if($this->user_model->add_user($userdatas,$user_table))
                    {
                        $error="New User Added Successfully.";
                        $this->session->set_flashdata('error',$error);
                        $this->session->set_flashdata('status','btn-success');
                        redirect(base_url('all_users'));
                    }
                    else
                    {
                        $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
                        $this->load->view('backend/header',['user_row'=>$user_row]);
                        $this->load->view('backend/add_users');
                        $this->load->view('backend/footer');
                    }
                }
			}
			else
			{
			
                $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
                $this->load->view('backend/header',['user_row'=>$user_row]);
                $this->load->view('backend/add_users');
                $this->load->view('backend/footer');
			}


	}
     public function edit_user($value)
     {
       $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
        $this->load->view('backend/header',['user_row'=>$user_row]);
        $this->load->model("user_model");
        $use_row=$this->user_model->get_user($value);
        $this->load->view('backend/edit_users',['use_row'=>$use_row]);
        $this->load->view('backend/footer');
     }
      public function edituser()
     {
        $config=[
                    'upload_path'=>'./uploads/users/',
                    'allowed_types'=>'jpg|gif|png|jpeg',
                ];
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
            $this->load->library('form_validation');

            $this->form_validation->set_rules('user_firstname','user firstname','required');
            $this->form_validation->set_rules('user_lastname','user lastname','required');
            $this->form_validation->set_rules('user_email','user email','required');
            $this->form_validation->set_rules('user_contact','user contact','required');
            $this->form_validation->set_rules('user_address','user address','required');
            $this->form_validation->set_rules('username','username','required');
            $this->form_validation->set_rules('password','password','required');
            
            $this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");
            
            if($this->form_validation->run())
            {
                
                    $fname=$this->input->post("user_firstname");
                    $lname=$this->input->post("user_lastname");
                    $mail=$this->input->post("user_email");
                    $con=$this->input->post("user_contact");
                    $user_address=$this->input->post("user_address");
                    $uname=$this->input->post("username");
                    $pass=$this->input->post("password");
                    $uid=$this->input->post('id');
                    if($this->upload->do_upload('userfile'))
                     {
                        $old_image_path=$this->input->post("user_img");    
                        unlink("uploads/users/".$old_image_path);
                        $fdata = $this->upload->data();
                        $image_path=$fdata["raw_name"].$fdata['file_ext'];

                        $useredit = ['id'=>$uid,'user_firstname'=>$fname,'user_lastname'=>$lname,'user_email'=>$mail,'user_contact'=>$con,'user_address'=>$user_address,'username'=>$uname,'password'=>$pass,'user_img'=>$image_path,'user_id'=>$this->session->userdata('user_id')];
                       
                        $user_table_edit=['id'=>$uid,'username'=>$uname,'password'=>$pass,'firstname'=>$fname,'lastname'=>$lname,'user_img'=>$image_path];
                        $this->load->model('user_model');
                        if($this->user_model->insertuser($uid,$useredit,$user_table_edit))
                         {
                              $error="Edit User Successful.";
                              $this->session->set_flashdata('error',$error);
                              $this->session->set_flashdata('status','btn-success');
                              redirect(base_url('all_users'));
                          }
                        else
                         {
                              redirect(base_url('edit_user'));                         
                         }
                   }
                   else
                    {
                         $old_image_path=$this->input->post("user_img");
                         $useredit = ['id'=>$uid,'user_firstname'=>$fname,'user_lastname'=>$lname,'user_email'=>$mail,'user_contact'=>$con,'user_address'=>$user_address,'username'=>$uname,'password'=>$pass,'user_id'=>$uid,'user_img'=>$old_image_path];
                  

                        $user_table_edit=['id'=>$uid,'username'=>$uname,'password'=>$pass,'firstname'=>$fname,'lastname'=>$lname,'user_img'=>$old_image_path];

                            
                         $this->load->model('user_model');
                        if($this->user_model->insertuser($uid,$useredit,$user_table_edit))
                         {
                              $error="Edit User Successful.";
                              $this->session->set_flashdata('error',$error);
                              $this->session->set_flashdata('status','btn-success');
                              redirect(base_url('all_users'));
                          }
                        else
                         {
                              redirect(base_url('edit_user'));                         
                         }
                    }
                }
            else
            {
                $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
                $this->load->view('backend/header',['user_row'=>$user_row]);
                $this->load->model("user_model");
                $use_row=$this->user_model->get_user_edit($value);
                $this->load->view('backend/edit_users',['use_row'=>$use_row]);
                $this->load->view('backend/footer');
            }

     }
     public function delete_user($id,$image)
    {
        $this->load->model('user_model');
        if($this->user_model->delete_user($id))
        {

            unlink("uploads/users/".$image);
            redirect(base_url('all_users'));
        }
        else
        {
            echo "Delete Failed";       
            redirect(base_url('all_users'));
        }
    }
    public function all_gallery()
    {
        $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
         $this->load->model('gallery_model');
        $imgss = $this->gallery_model->all_gall_list();
        $this->load->view('backend/all_gallery',['user_row'=>$user_row,'imgss'=>$imgss]);

    }
     public function add_gallery()
    {
        $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
         $this->load->view('backend/header',['user_row'=>$user_row]);
         $this->load->view('backend/add_gallery');
        $this->load->view('backend/footer');
    }
    public function addgallery()
    {

        $uploadPath = 'uploads/gallery/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'jpg|jpeg|png|gif';                

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $this->load->library('form_validation');

        $this->form_validation->set_rules('img_title','image title','required');
        $this->form_validation->set_rules('img_desc','image description','required');

        $this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");

        if($this->form_validation->run() && !empty($_FILES['files']['name']))
        {
            $img_title=$this->input->post("img_title");
            $img_desc=$this->input->post("img_desc");
            $uid=$this->session->userdata("user_id");
            $this->load->helper('date');
            $Date= date('Y-m-d H:i:s');

              
            $gallerydata=['user_id'=>$uid,'img_title'=>$img_title,'img_desc'=>$img_desc,'created_at'=>$Date];
            
            $this->load->model('multiplemodel');
                $filesCount = count($_FILES['files']['name']);
                if($filesCount <= 4)
                {
                if($this->multiplemodel->insertgallery($gallerydata))
                {

                    $this->load->model("gallery_model");
                    $rest=$this->gallery_model->get_recent_sr($uid);
                    $title_id=$rest->title_id;

                        $data = array();
                        $filesCount = count($_FILES['files']['name']);
                        for($i = 0; $i < $filesCount; $i++)
                        {
                            $_FILES['file']['name']     = $_FILES['files']['name'][$i];
                            $_FILES['file']['type']     = $_FILES['files']['type'][$i];
                            $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                            $_FILES['file']['error']     = $_FILES['files']['error'][$i];
                            $_FILES['file']['size']     = $_FILES['files']['size'][$i];

                            if($this->upload->do_upload('file'))
                            {
                                $fileData = $this->upload->data();
                                   
                                $uploadData[$i]['image_name'] = $fileData['orig_name'];
                                $uploadData[$i]['image_path'] = "uploads/gallery/".$fileData['orig_name'];
                                $uploadData[$i]['title_id'] = $title_id;   
                            }
                        }
            
                        if(!empty($uploadData))
                        {
                            $this->load->model('multiplemodel','file');
                            $this->multiplemodel->insert($uploadData);                             
                        }
                         $error="New Gallery Added Successfully.";
                        $this->session->set_flashdata('error',$error);
                        $this->session->set_flashdata('status','btn-success');
                        redirect(base_url('all_gallery'));
                }
                else
                {
                    $this->session->set_flashdata('Feedback',"Record not inserted successfully.");
                    $this->session->set_flashdata('Feedback_class','alert-danger');
                }
            }
            else
            {
                $message="Do not upload more than 4 images.";
                $this->session->set_flashdata('message',$message);

                redirect(base_url('add_gallery'));
            }

                redirect(base_url('all_gallery'));
            
        }
        else
        {
            $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
            $this->load->view('backend/header',['user_row'=>$user_row]);
            $this->load->view('backend/add_gallery');
            $this->load->view('backend/footer');
        }              
    }
    public function edit_gallery($value)
        {
            $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
             $this->load->view('backend/header',['user_row'=>$user_row]);
            $this->load->model("gallery_model");

             $img_row=$this->gallery_model->get_img_des($value);
             $gall_img=$this->gallery_model->get_multi_images($value);
             $this->load->view('backend/edit_gallery',['img_row'=>$img_row,'gall_img'=>$gall_img]);

             $this->load->view('backend/footer');

    

        }

        public function update_gallery()
        {
            $uploadPath = 'uploads/gallery/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
                

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $this->load->library('form_validation');

             $this->form_validation->set_rules('img_title','image title','required');
            $this->form_validation->set_rules('img_desc','image description','required');

            $this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>"); 
            if ( $this->form_validation->run() && !empty($_FILES['files']['name']))
            {
                
                $img_title=$this->input->post("img_title");
                $img_desc=$this->input->post("img_desc");
                $uid=$this->session->userdata("user_id");
                $title_id=$this->input->post("title_id");
                $this->load->helper('date');
                $Date= date('Y-m-d H:i:s');

              
                $gallerydata=['user_id'=>$uid,'img_title'=>$img_title,'img_desc'=>$img_desc,'created_at'=>$Date,'title_id'=>$title_id];
                $this->load->model('gallery_model');

                    if($this->gallery_model->update_gallery($title_id,$gallerydata))
                    {
                         $data = array();
                         $filesCount = count($_FILES['files']['name']);
                          for($i = 0; $i < $filesCount; $i++)
                          {
                                $_FILES['file']['name']     = $_FILES['files']['name'][$i];
                                $_FILES['file']['type']     = $_FILES['files']['type'][$i];
                                $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                                $_FILES['file']['error']     = $_FILES['files']['error'][$i];
                                $_FILES['file']['size']     = $_FILES['files']['size'][$i];

                                 if($this->upload->do_upload('file'))
                                {
                                   
                                   $fileData = $this->upload->data();
                                   
                                   $uploadData[$i]['image_name'] = $fileData['orig_name'];
                                   $uploadData[$i]['image_path'] = "uploads/gallery/".$fileData['orig_name'];
                                   $uploadData[$i]['title_id'] = $title_id;
                                 
                                 
                                }
                         }
            
                            if(!empty($uploadData))
                            {
                               
                                $this->load->model('gallery_model','file');
                                $this->gallery_model->insert($uploadData);
                             
                            }
                              $error="Gallery Edit Successful.";
                                $this->session->set_flashdata('error',$error);
                                $this->session->set_flashdata('status','btn-success');
                              redirect(base_url('all_gallery'));
                            }
                            else
                            {
                                $this->session->set_flashdata('Feedback',"Record not inserted successfully.");
                                $this->session->set_flashdata('Feedback_class','alert-danger');
                                 
                          } 
                           redirect(base_url('all_gallery'));
                                
                        
                   }

            else{

                    $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
                     $this->load->view('backend/header',['user_row'=>$user_row]);
                    $this->load->helper('form');
                    $this->load->model("gallery_model");

                     $img_row=$this->gallery_model->get_img_des_edit();
                     $gall_img=$this->gallery_model->get_multi_images_edit();
                     $this->load->view('backend/edit_gallery',['img_row'=>$img_row,'gall_img'=>$gall_img]);

                     $this->load->view('backend/footer');

                }
              
              
      }

        
        public function delete_images($id,$img_id)
        {
            // print_r($img_id);
            $this->load->model("gallery_model");
            $image=$this->gallery_model->get_image($img_id);
            $image_path = $image->image_name; 
            
            $this->load->helper("file");
            
            $a=$this->gallery_model->del_image($img_id);
           
            if ($a)
            {
                unlink("uploads/gallery/".$image_path);
                redirect(base_url()."edit_gallery/".$id);
            }
            
            
         }
        public function delete_gallery($id)
        {              
            $this->load->model("gallery_model");

            //$title_id = $this->input->post('title_id');
            $this->load->helper('form');
            $this->load->model('gallery_model');
            $imgss = $this->gallery_model->all_img_list($id);
            foreach ($imgss as $abc)
            {
                unlink("./".$abc->image_path);
            }
             

                if($this->gallery_model->delete_gallery_all($id,$post))
                {
                    
                       
                        redirect(base_url('all_gallery'));
                }
                else{
          
                }
                       redirect(base_url('all_gallery'));
            }
        public function all_courses()
        {
            $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
            $this->load->model('add_coursemodel');
            $courseall = $this->add_coursemodel->course_list();
            $this->load->view('backend/all_courses',['user_row'=>$user_row,'courseall'=>$courseall]);
        }

        public function add_course()
        {
           $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
           $this->load->view('backend/header',['user_row'=>$user_row]);
           $this->load->view('backend/add_course');
           $this->load->view('backend/footer');
        }

         public function addcourse()
         {
                $this->load->library('form_validation');

                $this->form_validation->set_rules('course_name','course_name','required');
                $this->form_validation->set_rules('course_fees','course_fees','required');
                $this->form_validation->set_rules('course_details','course_details','required');

                $this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");
                if ( $this->form_validation->run() )
                {
                        
                    $cname=$this->input->post('course_name');
                    $cfees=$this->input->post('course_fees');
                    $cdetails=$this->input->post('course_details');

                    $coursedetails=['course_name'=>$cname,'course_fees'=>$cfees,'course_details'=>$cdetails,'user_id'=>$this->session->userdata('user_id')];
                      
                    $this->load->model('add_coursemodel');
                    if ( $this->add_coursemodel->add_course($coursedetails))
                    {
                        $error="New Course Added Successfully.";
                        $this->session->set_flashdata('error',$error);
                        $this->session->set_flashdata('status','btn-success');
                        redirect(base_url('all_courses'));                   
                    }
                    else
                    {
                        $this->load->view('backend/add_course');
                    }
        }
        else
        {
                 $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
                 $this->load->view('backend/header',['user_row'=>$user_row]);
                $this->load->view('backend/add_course');
                $this->load->view('backend/footer');
        }
    }
    //--------------------------------Add Course End-----------------------------------------//



    public function get_course()
    {
        $course_id=$_POST['course_id'];
        $this->load->model('add_coursemodel');
        $find_course=$this->add_coursemodel->get_course($course_id);
        $data['json']=$find_course;
        echo json_encode($data);
    }



    //--------------------------------Edit Course-----------------------------------------//

    public function edit_course($value)
    {
        $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
        $this->load->view('backend/header',['user_row'=>$user_row]);
        $this->load->helper('form');
        $this->load->model("add_coursemodel");
        $cour_row=$this->add_coursemodel->get_course($value);
        $this->load->view('backend/edit_course',['cour_row'=>$cour_row]);
        $this->load->view('backend/footer');
     }

     public function editcourse()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('course_name','course name','required');
        $this->form_validation->set_rules('course_details','course details','required');
        $this->form_validation->set_rules('course_fees','course fees','required');

        $this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");         
            if($this->form_validation->run())
            {
                $cname=$this->input->post('course_name');
                $cdetails=$this->input->post('course_details');
                $cfees=$this->input->post('course_fees');
                $course_id=$this->input->post('course_id');

                $editcourses=['course_id'=>$course_id,'course_name'=>$cname,'course_details'=>$cdetails,'course_fees'=>$cfees,'user_id'=>$this->session->userdata('user_id')];
                $this->load->model('add_coursemodel');

                if($this->add_coursemodel->insertcourse($course_id,$editcourses))
                {
                    $error="Course Edit Successful.";
                    $this->session->set_flashdata('error',$error);
                    $this->session->set_flashdata('status','btn-success');
                    redirect(base_url('all_courses'));
                }
                else
                {
                    redirect(base_url('add_course'));
                     
               }
            }
            else
            {
                redirect(base_url('all_courses'));   
            }
    }

    //--------------------------------Edit Course End-----------------------------------------//




    //--------------------------------Delet Course-----------------------------------------//

    public function delete_course($course_id)
    {
        $this->load->model('add_coursemodel');                    
                         

        if($this->add_coursemodel->delete_courses($course_id))
        {
            redirect(base_url('all_courses'));
        }
        else
        {
            echo "Delete Failed";       
            redirect(base_url('all_courses'));
        }
    }

    //----------------------------------News----------------------------------------//

    public function all_newsevents()
    {
        $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
        $this->load->model('news_model');
        $newsdatas = $this->news_model->news_list();
        $this->load->view('backend/all_newsevents', ['user_row'=>$user_row,'newsdatas'=>$newsdatas]);

    }
    public function addnewsevents()
    {
        $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
        $this->load->view('backend/header',['user_row'=>$user_row]);
        $this->load->view('backend/add_newsevents');
        $this->load->view('backend/footer');
    }
    public function add_newsevents()
    {
    
        $config=[
                    'upload_path'=>'./uploads/news/',
                    'allowed_types'=>'jpg|gif|png|jpeg',
                ];
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        $this->load->library('form_validation');

        $this->form_validation->set_rules('news_title','news title','required');
        $this->form_validation->set_rules('news_desc','news description','required');
        $this->form_validation->set_rules('category','news category','required');
       
        $this->form_validation->set_rules('dates','news date','required');
       /* $this->form_validation->set_rules('times','news time','required');
        $this->form_validation->set_rules('location','news location','required');  */         
            
        $this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");
        if($this->form_validation->run() && $this->upload->do_upload('userfile'))
        {
           
            $news_title=$this->input->post("news_title");
            $news_desc=$this->input->post("news_desc");
            $dates=$this->input->post("dates");
            $category=$this->input->post("category");
            $times=$this->input->post("times");
            $location=$this->input->post("location");
            $uid=$this->session->userdata('user_id');

            $data=$this->upload->data();
            $image_path=$data["raw_name"].$data['file_ext'];
                
            $newsdata = ['news_title'=>$news_title,'news_desc'=>$news_desc,'user_id'=>$uid,'news_img'=>$image_path,'dates'=>$dates,'times'=>$times,'location'=>$location,'category'=>$category];
           
            $this->load->model('news_model');

            if($this->news_model->add_news($newsdata))
            {
                $error="News and Events Created Successfully.";
                $this->session->set_flashdata('error',$error);
                $this->session->set_flashdata('status','btn-success');
                redirect(base_url('all_newsevents'));
            }
            else
            {
                redirect(base_url('add_newsevents'));                              
            }
        }
        else
        {
            $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
        $this->load->view('backend/header',['user_row'=>$user_row]);
        $this->load->view('backend/add_newsevents');
        $this->load->view('backend/footer');                                
        }

           
     }
    


      public function edit_newsevents($value)
     {
        $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
        $this->load->view('backend/header',['user_row'=>$user_row]);
        $this->load->helper('form');
         $this->load->model("news_model");
        $news_row=$this->news_model->get_news($value);
        $this->load->view('backend/edit_newsevents',['news_row'=>$news_row]);
        $this->load->view('backend/footer');
        

     }
     public function editnewsevents()
     {

        $config=[
                    'upload_path'=>'uploads/news/',
                    'allowed_types'=>'jpg|gif|png|jpeg',
                ];
            $this->load->library('upload');
            $this->upload->initialize($config);

            $this->load->library('form_validation');

            $this->form_validation->set_rules('news_title','news title','required');
            $this->form_validation->set_rules('news_desc','news_desc','required');
             $this->form_validation->set_rules('category','news category','required');
             $this->form_validation->set_rules('dates','news date','required');
            
            $this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");
            if($this->form_validation->run())
            {

                $news_title=$this->input->post("news_title");
                $news_desc=$this->input->post("news_desc");
                $dates=$this->input->post("dates");
                $category=$this->input->post("category");
                $times=$this->input->post("times");
                $location=$this->input->post("location");
                $uid=$this->session->userdata('user_id');
                $news_id=$this->input->post('news_id');
               if($this->upload->do_upload('userfile'))
               {
                   $old_image_path="".$this->input->post("news_img");
                   unlink("uploads/news/".$old_image_path);

                   $fdata = $this->upload->data();
                   $image_path=$fdata["raw_name"].$fdata['file_ext'];
        
                    $editdata = ['news_title'=>$news_title,'news_desc'=>$news_desc,'user_id'=>$uid,'news_img'=>$image_path,'dates'=>$dates,'times'=>$times,'location'=>$location,'category'=>$category,'news_id'=>$news_id];
                   
                    $this->load->model('news_model');

                if($this->news_model->update_news($news_id,$editdata))
                    {
                        $error="Edit Event Successful.";
                        $this->session->set_flashdata('error',$error);
                        $this->session->set_flashdata('status','btn-success');
                        redirect(base_url('all_newsevents'));
                    }
                    else
                    {
                         redirect(base_url('edit_newsevents'));                              
                    }
                
                }
                else
                {
                    $old_image_path="".$this->input->post("news_img");
                    $editdata = ['news_id'=>$news_id,'news_title'=>$news_title,'news_desc'=>$news_desc,'user_id'=>$uid,'dates'=>$dates,'times'=>$times,'location'=>$location,'category'=>$category,'news_img'=>$old_image_path];
               
                     $this->load->model('news_model');

                    if($this->news_model->update_news($news_id,$editdata))
                    {
                        $error="Edit Event Successful.";
                        $this->session->set_flashdata('error',$error);
                        $this->session->set_flashdata('status','btn-success');
                        redirect(base_url('all_newsevents'));
                    }
                    else
                    {
                        redirect(base_url('edit_newsevents'));                              
                    }
                }
            }
            else
            {
                $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
                $this->load->view('backend/header',['user_row'=>$user_row]);
                $this->load->model('news_model');
                $news_row=$this->news_model->get_news_edit();
                $this->load->view('backend/edit_newsevents',['news_row'=>$news_row]);
                $this->load->view('backend/footer');
            }
     }
    
    public function delete_news($news_id,$news_img)
    {
        $this->load->model('news_model');                    
                         

        if($this->news_model->delete_news($news_id))
        {
            unlink("uploads/news/".$news_img);
            redirect(base_url('all_newsevents'));
        }
        else
        {
            echo "Delete Failed";       
            redirect(base_url('all_newsevents'));
        }
    }




    //----------------------------------News End----------------------------------------//

    //----------------------Blog start--------------------------------------//

    public function all_blogs()
    {
        $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
        $this->load->model('blogs_model');
        $blogdatas = $this->blogs_model->blogs_list();
        $this->load->view('backend/all_blogs', ['user_row'=>$user_row,'blogdatas'=>$blogdatas]);

    }
    public function addblogs()
    {
        $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
        $this->load->view('backend/header',['user_row'=>$user_row]);
        $this->load->view('backend/add_blogs');
        $this->load->view('backend/footer');
    }
    public function add_blogs()
    {
        $config=[
                    'upload_path'=>'./uploads/blogs/',
                    'allowed_types'=>'jpg|gif|png|jpeg',
                ];
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        $this->load->library('form_validation');

        $this->form_validation->set_rules('blog_title','blog title','required');
        $this->form_validation->set_rules('blog_desc','blog description','required');            
            
        $this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");
        if($this->form_validation->run() && $this->upload->do_upload('userfile'))
        {
            $blog_title=$this->input->post("blog_title");
            $blog_desc=$this->input->post("blog_desc");
            $uid=$this->session->userdata('user_id');
             $this->load->helper('date');
            $Date= date('Y-m-d H:i:s');

            $data=$this->upload->data();
            $image_path=$data["raw_name"].$data['file_ext'];
                
            $blogdata = ['blog_title'=>$blog_title,'blog_desc'=>$blog_desc,'user_id'=>$uid,'blog_img'=>$image_path,'created_at'=>$Date];
            $this->load->model('blogs_model');

            if($this->blogs_model->add_blogs($blogdata))
            {
                $error="Blog Created Successfully.";
                $this->session->set_flashdata('error',$error);
                $this->session->set_flashdata('status','btn-success');
                redirect(base_url('all_blogs'));
            }
            else
            {
                redirect(base_url('add_blogs'));                              
            }
        }
        else
        {
            $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
        $this->load->view('backend/header',['user_row'=>$user_row]);
        $this->load->view('backend/add_blogs');
        $this->load->view('backend/footer');                                
        }

           
     }

      public function edit_blogs($value)
     {
        $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
        $this->load->view('backend/header',['user_row'=>$user_row]);
        $this->load->helper('form');
         $this->load->model("blogs_model");
        $blog_row=$this->blogs_model->get_blogs($value);
        $this->load->view('backend/edit_blogs',['blog_row'=>$blog_row]);
        $this->load->view('backend/footer');
        

     }
     public function editblogs()
     {

        $config=[
                    'upload_path'=>'uploads/blogs/',
                    'allowed_types'=>'jpg|gif|png|jpeg',
                ];
            $this->load->library('upload');
            $this->upload->initialize($config);

            $this->load->library('form_validation');

            $this->form_validation->set_rules('blog_title','blog title','required');
            $this->form_validation->set_rules('blog_desc','blog description','required');
            
            $this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");
            if($this->form_validation->run())
            {

               $blog_id=$this->input->post("blog_id");
               $blog_title=$this->input->post("blog_title");
               $blog_desc=$this->input->post("blog_desc");
               $uid=$this->session->userdata('user_id');
               $this->load->helper('date');
               $Date= date('Y-m-d H:i:s');
               if($this->upload->do_upload('userfile'))
               {
               $old_image_path="".$this->input->post("news_img");
               unlink("uploads/blogs/".$old_image_path);

               $fdata = $this->upload->data();
               $image_path=$fdata["raw_name"].$fdata['file_ext'];
    
                $editdata = ['blog_id'=>$blog_id,'blog_title'=>$blog_title,'blog_desc'=>$blog_desc,'user_id'=>$uid,'blog_img'=>$image_path,'created_at'=>$Date];
               
                $this->load->model('blogs_model');

                if($this->blogs_model->update_blogs($blog_id,$editdata))
                    {
                        $error="Edit Blog Successful.";
                        $this->session->set_flashdata('error',$error);
                        $this->session->set_flashdata('status','btn-success');
                        redirect(base_url('all_blogs'));
                    }
                    else
                    {
                         redirect(base_url('edit_blogs'));                              
                    }
                
                }
                else
                {
                    $old_image_path="".$this->input->post("blog_img");
                    $editdata = ['blog_id'=>$blog_id,'blog_title'=>$blog_title,'blog_desc'=>$blog_desc,'user_id'=>$uid,'blog_img'=>$old_image_path];
               
                     $this->load->model('blogs_model');

                    if($this->blogs_model->update_blogs($blog_id,$editdata))
                    {
                        $error="Edit Blog Successful.";
                        $this->session->set_flashdata('error',$error);
                        $this->session->set_flashdata('status','btn-success');
                        redirect(base_url('all_blogs'));
                    }
                    else
                    {
                        redirect(base_url('edit_blogs'));                              
                    }
                }
            }
            else
            {
                $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
                $this->load->view('backend/header',['user_row'=>$user_row]);
                $this->load->model('blogs_model');
                $blogs_row=$this->blogs_model->get_blogs_edit();
                $this->load->view('backend/edit_blogs',['blogs_row'=>$blogs_row]);
                $this->load->view('backend/footer');
            }
     }
    
    public function delete_blogs($blog_id,$blog_img)
    {

        $this->load->model('blogs_model');                    
                         

        if($this->blogs_model->delete_blogs($blog_id))
        {
            unlink("uploads/blogs/".$blog_img);
            redirect(base_url('all_blogs'));
        }
        else
        {
            echo "Delete Failed";       
            redirect(base_url('all_blogs'));
        }
    }

//----------------------blog end-----------------------------------------//

//---------------success stories------------------------------//
    public function all_stories()
    {
        $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
        $this->load->model('sucessstory_model');
        $stories = $this->sucessstory_model->stories_list();
        $this->load->view('backend/all_stories', ['stories'=>$stories,'user_row'=>$user_row]);
    }
      public function add_success_stories()
    {
        $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
        $this->load->view('backend/header',['user_row'=>$user_row]);
         $this->load->view('backend/add_success_stories');
         $this->load->view('backend/footer');
    }
    public function addstories()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('story_title','story_title','required');
        $this->form_validation->set_rules('story_desc','story_desc','required');
        $this->form_validation->set_rules('story_by','story_by','required');
        $this->form_validation->set_rules('story_loc','course story_loc','required');

        $this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");
        if ( $this->form_validation->run() )
        {
                
            $story_title=$this->input->post('story_title');
            $story_desc=$this->input->post('story_desc');
            $story_by=$this->input->post('story_by');
            $story_loc=$this->input->post('story_loc');

            $storydetails=['story_title'=>$story_title,'story_desc'=>$story_desc,'story_by'=>$story_by,'story_loc'=>$story_loc];
              
            $this->load->model('sucessstory_model');
            if ( $this->sucessstory_model->add_stories($storydetails))
            {
                $error="New Success Story Added Successfully.";
                $this->session->set_flashdata('error',$error);
                $this->session->set_flashdata('status','btn-success');
                redirect(base_url('all_stories'));                   
            }
            else
            {
                $this->load->view('backend/add_success_stories');
            }
        }
        else
        {
            $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
        $this->load->view('backend/header',['user_row'=>$user_row]);
         $this->load->view('backend/add_success_stories');
         $this->load->view('backend/footer');
        }
    }
    public function edit_success_stories($value)
    {
        $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
        $this->load->view('backend/header',['user_row'=>$user_row]);
        $this->load->helper('form');
        $this->load->model("sucessstory_model");
        $story_row=$this->sucessstory_model->get_stories($value);
        $this->load->view('backend/edit_success_stories',['story_row'=>$story_row]);
        $this->load->view('backend/footer');
     }

     public function editstory()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('story_title','story_title','required');
        $this->form_validation->set_rules('story_desc','story_desc','required');
        $this->form_validation->set_rules('story_by','story_by','required');
        $this->form_validation->set_rules('story_loc','course story_loc','required');

        $this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");
        if ( $this->form_validation->run() )
        {
                
                $story_title=$this->input->post('story_title');
                $story_desc=$this->input->post('story_desc');
                $story_by=$this->input->post('story_by');
                $story_loc=$this->input->post('story_loc');
                $id=$this->input->post('id');

                $storyedit=['story_title'=>$story_title,'story_desc'=>$story_desc,'story_by'=>$story_by,'story_loc'=>$story_loc,'id'=>$id];

   
                $this->load->model('sucessstory_model');

                if($this->sucessstory_model->insertstory($id,$storyedit))
                {

                    $error="Success Story Edit Successful.";
                    $this->session->set_flashdata('error',$error);
                    $this->session->set_flashdata('status','btn-success');
                    redirect(base_url('all_stories'));
                }
                else
                {
                    redirect(base_url('add_success_stories'));
                     
               }
            }
            else
            {
                redirect(base_url('all_stories'));   
            }
        }

        public function delete_story($id)
        {
            $this->load->model('sucessstory_model');                    
                             

            if($this->sucessstory_model->delete_story($id))
            {
                redirect(base_url('all_stories'));
            }
            else
            {
                echo "Delete Failed";       
                redirect(base_url('all_stories'));
            }
        }

//------------------end stories------------------------------//
        //-------------------------About Page---------------//

        public function all_aboutpage()
    {
        $this->load->model('about_page_model');
        $user_row=$this->mlogin->get_user($this->session->userdata('user_id')); 
        $about = $this->about_page_model->about_list();       
        $this->load->view('backend/all_aboutpage',['about'=>$about,'user_row'=>$user_row]);
    }
     public function edit_aboutpage($value)
    {
        $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));        
        $this->load->view('backend/header',['user_row'=>$user_row]);
        $this->load->helper('form');
        $this->load->model("about_page_model");
        $about_row=$this->about_page_model->get_aboutpage($value);
        $about_img=$this->about_page_model->get_multi_images($value);
        $this->load->view('backend/edit_aboutpage',['about_row'=>$about_row,'about_img'=>$about_img]);
        $this->load->view('backend/footer');
    }
    public function edit_about_set()
    {
       

            $uploadPath = 'uploads/aboutpage/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
                

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            $this->load->library('form_validation');

            $this->form_validation->set_rules('name','name','required');
            $this->form_validation->set_rules('desig','desig','required');
            $this->form_validation->set_rules('about_me','about_me','required');
            $this->form_validation->set_rules('certified_in','certified_in','required');
            $this->form_validation->set_rules('personal_stat','personal_stat','required');
            $this->form_validation->set_rules('other_stat','other_stat','required');
            $this->form_validation->set_rules('mission','mission','required');
            $this->form_validation->set_rules('vission','vission','required');
            $this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>"); 
            if ( $this->form_validation->run() && !empty($_FILES['files']['name']))
            {

                $name=$this->input->post('name');
                $desig=$this->input->post('desig');
                $about_me=$this->input->post('about_me');
                $certified_in=$this->input->post('certified_in');
                $personal_stat=$this->input->post('personal_stat');
                $other_stat=$this->input->post('other_stat');
                $mission=$this->input->post("mission");
                $vission=$this->input->post("vission");
                $aboutpage_id=$this->input->post('aboutpage_id');

              
                $abouts = ['name'=>$name,'desig'=>$desig,'about_me'=>$about_me,'certified_in'=>$certified_in,'personal_stat'=>$personal_stat,'other_stat'=>$other_stat,'mission'=>$mission,'vission'=>$vission,'aboutpage_id'=>$aboutpage_id];


                // print_r($articledata);exit();
                $this->load->model('about_page_model');

                if($this->about_page_model->update_about_images($aboutpage_id,$abouts))
                {
                    $data = array();
                    $filesCount = count($_FILES['files']['name']);
                    for($i = 0; $i < $filesCount; $i++)
                    {
                        $_FILES['file']['name']     = $_FILES['files']['name'][$i];
                        $_FILES['file']['type']     = $_FILES['files']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                        $_FILES['file']['error']     = $_FILES['files']['error'][$i];
                        $_FILES['file']['size']     = $_FILES['files']['size'][$i];

                         if($this->upload->do_upload('file'))
                        {                       
                           $fileData = $this->upload->data();
                           
                           $uploadData[$i]['image_name'] = $fileData['orig_name'];
                           $uploadData[$i]['image_path'] = "uploads/aboutpage/".$fileData['orig_name'];
                           $uploadData[$i]['aboutpage_id'] = $aboutpage_id;
                        }
                        else
                        {
                            $name=$this->input->post('name');
                            $desig=$this->input->post('desig');
                            $about_me=$this->input->post('about_me');
                            $certified_in=$this->input->post('certified_in');
                            $personal_stat=$this->input->post('personal_stat');
                            $other_stat=$this->input->post('other_stat');
                            $mission=$this->input->post("mission");
                            $vission=$this->input->post("vission");
                            $aboutpage_id=$this->input->post('aboutpage_id');

                          
                            $abouts = ['name'=>$name,'desig'=>$desig,'about_me'=>$about_me,'certified_in'=>$certified_in,'personal_stat'=>$personal_stat,'other_stat'=>$other_stat,'mission'=>$mission,'vission'=>$vission,'aboutpage_id'=>$aboutpage_id];


                        }
                    }
        
                    if(!empty($uploadData))
                    {
                        $this->load->model('about_page_model','file');
                        $this->about_page_model->insert($uploadData);
                    }
                    $error="About page Edit Successful.";
                    $this->session->set_flashdata('error',$error);
                    $this->session->set_flashdata('status','btn-success');
                    redirect(base_url('all_aboutpage'));
                }
                else
                {
                    $this->session->set_flashdata('Feedback',"Record not inserted successfully.");
                    $this->session->set_flashdata('Feedback_class','alert-danger');
                } 
                redirect(base_url('all_aboutpage'));
            }
            else
            {
                $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));        
                $this->load->view('backend/header',['user_row'=>$user_row]);
                $this->load->helper('form');
                $this->load->model("about_page_model");
                $about_row=$this->about_page_model->get_aboutpage($value);
                 $about_img=$this->about_page_model->get_multi_images($value);
                $this->load->view('backend/edit_aboutpage',['about_row'=>$about_row,'about_img'=>$about_img]);
                $this->load->view('backend/footer');
            }
    }
     public function delete_about_images($aboutpage_id,$img_id)
        {
            // print_r($img_id);
            $this->load->model("about_page_model");
            $image=$this->about_page_model->get_about_image($img_id);
            $image_path = $image->image_name; 
            
            $this->load->helper("file");
            
            $a=$this->about_page_model->del_about_image($img_id);
           
            if ($a)
            {
                unlink("uploads/aboutpage/".$image_path);
                redirect(base_url()."edit_aboutpage/".$aboutpage_id);
            }
            
            
         }


         //--------------Global setting-------------------------//


    public function all_global_settings()
    {
         $user_row=$this->mlogin->get_user($this->session->userdata('user_id')); 
        $this->load->model('global_setting');
        $setting = $this->global_setting->global_list();     
        $this->load->view('backend/all_global_setting',['setting'=>$setting,'user_row'=>$user_row]);
    }

    public function edit_global_settings($value)
    {
        $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));        
        $this->load->view('backend/header',['user_row'=>$user_row]);
        $this->load->helper('form');
        $this->load->model("global_setting");
        $setting_row=$this->global_setting->get_setting($value);
        $this->load->view('backend/edit_global_settings',['setting_row'=>$setting_row]);
        $this->load->view('backend/footer');
    }

    public function edit_global_set()
    {
        $address=$this->input->post("address");
        $phone=$this->input->post("phone");
        $email=$this->input->post("email");
        $facebook=$this->input->post("facebook");
        $twitter=$this->input->post("twitter");
        $instagram=$this->input->post("instagram");
        $about_brainpower=$this->input->post("about_brainpower");
        $did_you_know=$this->input->post('did_you_know');
        $total_stud=$this->input->post('total_stud');
        $total_courses=$this->input->post('total_courses');
        $total_class=$this->input->post('total_class');
        $total_teacher=$this->input->post('total_teacher');

        $company = ['facebook'=>$facebook,'phone'=>$phone,'address'=>$address,'email'=>$email,'about_brainpower'=>$about_brainpower,'twitter'=>$twitter,'instagram'=>$instagram,'did_you_know'=>$did_you_know,'total_stud'=>$total_stud,'total_courses'=>$total_courses,'total_teacher'=>$total_teacher,'total_class'=>$total_class];

        $this->load->model('global_setting');

        if($this->global_setting->updatesetting($company))
        {
            $error="Global setting Edited Successful.";
            $this->session->set_flashdata('error',$error);
            $this->session->set_flashdata('status','btn-success');
            redirect(base_url('all_global_settings'));
        }
    }
//--------------end of global setting--------------------------------//

//------------my profile--------------------------------------//
    public function profile()
    {
        $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
        $value = $user_row->user_id;
       $this->load->view('backend/header',['user_row'=>$user_row]);
        $this->load->model("user_model");
        $use_rows=$this->user_model->get_user($value);
        $this->load->view('backend/profile',['use_rows'=>$use_rows]);
        $this->load->view('backend/footer');
    }
     public function updateprofile()

     {
       $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
       
       
            $uid=$this->session->userdata('user_id');

            $old_pass=$this->input->post('password');
                        $config=[
                        'upload_path'=>'./uploads/users/',
                        'allowed_types'=>'jpg|gif|png|jpeg',
                    ];
            $this->load->library('upload',$config);
            $this->upload->initialize($config);
            $this->load->library('form_validation');

            $this->form_validation->set_rules('user_firstname','user firstname','required');
            $this->form_validation->set_rules('user_lastname','user lastname','required');
            $this->form_validation->set_rules('user_email','user email','required');
            $this->form_validation->set_rules('user_contact','user contact','required');
            $this->form_validation->set_rules('user_address','user address','required');
            $this->form_validation->set_rules('username','username','required');
            $this->form_validation->set_rules('password','password','required');
            
            $this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");
            
            if(true)
            {

                
                    $fname=$this->input->post("user_firstname");
                    $lname=$this->input->post("user_lastname");
                    $mail=$this->input->post("user_email");
                    $con=$this->input->post("user_contact");
                    $user_address=$this->input->post("user_address");
                    $uname=$this->input->post("username");
                    $pass=$this->input->post("password");
                    $uid=$this->input->post('id');
                    if($this->upload->do_upload('userfile'))
                    {
                            $old_image_path=$this->input->post("user_img");    
                            unlink("uploads/users/".$old_image_path);
                            $fdata = $this->upload->data();
                            $image_path=$fdata["raw_name"].$fdata['file_ext'];

                            $useredit = ['id'=>$uid,'user_firstname'=>$fname,'user_lastname'=>$lname,'user_email'=>$mail,'user_contact'=>$con,'user_address'=>$user_address,'username'=>$uname,'password'=>$pass,'user_img'=>$image_path,'user_id'=>$this->session->userdata('user_id')];
                           
                            $user_table_edit=['id'=>$uid,'username'=>$uname,'password'=>$pass,'firstname'=>$fname,'lastname'=>$lname,'user_img'=>$image_path];
                            $this->load->model('user_model');
                            if($user_row->password == $pass)
                            {
                                if($this->user_model->insertuser($uid,$useredit,$user_table_edit))
                                {

                                  $error="Edit Profile Successful.";
                                  $this->session->set_flashdata('error',$error);
                                  $this->session->set_flashdata('status','btn-success');
                                  redirect(base_url('profile'));
                                }
                                else
                                 {
                                      redirect(base_url('profile'));                         
                                 }
                            }
                            else
                            {
                                if($this->user_model->insertuser($uid,$useredit,$user_table_edit))
                                {

                                  $error="Edit Profile Successful.";
                                  $this->session->set_flashdata('error',$error);
                                  $this->session->set_flashdata('status','btn-success');
                                  $this->session->sess_destroy('user_id');
                                  redirect(base_url('login_form'));
                                }
                                else
                                 {
                                      redirect(base_url('profile'));                         
                                 }
                            }
                            
                    }
                    else
                    {
                                 $old_image_path=$this->input->post("user_img");
                                 $useredit = ['id'=>$uid,'user_firstname'=>$fname,'user_lastname'=>$lname,'user_email'=>$mail,'user_contact'=>$con,'user_address'=>$user_address,'username'=>$uname,'password'=>$pass,'user_id'=>$uid,'user_img'=>$old_image_path];
                          

                                $user_table_edit=['id'=>$uid,'username'=>$uname,'password'=>$pass,'firstname'=>$fname,'lastname'=>$lname,'user_img'=>$old_image_path];

                                    
                                 $this->load->model('user_model');
                                  if($user_row->password == $pass)
                                  {
                                      if($this->user_model->insertuser($uid,$useredit,$user_table_edit))
                                        {
                                            $error="Edit Profile Successful.";
                                            $this->session->set_flashdata('error',$error);
                                            $this->session->set_flashdata('status','btn-success');
                                            redirect(base_url('profile'));
                                        }
                                        else
                                        {
                                            redirect(base_url('profile'));                         
                                        }
                                  }
                                  else
                                  {
                                        if($this->user_model->insertuser($uid,$useredit,$user_table_edit))
                                        {
                                            $error="Edit Profile Successful.";
                                            $this->session->set_flashdata('error',$error);
                                            $this->session->set_flashdata('status','btn-success');
                                           $this->session->sess_destroy('user_id');
                                            

                                            redirect(base_url('login_form'));
                                        }
                                        else
                                        {
                                            redirect(base_url('profile'));                         
                                        }

                                  }
                                
                    }
            }
            else
            {
                $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
                $value = $user_row->user_id;
               $this->load->view('backend/header',['user_row'=>$user_row]);
                $this->load->model("user_model");
                $use_rows=$this->user_model->get_user($value);
                $this->load->view('backend/profile',['use_rows'=>$use_rows]);
                $this->load->view('backend/footer');
            }

     }
    /////---------------------end my profile--------------------////////////
     public function all_enquiries()
     {
             $user_row=$this->mlogin->get_user($this->session->userdata('user_id'));
             $this->load->model('enquiry_model');
            $enquiry = $this->enquiry_model->enquiry_list();
             $this->load->view('backend/all_enquiries',['user_row'=>$user_row,'enquiry'=>$enquiry]);

     }
    public function __construct()
        {
            parent::__construct();
            
          $this->load->model('loginmodel', 'mlogin');
           if($this->session->userdata('user_id') == NULL || $this->session->userdata('user_id') == 0)
        {
            return redirect(base_url('login_form'));
        }
        }
}